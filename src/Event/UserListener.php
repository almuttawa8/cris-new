<?
use Cake\Event\EventListenerInterface;
use Cake\Log\Log; 
use Cake\Mailer\Email; 

class UserListener implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Model.Events.afterEdit' => 'afterEdit'
        ];
    }

    public function afterEdit($event)
    {
        echo 'An event has been edited.';
    }
}