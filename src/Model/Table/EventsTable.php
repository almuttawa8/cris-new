<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Events Model
 *
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\ResidentsTable|\Cake\ORM\Association\BelongsToMany $Residents
 * @property |\Cake\ORM\Association\HasMany $Registrations
 * @property |\Cake\ORM\Association\BelongsToMany $Services
 *
 * @method \App\Model\Entity\Event get($primaryKey, $options = [])
 * @method \App\Model\Entity\Event newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Event[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Event|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Event patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Event[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Event findOrCreate($search, callable $callback = null, $options = [])
 */
class EventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('events');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Residents', [
            'foreignKey' => 'event_id',
            'targetForeignKey' => 'resident_id',
            'joinTable' => 'registrations'
        ]);
        $this->belongsToMany('Services', [
            'foreignKey' => 'event_id',
            'targetForeignKey' => 'service_id',
            'joinTable' => 'events_services'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image_name'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->date('event_date')
            ->requirePresence('event_date', 'create')
            ->notEmpty('event_date');

        $validator
            ->time('start_time')
            ->requirePresence('start_time', 'create')
            ->notEmpty('start_time');

        $validator
            ->time('end_time')
            ->requirePresence('end_time', 'create')
            ->notEmpty('end_time');

        $validator
            ->decimal('cost')
            ->allowEmpty('cost')
            ->greaterThanOrEqual('cost',0);

        $validator
            ->scalar('catering')
            ->requirePresence('catering', 'create')
            ->notEmpty('catering');

        $validator
            ->allowEmpty('image_name');

        $validator
            ->boolean('archived')
            ->allowEmpty('archived');

        $validator
            ->add('image_name', [
                'validExtension' => [
                    'rule' => ['extension',['jpeg', 'png','jpg','gif']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => ('Only valid image files are allowed to be uploaded! ( jpeg, png, jpg, gif )')
                ]
            ])
            ->allowEmpty('image_name');

        $validator
            ->integer('availability')
            ->greaterThanOrEqual('availability',0)
            ->notEmpty('availability');

        $validator
            ->scalar('Hyperlink')
            ->maxLength('Hyperlink', 255)
            ->allowEmpty('Hyperlink');

        $validator
            ->scalar('desc')
            ->maxLength('desc', 1000)
            ->allowEmpty('desc');

        $validator
            ->scalar('accessibility');

        $validator
            ->scalar('beverage')
            ->requirePresence('beverage', 'create')
            ->notEmpty('beverage');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['location_id'], 'Locations'));

        return $rules;
    }

}
