<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 * @property \App\Model\Table\EventsTable|\Cake\ORM\Association\BelongsToMany $Events
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsToMany $Categories
 *
 * @method \App\Model\Entity\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, callable $callback = null, $options = [])
 */
class ServicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('services');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Events', [
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'event_id',
            'joinTable' => 'events_services'
        ]);
        $this->belongsToMany('Categories', [
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'category_id',
            'joinTable' => 'services_categories'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'banner'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->allowEmpty('name');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 10)
            ->integer('phone')
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->add('banner', [
                'validExtension' => [
                    'rule' => ['extension',['jpeg', 'png','jpg','gif']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => ('Only valid image files are allowed to be uploaded! ( jpeg, png, jpg, gif )')
                ]
            ])
            ->allowEmpty('banner');

        $validator
            ->scalar('desc')
            ->maxLength('desc', 300)
            ->allowEmpty('desc');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
