<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ecommerce Model
 *
 *
 * @method \App\Model\Entity\Maintenance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Maintenance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Maintenance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Maintenance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Maintenance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Maintenance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Maintenance findOrCreate($search, callable $callback = null, $options = [])
 */

class MaintenanceTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('maintenance');
        $this->setDisplayField('resident_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image_name'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->scalar('apartment_no')
            ->maxLength('apartment_no', 64)
            ->notEmpty('apartment_no');

        $validator
            ->scalar('resident_name')
            ->maxLength('resident_name', 64)
            ->notEmpty('resident_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 16)
            ->integer('phone')
            ->notEmpty('phone');

        $validator
            ->scalar('maintenance_desc')
            ->maxLength('maintenance_desc', 300)
            ->notEmpty('maintenance_desc');

        $validator
            ->add('image_name', [
                'validExtension' => [
                    'rule' => ['extension',['jpeg', 'png','jpg','gif']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => ('Only valid image files are allowed to be uploaded! ( jpeg, png, jpg, gif )')
                ]
            ])
            ->allowEmpty('image_name');

        $validator
            ->scalar('internal_access')
            ->maxLength('internal_access', 16)
            ->allowEmpty('internal_access');

        return $validator;
    }

}
