<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Home Model
 *
 *
 * @method \App\Model\Entity\MaintenanceDetails get($primaryKey, $options = [])
 * @method \App\Model\Entity\MaintenanceDetails newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MaintenanceDetails[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MaintenanceDetails|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaintenanceDetails patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MaintenanceDetails[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MaintenanceDetails findOrCreate($search, callable $callback = null, $options = [])
 */
class MaintenanceDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('maintenance_details');
        $this->setDisplayField('form_instruction');
        $this->setPrimaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('form_instruction')
            ->maxLength('form_instruction', 300)
            ->requirePresence('form_instruction', 'create')
            ->notEmpty('form_instruction');

        $validator
            ->scalar('form_conditions')
            ->maxLength('form_conditions', 300)
            ->requirePresence('form_conditions', 'create')
            ->notEmpty('form_conditions');

        $validator
            ->scalar('form_note')
            ->maxLength('form_note', 300)
            ->requirePresence('form_note', 'create')
            ->notEmpty('form_note');

        return $validator;
    }

}
