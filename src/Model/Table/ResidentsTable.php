<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Residents Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsTo $Properties
 * @property |\Cake\ORM\Association\HasMany $Answers
 * @property |\Cake\ORM\Association\HasMany $Enquiries
 * @property |\Cake\ORM\Association\HasMany $Registrations
 *
 * @property \App\Model\Table\ClubsTable|\Cake\ORM\Association\BelongsToMany $Clubs
 * @property \App\Model\Table\EventsTable|\Cake\ORM\Association\BelongsToMany $Events
 * @property \App\Model\Table\InterestsTable|\Cake\ORM\Association\BelongsToMany $Interests
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\Resident get($primaryKey, $options = [])
 * @method \App\Model\Entity\Resident newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Resident[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Resident|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Resident patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Resident[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Resident findOrCreate($search, callable $callback = null, $options = [])
 */
class ResidentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('residents');
        $this->setDisplayField('identifier');
        $this->setPrimaryKey('id');

        $this->belongsTo('Properties', [
            'foreignKey' => 'property_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Enquiries', [
            'foreignKey' => 'resident_id'
        ]);
        $this->belongsToMany('Clubs', [
            'foreignKey' => 'resident_id',
            'targetForeignKey' => 'club_id',
            'joinTable' => 'residents_clubs'
        ]);
        $this->belongsToMany('Events', [
            'foreignKey' => 'resident_id',
            'targetForeignKey' => 'event_id',
            'joinTable' => 'registrations'
        ]);
        $this->belongsToMany('Interests', [
            'foreignKey' => 'resident_id',
            'targetForeignKey' => 'interest_id',
            'joinTable' => 'residents_interests'
        ]);
        $this->belongsToMany('Questions', [
            'foreignKey' => 'resident_id',
            'targetForeignKey' => 'question_id',
            'joinTable' => 'answers'
        ]);
        $this->belongsToMany('Roles', [
            'foreignKey' => 'resident_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'residents_roles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('lname')
            ->maxLength('lname', 50)
            ->requirePresence('lname', 'create')
            ->notEmpty('lname');

        $validator
            ->scalar('fname')
            ->maxLength('fname', 50)
            ->requirePresence('fname', 'create')
            ->notEmpty('fname');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 10)
            ->minLength('phone', 8)
            ->integer('phone')
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->boolean('archived')
            ->allowEmpty('archived');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 4)
            ->allowEmpty('identifier');

        $validator
            ->scalar('property_type')
            ->maxLength('property_type', 50)
            ->allowEmpty('property_type');

        $validator
            ->scalar('landline')
            ->maxLength('landline', 4)
            ->allowEmpty('landline');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identifier']));
        $rules->add($rules->existsIn(['property_id'], 'Properties'));

        return $rules;
    }
}
