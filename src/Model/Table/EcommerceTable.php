<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ecommerce Model
 *
 *
 * @method \App\Model\Entity\Ecommerce get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ecommerce newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ecommerce[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ecommerce|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ecommerce patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ecommerce[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ecommerce findOrCreate($search, callable $callback = null, $options = [])
 */

class EcommerceTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ecommerce');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image_name'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 64)
            ->notEmpty('name');

        $validator
            ->decimal('cost')
            ->allowEmpty('cost')
            ->greaterThanOrEqual('cost',0);

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 16)
            ->integer('phone')
            ->allowEmpty('phone');

        $validator
            ->scalar('website')
            ->maxLength('website', 255)
            ->allowEmpty('website');

        $validator
            ->scalar('desc')
            ->maxLength('desc', 300)
            ->allowEmpty('desc');

        $validator
            ->add('image_name', [
                'validExtension' => [
                    'rule' => ['extension',['jpeg', 'png','jpg','gif']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => ('Only valid image files are allowed to be uploaded! ( jpeg, png, jpg, gif )')
                ]
            ])
            ->allowEmpty('image_name');

        $validator
            ->scalar('acceptance')
            ->maxLength('acceptance', 25)
            ->allowEmpty('acceptance');

        $validator
            ->time('date')
            ->allowEmpty('date', 'create');

        $validator
            ->scalar('wanted_for')
            ->maxLength('wanted_for', 25)
            ->allowEmpty('wanted_for');


        return $validator;
    }

}
