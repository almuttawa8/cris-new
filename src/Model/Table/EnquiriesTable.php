<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Enquiries Model
 *
 * @property \App\Model\Table\TopicsTable|\Cake\ORM\Association\BelongsTo $Topics
 * @property \App\Model\Table\ResidentsTable|\Cake\ORM\Association\BelongsTo $Residents
 *
 * @method \App\Model\Entity\Enquiry get($primaryKey, $options = [])
 * @method \App\Model\Entity\Enquiry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Enquiry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Enquiry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Enquiry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Enquiry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Enquiry findOrCreate($search, callable $callback = null, $options = [])
 */
class EnquiriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('enquiries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Residents', [
            'foreignKey' => 'resident_id'
        ]);

        $this->belongsTo('Topics', [
            'foreignKey' => 'topic_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 50);

        $validator
            ->scalar('body')
            ->maxLength('body', 350);

        $validator
            ->scalar('solved')
            ->allowEmpty('solved');

        $validator
            ->scalar('solution')
            ->maxLength('solution', 255)
            ->allowEmpty('solution');

        $validator
            ->scalar('response_type')
            ->maxLength('response_type', 50)
            ->allowEmpty('response_type');

        $validator
            ->scalar('modified_date')
            ->allowEmpty('modified_date');

        $validator
            ->scalar('modified_name')
            ->maxLength('modified_name', 50)
            ->allowEmpty('modified_name');

        $validator
            ->scalar('modified_phone')
            ->maxLength('modified_phone', 10)
            ->allowEmpty('modified_phone');

        $validator
            ->scalar('modified_email')
            ->maxLength('modified_email', 50)
            ->allowEmpty('modified_email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['resident_id'], 'Residents'));

        return $rules;
    }
}
