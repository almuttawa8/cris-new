<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Enquiry Entity
 *
 * @property int $id
 * @property int $resident_id
 * @property string $subject
 * @property string $body
 * @property string $solved
 * @property string $solution
 * @property string $response_type
 * @property int $topic_id
 * @property \Cake\I18n\FrozenDate modified_date
 * @property string modified_name
 * @property string modified_phone
 * @property string modified_email
 *
 * @property \App\Model\Entity\Topic $topic
 * @property \App\Model\Entity\Resident $resident
 */
class Enquiry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'resident_id' => true,
        'subject' => true,
        'body' => true,
        'solved' => true,
        'solution' => true,
        'response_type' => true,
        'topic_id' => true,
        'resident' => true,
        'topic' => true,
        'modified_date' => true,
        'modified_name' => true,
        'modified_phone' => true,
        'modified_email' => true
    ];

    // this function will return Yes or No
    protected function _getSolvedName(){
        if($this->solved == 'Y'){
            return 'Solved';
        }
        else if ($this->solved == 'N'){
            return 'Unsolved';
        }
        else{
            return '';
        }
    }


}
