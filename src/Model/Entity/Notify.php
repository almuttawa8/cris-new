<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $news
 */
class Notify extends Entity
{

    protected $_accessible = [
        'news' => true
    ];

}
