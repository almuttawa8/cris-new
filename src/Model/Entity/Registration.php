<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Registration Entity
 *
 * @property int $resident_id
 * @property int $event_id
 * @property string $transport
 * @property string $dietary
 * @property string $payment
 * @property string $comment
 *
 * @property \App\Model\Entity\Resident $resident
 * @property \App\Model\Entity\Event $event
 */
class Registration extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'transport' => true,
        'dietary' => true,
        'payment' => true,
        'comment' => true,
        'resident' => true,
        'event' => true
    ];

    #Change Y and No to Yes and No respectively
    protected function _getTransportOption() {
        if($this->transport == "Y"){
            return "Yes";
        }
        elseif ($this->transport == "N"){
            return "No";
        }
    }

    #Change Y and No to Yes and No respectively
    protected function _getDietaryOption() {
        if($this->dietary == "Y"){
            return "Yes";
        }
        elseif ($this->dietary == "N"){
            return "No";
        }
    }

    #Change Y and No to Yes and No respectively
    protected function _getPaymentOption() {
        if($this->payment == "Y"){
            return "Yes";
        }
        elseif ($this->payment == "N"){
            return "No";
        }
    }
}
