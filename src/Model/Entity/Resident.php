<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Resident Entity
 *
 * @property int $id
 * @property int $property_id
 * @property string $lname
 * @property string $fname
 * @property string $phone
 * @property string $email
 * @property bool $archived
 * @property string $identifier
 * @property string $property_type
 * @property string $landline
 *
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\Club[] $clubs
 * @property \App\Model\Entity\Event[] $events
 * @property \App\Model\Entity\Interest[] $interests
 * @property \App\Model\Entity\Role[] $roles
 */
class Resident extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'property_id' => true,
        'lname' => true,
        'fname' => true,
        'phone' => true,
        'email' => true,
        'archived' => true,
        'identifier' => true,
        'property' => true,
        'clubs' => true,
        'events' => true,
        'interests' => true,
        'roles' => true,
        'property_type' => true,
        'landline' => true
    ];
}
