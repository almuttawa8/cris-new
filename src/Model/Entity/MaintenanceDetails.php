<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Home Entity
 *
 * @property int $id
 * @property string $form_instruction
 * @property string $form_conditions
 * @property string $form_note
 */
class MaintenanceDetails extends Entity
{

    protected $_accessible = [
        'form_instruction' => true,
        'form_conditions' => true,
        'form_note' => true
    ];

}
