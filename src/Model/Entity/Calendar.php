<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Calendar Entity
 *
 * @property int $id
 * @property string $file_name
 * @property \Cake\I18n\FrozenDate $date_published
 */
class Calendar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'file_name' => true,
        'archived' => true,
        'date_published' => true,
        'display_field' => true
    ];

    protected function _getCalendarFileUrl(){
        return \Cake\Routing\Router::url("/files/Calendars/file_name/{$this->file_name}");
    }
}
