<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $question
 * @property string $answer
 */
class Faqs extends Entity
{

    protected $_accessible = [
        'question' => true,
        'answer' => true
    ];

}
