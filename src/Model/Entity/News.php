<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $newsDesc
 * @property \Cake\I18n\FrozenDate $date
 */
class News extends Entity
{

    protected $_accessible = [
        'newsDesc' => true
    ];

}
