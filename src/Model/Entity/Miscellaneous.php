<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Miscellaneous Entity
 *
 * @property int $id
 * @property string $file_name
 * @property \Cake\I18n\FrozenDate $date_published
 * @property bool $archived
 */
class Miscellaneous extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'file_name' => true,
        'date_published' => true,
        'archived' => true,
        'display_field' => true
    ];

    protected function _getMiscFileUrl(){
        return \Cake\Routing\Router::url("/files/Miscellaneous/file_name/{$this->file_name}");
    }
}
