<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Service Entity
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $Hyperlink
 * @property string $banner
 * @property string $desc
 *
 * @property \App\Model\Entity\Event[] $events
 * @property \App\Model\Entity\Category[] $categories
 */
class Service extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'phone' => true,
        'email' => true,
        'Hyperlink' => true,
        'banner' => true,
        'events' => true,
        'categories' => true,
        'desc' => true
    ];

    protected function _getImageUrl(){
        return \Cake\Routing\Router::url("/files/Services/banner/{$this->banner}");
    }

}
