<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Newsletter Entity
 *
 * @property int $id
 * @property string $file_name
 * @property string $category
 * @property string $Hyperlink
 * @property \Cake\I18n\FrozenDate $date_published
 */
class Newsletter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'file_name' => true,
        'category' => true,
        'archived' => true,
        'date_published' => true,
        'display_field' => true,
        'Hyperlink' => true

    ];

    // this function will return the url of the newsletter file
    protected function _getNewsletterFileUrl(){
        return \Cake\Routing\Router::url("/files/Newsletters/file_name/{$this->file_name}");
    }

    // this function will return the full category name
    protected function _getCategoryName(){
        if($this->category == 'C'){
            return 'Committee';
        }
        else if ($this->category == 'L'){
            return 'Lendlease';
        }
        else{
            return '';
        }
    }
}
