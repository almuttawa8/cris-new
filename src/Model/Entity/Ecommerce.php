<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Service Entity
 *
 * @property int $id
 * @property string $name
 * @property string $cost
 * @property string $email
 * @property string $phone
 * @property string $website
 * @property string $desc
 * @property string $image_name
 * @property string $acceptance
 * @property \Cake\I18n\FrozenDate $date
 * @property int $resident_id
 * @property string $wanted_for
 */

class Ecommerce extends Entity
{
    protected $_accessible = [
        'name' => true,
        'cost' => true,
        'email' => true,
        'phone' => true,
        'website' => true,
        'desc' => true,
        'image_name' => true,
        'acceptance' => true,
        'resident_id' => true,
        'wanted_for' => true
    ];
}
