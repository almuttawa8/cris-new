<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\FrozenDate $event_date
 * @property \Cake\I18n\FrozenTime $start_time
 * @property \Cake\I18n\FrozenTime $end_time
 * @property float $cost
 * @property string $catering
 * @property string beverage
 * @property int $location_id
 * @property $image_name
 * @property bool $archived
 * @property int $availability
 * @property string $desc
 * @property string $accessibility
 * @property string $Hyperlink
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\Resident[] $residents
 */
class Event extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'event_date' => true,
        'start_time' => true,
        'end_time' => true,
        'cost' => true,
        'catering' => true,
        'beverage' => true,
        'location_id' => true,
        'image_name' => true,
        'archived' => true,
        'availability' => true,
        'desc' => true,
        'accessibility' => true,
        'location' => true,
        'residents' => true,
        'Hyperlink' => true
    ];

    #Change Y and No to Yes and No respectively
    protected function _getCateringOption() {
        if($this->catering == "Y"){
            return "Yes";
        }
        elseif ($this->catering == "N"){
            return "No";
        }
    }

    #Change Y and No to Yes and No respectively
    protected function _getBeverageOption() {
        if($this->beverage == "Y"){
            return "Yes";
        }
        elseif ($this->beverage == "N"){
            return "No";
        }
    }

    protected function _getAccessibilityOption() {
        if($this->accessibility == "Y"){
            return "Yes";
        }
        elseif ($this->accessibility == "N"){
            return "No";
        }
    }

    // this function will return the url of the image file
    protected function _getImageUrl(){
        return \Cake\Routing\Router::url("/files/Events/image_name/{$this->image_name}");
    }

    protected function _getCountResident(){
        //get the user who registers this event
        $registrations = TableRegistry::get('Registrations');
        $query = $registrations->find()->where(['event_id' => $this->id ]);

        $total = $query->count();
        return $total;
    }





}
