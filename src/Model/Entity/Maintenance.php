<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Maintenance Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $date
 * @property string $apartment_no
 * @property string $resident_name
 * @property string $email
 * @property string $phone
 * @property string $maintenance_desc
 * @property string $image_name
 * @property string $internal_access
 * @property int $resident_id
 */

class Maintenance extends Entity
{
    protected $_accessible = [
        'apartment_no' => true,
        'resident_name' => true,
        'email' => true,
        'phone' => true,
        'maintenance_desc' => true,
        'image_name' => true,
        'internal_access' => true,
        'resident_id' => true
    ];
}
