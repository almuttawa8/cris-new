<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Minute Entity
 *
 * @property int $id
 * @property string $file_name
 * @property int $min_id
 * @property \Cake\I18n\FrozenDate $date_published
 *
 * @property \App\Model\Entity\MinCategory $min_category
 */
class Minute extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'file_name' => true,
        'min_id' => true,
        'date_published' => true,
        'archived' => true,
        'min_category' => true,
        'display_field' => true
    ];

    protected function _getMinuteFileUrl(){
        return \Cake\Routing\Router::url("/files/Minutes/file_name/{$this->file_name}");
    }

}
