<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $header
 * @property string $desc
 */
class Restaurant extends Entity
{

    protected $_accessible = [
        'header' => true,
        'desc' => true
    ];

}
