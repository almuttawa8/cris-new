<?php
namespace App\Model\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Home Entity
 *
 * @property int $id
 * @property string $welcome
 * @property string $Intro
 * @property bool $archived
 */
class Home extends Entity
{

    protected $_accessible = [
        'welcome' => true,
        'Intro' => true,
        'archived' => true
    ];

}
