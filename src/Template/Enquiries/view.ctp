<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Enquiry $enquiry
 */
?>
<div id="dataTable_filter" class="dataTables_filter" align="left" style="margin-bottom: 0.5em">
    <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'resident_index'], array('class' => 'btn btn-primary')) ?>
</div>

<div class="enquiries view large-9 medium-8 columns content">
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Receipt Number</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->id) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Topic</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->topic->name) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label" ><b>Subject</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->subject) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Body</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->body) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>