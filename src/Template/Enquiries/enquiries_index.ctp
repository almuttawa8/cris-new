<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Enquiry[]|\Cake\Collection\CollectionInterface $Enquiries
* @var \App\Model\Entity\Resident[]|\Cake\Collection\CollectionInterface $Residents
*/
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Enquiries page</h1>

            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Enquiries table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Resident Name</th>
                                <th>Subject</th>
                                <th>Response Type</th>
                                <th>Question</th>
                                <th>Phone or Email</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($Enquiries as $enquiries): ?>
                                <tr>
                                    <td><?php foreach ($Residents as $resident):
                                            if($enquiries->resident_id == $resident->id){?>
                                                <?= h($resident->fname)?>
                                                <?= h($resident->lname)?>
                                            <?php }
                                        endforeach; ?>
                                    </td>
                                    <td><?= h($enquiries->subject) ?></td>
                                    <td><?= h($enquiries->response_type) ?></td>
                                    <td><?= h($enquiries->body) ?></td>

                                    <td>
                                        <?php foreach ($Residents as $resident):
                                            if($enquiries->resident_id == $resident->id){
                                                if($enquiries->response_type == 'phone'){?>
                                                    <?= h($resident->phone)?>
                                                <?php } else{?>
                                                    <?= h($resident->email)?>

                                                <?php }}
                                        endforeach;?>

                                    </td>
                                    <td><?= h($enquiries->modified_date->format('d M,Y')) ?></td>
                                    <td class="actions" align="center">

                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal<?=$enquiries->id ?>">Delete</button>

                                        <div class="modal fade" id="confirmModal<?=$enquiries->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this enquiry?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'deleteEnquiry', $enquiries->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>


                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>



