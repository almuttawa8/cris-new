<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Enquiry $enquiry
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>




<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">
        Send an enquiry
    </h1>


    <?= $this->Form->create($enquiry) ?>
    <div class="row ml-2">
        <div class="col-lg-8 mb-4">
            <div class="alert alert-warning" role="alert">
                * Indicates required field
            </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Subject*</label>
                        <div class="col-sm-50">
                            <?php echo $this->Form->control('subject', ['label' => false, 'class' => 'form-control']);?>
                        </div>
                        <p class="help-block"></p>
                    </div>
                </div>
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-8 pt-0">Preferred Contact Method</legend>

                        <div class="col-sm-7">
                            <label class="container1">Email
                                <input type="radio" name="response_type" checked="checked" value="email">
                                <span class="checkmark1 mt-1" for="email"></span>
                            </label>
                            <label class="container1">Phone
                                <input type="radio" name="response_type" value="phone">
                                <span class="checkmark1 mt-1" for="phone"></span>
                            </label>
                        </div>

                    </div>
                </fieldset>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Question*</label>
                        <div class="col-sm-50">
                            <?php echo $this->Form->input('body', ['rows'=>10, 'label' => false,'class' => 'form-control'])?>
                        </div>
                    </div>
                </div>

        </div>

    </div>
    <div style="color: #961982;"><b><?= $this->Flash->render('success_enquiry') ?>
            <?= $this->Flash->render('fail_enquiry') ?>
    </b></div>
    <div align="left" style="margin-bottom: 0.5em">
        <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'mainView'], array('style' => 'color:white', 'class' => 'btn btn-primary ml-4 mt-2')) ?>
        <input class="btn btn-primary ml-4 mt-2" type="submit" value="Submit">
    </div>

    <?= $this->Form->end() ?>

</div>

