<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Enquiry $enquiry
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
</div>

<div class="enquiries view large-9 medium-8 columns content">
    <h3>Enquiry from <?= h($enquiry->resident->fname) ?> <?= h($enquiry->resident->lname) ?></h3>
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Receipt Number</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->id) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Preferred Contact Method</b></label>
                    <div class="col-sm-8">
                        <?php if($enquiry->response_type == 'face'){
                            echo 'face-to-face';
                        }
                        else{
                            ?> <?= h($enquiry->response_type); ?>
                            <?php
                        } ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Phone Number</b></label>
                    <div class="col-8">
                        <?= h($enquiry->resident->phone) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Email</b></label>
                    <div class="col-8">
                        <?= h($enquiry->resident->email) ?>
                    </div>
                </div>

                <br>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Topic</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->topic->name) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label" ><b>Subject</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->subject) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Body</b></label>
                    <div class="col-sm-8">
                        <?= h($enquiry->body) ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="card container">
                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Last Modified By</b></label>
                    <div class="col-sm-8">
                        <?php if ($enquiry->modified_name){
                            echo h($enquiry->modified_name);
                        }  ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Last Modified On</b></label>
                    <div class="col-sm-8">
                        <?php if ($enquiry->modified_date){
                            echo h($enquiry->modified_date->format("d M, Y"));
                        }  ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Phone</b></label>
                    <div class="col-sm-8">
                        <?php if ($enquiry->modified_phone){
                            echo h($enquiry->modified_phone);
                        }  ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-4 col-form-label"><b>Email</b></label>
                    <div class="col-sm-8">
                        <?php if ($enquiry->modified_email){
                            echo h($enquiry->modified_email);
                        }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="enquiries form large-9 medium-8 columns content">
    <?= $this->Form->create($enquiry) ?>

    <fieldset class="form-group">

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Status</label>
            <div class="col-sm-4">
                <?php echo $this->Form->control('solved', ['type'=>'radio','options' => [
                    ['value' => 'Y', 'text' => __('Solved')],
                    ['value' => 'N', 'text' => __('Unsolved')],
                ],
                    'default'=>'N',
                    'label'=>false,
                ]); ?>
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4 col-form-label"><b>Solution</b></label>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php echo $this->Form->control('solution', ['rows'=>5, 'label' => false]); ?>
            </div>
        </div>

    </fieldset>
    <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'index'], array('class' => 'btn btn-primary','style'=> 'margin-bottom: 0.5em')) ?>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

    <br>
</div>
