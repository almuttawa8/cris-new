<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Faqs $faqs
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Q&A</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">FAQs page</li>
                <li class="breadcrumb-item active">Add Q&A</li>
            </ol>
            <?= $this->Form->create($faqs, ['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('question', ['label'=>"Question *", 'rows'=>5]); ?>
                            <?php echo $this->Form->control('answer', ['label'=>"Answer *", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'index'], array('style' => 'color:white' , 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>


