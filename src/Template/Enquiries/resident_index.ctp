<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registration[]|\Cake\Collection\CollectionInterface $registrations
 */
?>

<div id="dataTable_filter" class="dataTables_filter" align="left">
    <?= $this->Html->link(__('Back'), ['controller' => 'Enquiries', 'action' => 'add'], array('class' => 'btn btn-primary')) ?>
</div>

<div class="table-responsive">
    <div class="registrations index large-9 medium-8 columns content">
        <h3><?= __('My Enquiries') ?></h3>

        <table class="table table-hover" id="dataTable">
            <thead>
            <tr>
                <th scope="col" style="color:#c507ac">Receipt Number</th>
                <th scope="col" style="color:#c507ac">Topics</th>
                <th scope="col" style="color:#c507ac">Subject</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($enquiries as $enquiry): ?>
                <tr>
                    <td><?= h($enquiry->id)?></td>
                    <td><?= h($enquiry->topic->name) ?></td>
                    <td><?= h($enquiry->subject) ?></td>
                    <td class="actions" align="center">
                        <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link(__('View'), ['action' => 'view', $enquiry->id], array('style' => 'color:white')) ?></button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
