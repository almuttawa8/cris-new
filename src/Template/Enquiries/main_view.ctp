<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Faqs[]|\Cake\Collection\CollectionInterface $Faqs
 */
?>



<!-- Page Content -->
<div class="container">



    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">FAQs
    </h1>

    <div class="mt-4 mb-3" align="right">
        <?= $this->Html->link("Send an enquiry", ['controller'=>'Enquiries', 'action' => 'add'], array('class' => 'btn btn-primary', 'style'=> 'width: 160px')) ?>
    </div>

    <div class="mb-4" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach ($Faqs as $faqs): ?>
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color: #961982"><b><?= h($faqs->question) ?></b></a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-body">
                        <?= h($faqs->answer) ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>





