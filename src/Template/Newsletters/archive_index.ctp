<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Newsletter[]|\Cake\Collection\CollectionInterface $newsletters
*/
?>





<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Archived Newsletters</h1>


            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Archived files table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Newletters</th>
                                <th>Category</th>
                                <th>Find more details here</th>
                                <th>Date Published</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($archivedNewsletters as $newsletter): ?>
                                <tr>
                                    <td><?= $this->Html->link($newsletter->display_field,"/webroot/files/Newsletters/file_name/".$newsletter->file_name, ['target'=>'_blank']) ?></td>
                                    <td><?= h($newsletter->category_name) ?></td>
                                    <td><?= h($newsletter->Hyperlink) ?></td>
                                    <td><?= h($newsletter->date_published->format("d M, Y")) ?></td>
                                    <td class="actions" align="center">
                                        <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal-<?= $newsletter->id?>">Restore</button>

                                        <div class="modal fade" id="confirmModal-<?= $newsletter->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to restore this newsletter - <?= $newsletter->file_name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'restore', $newsletter->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal<?=$newsletter->id ?>"">Delete</button>

                                        <div class="modal fade" id="confirmModal<?=$newsletter->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete this file - <?= $newsletter->file_name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $newsletter->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>




