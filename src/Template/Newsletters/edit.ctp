<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Newsletter $newsletter
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
$this->Form->setTemplates([
    'radio' => '<input{{attrs}}class="form-check-input" type="radio" name="{{name}}" value="{{value}}">',
    'nestingLabel' => '<div{{attrs}}class="form-check form-check-inline">{{input}}<label class="form-check-label" for="inlineRadio1">{{text}}</label></div>'
]);
?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Newsletters</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Newsletters page</li>
                <li class="breadcrumb-item active">Edit Newsletters</li>
            </ol>
            <?= $this->Form->create($newsletter,['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">File Name*: </label>
                            <div class="ml-2"><?php echo $this->Form->control('display_field',['label' => false, 'maxlength'=> 40, 'required']); ?></div>


                            <?php
                            if ($newsletter->file_name){?>
                                <div class="ml-2"><?php echo $this->Html->link('View current PDF', "/webroot/files/Newsletters/file_name/".$newsletter->file_name, ['target'=>'_blank']);
                            } ?></div>

                            <label class="col-sm-4 col-form-label">Current File: </label>
                            <div class="ml-2"><?php echo $this->Form->control('file_name', array( 'disabled' => 'disabled', 'size'=>'30', 'label' => false)); ?>
                            </div>
                            <label class="col-sm-4 col-form-label">Upload New File: (<b>PDFs</b> Only) </label>
                            <div class="ml-2"><?php echo $this->Form->text('file_name', ['type' => 'file']); ?></div>


                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Category*</legend>
                                    <div class="col-sm-8 ml-2">
                                        <?php echo $this->Form->control('category', ['type'=>'radio', 'options' => [
                                            ['value' => 'C', 'text' => __('Committee')],
                                            ['value' => 'L', 'text' => __('LendLease')],
                                        ],
                                            'label'=>false,
                                        ]); ?>
                                    </div>
                                </div>
                            </fieldset>

                            <label class="col-sm-4 col-form-label">Website:</label>
                           <div class="ml-2"><?php echo $this->Form->control('Hyperlink', array('size'=>'30', 'type'=>'url','label' => false)); ?>
                            </div>


                            <label class="col-sm-4 col-form-label">Date Published*</label>
                            <div class="ml-2"><?php echo $this->Form->control('date_published',['id'=>'datepicker','type'=>'text', 'label'=>false,'readOnly']); ?></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['action' => 'index'], array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

