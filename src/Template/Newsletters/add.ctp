<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Newsletter $newsletter
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Newsletters</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Newsletters page</li>
                <li class="breadcrumb-item active">Add Newsletters</li>
            </ol>
            <?= $this->Form->create($newsletter,['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">File Name* </label>
                            <div class="ml-2"><?php echo $this->Form->control('display_field',['label' => false, 'maxlength'=> 40, 'required']); ?></div>


                            <label class="col-sm-4 col-form-label">Upload New File(<b>PDFs</b> Only ):</label>
                            <?php echo $this->Form->text('file_name', ['type' => 'file', 'accept' => '.pdf, .PDF']); ?>



                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Category*</legend>
                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input ml-3" type="radio" name="category" id="C" value="C" checked>
                                            <label class="form-check-label" for="C">Committee</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="category" id="L" value="L">
                                            <label class="form-check-label" for="L">LendLease</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <label class="col-sm-4 col-form-label">Website:</label>
                            <input type="url" class="form-control ml-2" name="Hyperlink" placeholder="https://www.website.com/">

                            <label class="col-sm-4 col-form-label">Date Published*</label>
                            <div class="ml-2"><?php echo $this->Form->control('date_published',['id'=>'datepicker','type'=>'text', 'label'=>false,'readOnly','value'=>date("d-m-yy")]); ?></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['action' => 'index'], array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

