<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Location $location
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Location</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Locations page</li>
                <li class="breadcrumb-item active">Add Location</li>
            </ol>
            <?= $this->Form->create($location) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Name</label>
                            <div class="ml-2"><?php echo $this->Form->control('name', ['label' => false]); ?></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Locations', 'action' => 'index'], array('style' => 'color:white' , 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

