<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Minute[]|\Cake\Collection\CollectionInterface $minutes
*/
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Minutes page</h1>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Categories", ['controller' => 'MinCategories', 'action' => 'index'], array('style' => 'color:white; width:120px;' , 'class'=> 'btn btn-primary')) ?>
                <?= $this->Html->link("Add Minutes", ['action' => 'add'], array('style' => 'color:white', 'class'=> 'btn btn-primary', 'style'=> 'width: 180px')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Minutes table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Minute Name</th>
                                <th>Minute Category</th>
                                <th>Date Published</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($minutes as $minute): ?>
                                <tr>
                                    <td><?= $this->Html->link($minute->display_field, "/webroot/files/Minutes/file_name/".$minute->file_name,['target' =>'_blank'])?></td>
                                    <td><?= $minute->min_category->name ?></td>
                                    <td><?= h($minute->date_published->format("d M, Y")) ?></td>
                                    <td class="actions" width="50%" align="center">
                                        <a class="btn btn-primary mb-1" href="
                                        <?php echo $this->Url->build([
                                            "controller" => "Minutes",
                                            "action" => "edit",
                                            $minute->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?=$minute->id ?>" data-id="<?php echo $minute->id?>" id="<?php echo $minute->id ?>">Archive</button>

                                        <div class="modal fade" id="confirmModal-<?=$minute->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this minute - <?= $minute->file_name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes', '#'), ['action' => 'archive', $minute->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>

                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

