<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question $question
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>

<div id="dataTable_filter" class="dataTables_filter" align="left">
    <button class="btn btn-primary"><?= $this->Html->link(__('Questions'), ['controller' => 'Questions', 'action' => 'index'], array('style' => 'color:white')) ?></button>
</div>
<div class="questions form large-9 medium-8 columns content">
    <?= $this->Form->create($question) ?>
    <legend><?= __('Add Question') ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="col-sm-6">
                    <div class="indicatorDefault">
                        * Indicates required field
                    </div>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('question',['label'=>"Question *"]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
