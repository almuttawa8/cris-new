<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question[]|\Cake\Collection\CollectionInterface $questions
 */
?>
<div id="dataTable_filter" class="dataTables_filter" align="right">
    <button class="btn btn-primary"><?= $this->Html->link("Add Question", ['action' => 'add'], array('style' => 'color:white')) ?></button>
</div>
<div class="questions index large-9 medium-8 columns content">
    <h3><?= __('Questions') ?></h3>
    <table class="table table-bordered" id="dataTable">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('question') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($questions as $question): ?>
            <tr>
                <td><?= h($question->question) ?></td>
                <td class="actions"  width="30%" align="center">
                    <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link(__('Edit'), ['action' => 'edit', $question->id], array('style' => 'color:white')) ?></button>
                    <button class="btn btn-outline-primary"><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $question->id], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id)]) ?></button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
