<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $menus
*/
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Food Menus page</h1>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Add Menus", ['action' => 'add'], array('style' => 'color:white', 'class'=>'btn btn-primary' , 'style'=> 'width: 180px')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Menus table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Food Menus</th>
                                <th>Date Published</th>
                                <th>Menu Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($menus as $menu): ?>
                                <tr>
                                    <td><?= $this->Html->link($menu->display_field, "/webroot/files/Menus/file_name/".$menu->file_name,['target' =>'_blank']) ?></td>
                                    <td><?= h($menu->date_published->format("d M, Y")) ?></td>
                                    <?php if (!empty($menu->menu_date)){?>
                                        <td><?= h($menu->menu_date->format("d M,Y")) ?></td>
                                    <?php }
                                    else{?>
                                        <td><?= h($menu->menu_date) ?></td>
                                    <?php }?>
                                    <td class="actions" width="50%" align="center">
                                        <a class="btn btn-primary mb-1" href="
                                        <?php echo $this->Url->build([
                                            "controller" => "Menus",
                                            "action" => "edit",
                                            $menu->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?=$menu->id ?>" data-id="<?php echo $menu->id?>" id="<?php echo $menu->id ?>">Archive</button>

                                        <div class="modal fade" id="confirmModal-<?=$menu->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this food menu - <?= $menu->file_name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes', '#'), ['action' => 'archive', $menu->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>



