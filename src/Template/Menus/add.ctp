<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu $menu
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Food Menu</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Food Menus page</li>
                <li class="breadcrumb-item active">Add Food Menu</li>
            </ol>
            <?= $this->Form->create($menu, ['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">File Name* </label>
                            <div class="ml-2"> <?php echo $this->Form->control('display_field',['label' => false, 'maxlength'=> 40]); ?></div>


                            <label class="col-sm-4 col-form-label">Upload New File(<b>PDFs</b> Only ):</label>
                            <?php echo $this->Form->control('file_name' , ['type' => 'file', 'label'=> false]); ?>

                            <br><label class="col-sm-4 col-form-label">Menu Date: Special Menu Date (Daily)</label>
                            <div class="ml-2"><?php echo $this->Form->control('menu_date',['id'=>'datepicker','type'=>'text', 'label'=>false,'readOnly']); ?></div>

                        </div>

                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['action' => 'index'], array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>
</main>
</div>


