<?php
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Archived Other files</h1>


            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Archived files table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>File Name</th>
                                <th>Date Published</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($archivedMiscs as $misc): ?>
                                <tr>
                                    <td><?= $this->Html->link($misc->display_field, "/webroot/files/Miscellaneous/file_name/".$misc->file_name, ['target'=>'_blank']) ?></td>
                                    <td><?= h($misc->date_published->format("d M, Y")) ?></td>
                                    <td class="actions" align="center">
                                        <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal-<?= $misc->id?>">Restore</button>

                                        <div class="modal fade" id="confirmModal-<?= $misc->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to restore the file?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'restore', $misc->id], array('style' => 'color: black;', 'class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal<?=$misc->id ?>"">Delete</button>

                                        <div class="modal fade" id="confirmModal<?=$misc->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete this file - <?= $misc->file_name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $misc->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


