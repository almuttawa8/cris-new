<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
$this->layout = 'resident';
?>
<style>
    .serviceImage{
        height:300px;
        width:600px
    }

    @media only screen and (max-width: 730px) {
        .serviceImage{
            height:300px;
            width:100%;
        }
    }



</style>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3"><?= h($service->name) ?>
    </h1>


    <!-- Project One -->
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php if($service->banner != '')
            {
                echo $this->Html->image("/files/Services/banner/".$service->banner, array('alt' => '','class' =>'card-img-top serviceImage'));
            }
            else{
                echo $this->Html->image('cris-logo.png',array('class' =>'card-img-top serviceImage'));

            }?>
        </div>
    </div>
    <div class="row mt-2">

        <div class="ml-3">

            <table>
                <tr>
                    <?php if (!empty($service->phone)){?>
                        <th scope="row"><i class="fas fa-phone-square fa-fw" style="font-size: 1.2em; margin-right: 10px"></i></th>
                        <td><?= "  ",h($service->phone) ?></td>
                    <?php }?>

                    <?php if (!empty($service->email)){?>
                        <th scope="row"<i class="fas fa-envelope fa-fw" style="font-size: 1.2em; margin-right: 10px"></i></th>
                        <td><?= "  ",h($service->email) ?></td>
                    <?php }?>

                </tr>

                <?php if (!empty($service->Hyperlink)){?>
                    <tr>
                        <th scope="row"><i class="fas fa-link fa-fw" style="font-size: 1.2em;"></i></th>

                        <td><a href="<?php echo $hyperlink= ($service->Hyperlink)?>" target="_blank"><b>Click here for more details</b></a></td>

                    </tr>
                <?php }?>

            </table>
            <?php if (!empty($service->desc)){?>
            <table>
                <tr>
                    <div class="row">

                            <td><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 1.2em; margin-right: 10px"></i> <?= h($service->desc) ?></td>

                    </div>
                </tr>
            </table>
            <?php }?>
        </div>
    </div>
    <hr>

    <div align="left">
        <?= $this->Html->link(__('Back'), ['controller' => 'Services', 'action' => 'resident_index'], ['class' => 'btn btn-primary', 'style' => 'margin-right: 0.5em']) ?>
    </div><br>


</div>
