<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Service</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Serivce page</li>
                <li class="breadcrumb-item active">Edit Service</li>
            </ol>
            <?= $this->Form->create($service, ['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Service Name *</label>
                            <div class="ml-2"><?php echo $this->Form->control('name',['label' => false,'class'=>'form-control','required']); ?></div>


                            <label class="col-sm-4 col-form-label">Phone number *</label>
                            <div class="ml-2"><?php echo $this->Form->control('phone', ['label' => false,'class' => 'form-control','type'=>'tel','maxlength'=>10,'required','pattern'=>'[0-9]{8,10}','placeholder'=>'04XXXXXXXX']); ?></div>

                            <label class="col-sm-4 col-form-label">Email *</label>
                            <div class="ml-2"><?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control','required','type'=>'email']); ?></div>

                            <label class="col-sm-4 col-form-label">Website</label>
                            <div class="ml-2"><?php echo $this->Form->control('Hyperlink', ['label' => false, 'class' => 'form-control','type'=>'url']); ?></div>
                            <div class="ml-2"><?php echo $this->Form->control('desc', ['label'=>"Description", 'rows'=>10]); ?></div>

                            <div class="row mt-2">
                                <div class="col-xl-9">
                                    <legend class="col-form-label col-sm-4 pt-0">Upload New Image: </legend>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->text('banner', ['type' => 'file', 'class' => 'form-control-file']); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-xl-9">
                                    <legend class="col-form-label col-sm-4 pt-0">Your current banner: </legend>
                                    <div class="col-sm-8">
                                        <?php echo $this->Html->link('View Current Banner', $service->image_url,['data-lightbox' => 'image'.$service->id]);?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-xl-9">
                                    <legend class="col-form-label col-sm-4 pt-0">Select Categroies: </legend>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->select('categories._ids', $categories, ['multiple' => 'checkbox', 'class' => 'form-check-label']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Services', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
    $(document).on('focusin', '.eventdatep', function(){
        $(this).datepicker({dateFormat:'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            minDate: 0});

    });


</script>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous">
</script>
<script>
    $(function(){
        $('#example').timepicker({
            timeFormat: 'HH:mm',
            interval:5
        });
    });
</script>

<script>
    $(function(){
        $('#example1').timepicker({
            timeFormat: 'HH:mm',
            interval:5
        });
    });

    $(document).ready(function () {
        var i =1;
        $('#add').click(function () {
            i++;
            $('#dynamic_field').append(' <tr id="row'+i+'"><td><input type="type" name="event_date[]"  class="eventdatep form-control name_list "></td><td><button name="remove" id="'+i+'" class="btn btn-primary btn_primary btn_remove">Remove</button></td></tr>');

        })
        $(document).on('click','.btn_remove', function () {
            var button_id=$(this).attr("id");
            $('#row'+button_id+'').remove();
        })

    })
</script>

