<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Service</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Service page</li>
                <li class="breadcrumb-item active">Add Service</li>
            </ol>
            <?= $this->Form->create($service ,['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Service Name *</label>
                            <div class="ml-2"><?php echo $this->Form->control('name',['label' => false,'class'=>'form-control','required']); ?></div>


                            <label class="col-sm-4 col-form-label">Phone number *</label>
                            <div class="ml-2"><?php echo $this->Form->control('phone', ['label' => false,'class' => 'form-control', 'type'=>'tel','maxlength'=>10,'pattern'=>'[0-9]{8,10}' ,'placeholder'=>'04XXXXXXXX','required']); ?></div>
                            </table>

                            <label class="col-sm-4 col-form-label">Email *</label>
                            <div class="ml-2"><?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control','required','type'=>'email']); ?></div>

                            <label class="col-sm-4 col-form-label">Website</label>
                            <div class="ml-2"><?php echo $this->Form->control('Hyperlink', ['label' => false, 'class' => 'form-control','type'=>'url']); ?></div>
                            <div class="ml-2"> <?php echo $this->Form->control('desc', ['label'=>"Description", 'rows'=>10, 'class' => 'form-control']); ?></div>

                            <div class="row mt-2">
                                <div class="col-xl-9">
                                    <legend class="col-form-label col-sm-4 pt-0">Upload New Image: </legend>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->text('banner', ['type' => 'file']); ?>
                                    </div>
                                </div>
                            </div>



                            <div class="row mt-2">
                                <div class="col-xl-9">
                                    <legend class="col-form-label col-sm-4 pt-0">Select Categroies: </legend>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->select('categories._ids', $categories, ['multiple' => 'checkbox', 'class' => 'form-check-label']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Services', 'action' => 'index'], array('style' => 'color:white' , 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>
</main>
</div>

