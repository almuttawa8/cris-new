<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service[]|\Cake\Collection\CollectionInterface $services
 */
?>



<style>
    .searchBar{
        height: calc(2.25rem + 8px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }
</style>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3">Services
    </h1>
    <div class="row">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle ml-3 mt-2" data-toggle="dropdown" type="button" style="width: 51%;height:calc(2.25rem + 8px);">
                Filter by category
            </button>
            <a class="btn btn-primary mt-2" style="color: white;width: 42%;height:calc(2.25rem + 8px); text-align: center;line-height: 32px; " href="<?php echo $this->Url->build([
                "controller" => "Services",
                "action" => "resident_index"
            ]);?>">Clear All Filters</a>
           <div class="dropdown-menu">
                <?php foreach($categories as $category): ?>
                    <a class= "dropdown-item"><?= $this->Form->postLink($category->name, ['action' => 'resident_index', $category->id],array('escape' => false,'style'=>'color: #961982;','class'=>'ml-2 mr-2')) ?></a>
                <?php endforeach ?>
            </div>
        </div>

        <div class="col" align="right">

            <form method="post" name="search" action="<?php echo $this->Url->build([
                "controller" => "Services",
                "action" => "residentIndex"
            ]);?>">

                <input type="text" class="searchBar mt-2" style="width: 50%" name="serviceName" placeholder="Search for services..">
                <input type="submit" class="btn btn-primary" style="height:calc(2.25rem + 8px); margin-top: -5px " value="Search">

            </form>
        </div>
    </div>

    <div class="row mt-4">
        <?php foreach ($services as $service): ?>
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-180" style="height: 31em">
                    <?php if($service->banner != '')
                    {
                        echo $this->Html->image("/files/Services/banner/".$service->banner, ['width'=>'250px', 'height'=>'250px', 'class' =>'card-img-top','url'=> ['action'=> 'view', $service->id]]);

                    }
                    else{
                        echo $this->Html->image('cris-logo.png', ['width'=>'250px', 'height'=>'250px', 'class' =>'card-img-top', 'url'=> ['action'=> 'view', $service->id]]);

                    }?>
                    <div class="card-body">
                        <h4 class="card-title" style="height: 1em">
                            <?= h($service->name) ?>
                        </h4>
                        <p class="card-text" style="padding-top: 50px"> <b>Phone: </b> <?= $service->phone ?></p>
                        <p class="card-text"> <b>Email: </b> <?= $service->email ?></p>

                    </div>
                    <div class="card-footer">
                        <div align="center">
                            <?= $this->Html->link(__('Service details'), ['action' => 'view', $service->id], array('class' => 'btn btn-primary mt-2 mb-2')) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>



    <!-- Pagination -->


    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
            </div>
        </div>

        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?php echo $this->Paginator->numbers();?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
    </div>



</div>


