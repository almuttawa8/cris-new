<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Service[]|\Cake\Collection\CollectionInterface $services
*/

?>




<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Services page</h1>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Categories", ['controller' => 'Categories', 'action' => 'index'], array('class' => 'btn btn-primary', 'style'=> 'width: 120px')) ?>
                <?= $this->Html->link("Add Service", ['action' => 'add'], array('class' => 'btn btn-primary' , 'style'=> 'width: 180px')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Services table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Service</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Hyperlink</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($services as $service): ?>
                                <tr>
                                    <td><?= h($service->name) ?></td>
                                    <td><?= h($service->phone) ?></td>
                                    <td><?= h($service->email) ?></td>
                                    <td><?= h($service->Hyperlink) ?></td>
                                    <td><?= h($service->desc) ?></td>
                                    <td class="actions" align="center">
                                        <a class="btn btn-primary mb-1" href="<?php echo $this->Url->build([
                                            "controller" => "Services",
                                            "action" => "edit",
                                            $service->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal<?=$service->id ?>"">Delete</button>

                                        <div class="modal fade" id="confirmModal<?=$service->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this service - <?= $service->name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $service->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>



