<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calendar[]|\Cake\Collection\CollectionInterface $calendars
 */
?>



<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Noticeboard
    </h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a>Noticeboard</a>
        </li>
        <li class="breadcrumb-item active">Calendars</li>
    </ol>

    <div align="center">
        <b><?= $this->Html->link(__('Newsletters'), ['controller' => 'Newsletters', 'action' => 'resident_index'], array('class' => 'btn btn-primary mt-2')) ?></b>
        <?= $this->Html->link(__('Minutes'), ['controller' => 'Minutes', 'action' => 'resident_index'], array('class' => 'btn btn-primary mt-2 ml-2')) ?>
        <?= $this->Html->link(__('Calendars'), ['controller' => 'Calendars', 'action' => 'resident_index'], array('class' => 'btn btn-secondary mt-2 ml-2')) ?>
        <?= $this->Html->link(__('Food Menus'), ['controller' => 'Menus', 'action' => 'resident_index'], array('class' => 'btn btn-primary mt-2 ml-2')) ?>
        <?= $this->Html->link(__('Others'), ['controller' => 'Miscellaneous', 'action' => 'resident_index'], array('class' => 'btn btn-primary mt-2 ml-2')) ?>
    </div>

    <div class="alert alert-warning mt-4" role="alert">
        <b>Note</b> : By clicking on a file, a PDF document will be opened in a new tab. When you are done reading the PDF document simply close the tab down, not the window.
    </div>


    <div class="table-responsive">
        <div class="calendars index large-9 medium-8 columns content">

            <table class="table table-bordered" id="datatable">
                <thead>
                <tr>
                    <th scope="col" style="color:#c507ac">Calendars</th>
                    <th scope="col" style="color:#c507ac">Date Published</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($calendars as $calendar): ?>
                    <tr>
                        <td><?= $this->Html->link($calendar->display_field, "/webroot/files/Calendars/file_name/".$calendar->file_name, ['target'=>'_blank']) ?></td>
                        <td data-order="<?= h($calendar->date_published->format("Y/m/d")) ?>" data-sort="<?= h($calendar->date_published->format("Y/m/d")) ?>">
                            <?= h($calendar->date_published->format("d M, Y")) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>

</div>



