<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Property[]|\Cake\Collection\CollectionInterface $properties
 */
?>


<div id="dataTable_filter" class="dataTables_filter" align="center">
    <?= $this->Html->link("Residents", ['controller' => 'Residents', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
    <b><?= $this->Html->link("Properties", ['controller' => 'Properties', 'action' => 'index'], array('class' => 'btn btn-primary1')) ?></b>
    <?= $this->Html->link("Clubs", ['controller' => 'Clubs', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
    <?= $this->Html->link("Interests", ['controller' => 'Interests', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
</div>

<div id="dataTable_filter" class="dataTables_filter" align="right">
    <button class="btn btn-primary"><?= $this->Html->link("Add Property", ['action' => 'add'], array('style' => 'color:white')) ?></button>
    <button class="btn btn-primary"><?= $this->Html->link("Import Properties", ['action' => 'import'], array('style' => 'color:white')) ?></button>
</div>



<div class="table-responsive">
    <div class="properties index large-9 medium-8 columns content">
        <h3><?= __('Properties') ?></h3>
        <table class="table table-bordered" id="dataTable">
            <thead>
                <tr>
                    <th scope="col" style="color:#c507ac">Address</th>
                    <th scope="col" style="color:#c507ac">Type</th>
                    <th scope="col" style="color:#c507ac">Landline Phone</th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($properties as $property): ?>
                <tr>
                    <td><?= h($property->address) ?></td>
                    <td><?= $property->type ?></td>
                    <td><?= $property->landline ?></td>
                    <td class="actions" width="30%" align="center">
                        <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link(__('Edit'), ['action' => 'edit', $property->id], array('style' => 'color:white')) ?></button>

                        <button type ="button" class="btn btn-primary1" data-toggle="modal" data-target="#confirmModal-<?= $property->id?>">Delete</button>

                        <div class="modal fade" id="confirmModal-<?= $property->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to delete this property - <?= $property->address?>?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"><?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $property->id]) ?></button>
                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                <?php endforeach; ?>
                <script>
                    $(document).ready(function () {
                        $('#confirmModal').modal('show');
                        $("#confirmModal").appendTo("body");
                    });
                </script>
            </tbody>
        </table>

    </div>
</div>
