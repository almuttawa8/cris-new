<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Property $property
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <button class="btn btn-primary"><?= $this->Html->link(__('Back'), ['controller' => 'Properties', 'action' => 'index'], array('style' => 'color:white')) ?></button>
</div>
<div class="properties form large-9 medium-8 columns content">
    <?= $this->Form->create($property) ?>
    <legend><?= __('Add Property') ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <div class="col-sm-6">
                    <div class="indicatorDefault">
                        * Indicates required field
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Address *</label>
                    <div class="col-sm-5">
                        <?php echo $this->Form->control('address', ['label' => false, 'placeholder'=>'Address']); ?>
                    </div>
                </div>

                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-4 pt-0">Type *</legend>
                        <div class="col-sm-8">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="Apartment" value="Apartment" checked>
                                <label class="form-check-label" for="Apartment">Apartment</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="Villa" value="Villa">
                                <label class="form-check-label" for="Villa">Villa</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="Service Apartment" value="Service Apartment">
                                <label class="form-check-label" for="Service Apartment">Service Apartment</label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Landline *</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="landline" placeholder="Landline" required>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
