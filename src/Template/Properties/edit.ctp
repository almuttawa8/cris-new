<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Property $property
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
$this->Form->setTemplates([
    'radio' => '<input{{attrs}}class="form-check-input" type="radio" name="{{name}}" value="{{value}}">',
    'nestingLabel' => '<div{{attrs}}class="form-check form-check-inline">{{input}}<label class="form-check-label" for="inlineRadio1">{{text}}</label></div>'
]);
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <button class="btn btn-primary"><?= $this->Html->link(__('Back'), ['controller' => 'Properties', 'action' => 'index'], array('style' => 'color:white')) ?></button>
</div>
<div class="properties form large-9 medium-8 columns content">
    <?= $this->Form->create($property) ?>
    <legend><?= __('Edit Property') ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <div class="col-sm-6">
                    <div class="indicatorDefault">
                        * Indicates required field
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Address *</label>
                    <div class="col-sm-5">
                        <?php echo $this->Form->control('address', ['label' => false]); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Type *</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->control('type',['type'=>'radio','options' => [
                            ['value' => 'Apartment', 'text' => __('Apartment')],
                            ['value' => 'Villa', 'text' => __('Villa')],
                            ['value' => 'Service Apartment', 'text' => __('Service Apartment')]], 'default' => 'Apartment', 'label'=>false]); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Landline *</label>
                    <div class="col-sm-5">
                        <?php echo $this->Form->control('landline', ['label' => false]); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
