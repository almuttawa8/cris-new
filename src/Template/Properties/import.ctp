<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Property $property
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <button class="btn btn-primary"><?= $this->Html->link(__('Back'), ['controller' => 'Properties', 'action' => 'index'], array('style' => 'color:white')) ?></button>
    <button class="btn btn-primary"><?= $this->Html->link(__('Download Sample File'),'/files/PropertyCSV/file_name/Property Sample.csv' , array('download' => 'Property Sample.csv','style' => 'color:white')) ?></button>
</div>
<div class="properties form large-9 medium-8 columns content">
    <?= $this->Form->create(null,["type"=>"file"]) ?>
    <legend><?= __('Import CSV') ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <br>

                <div class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-4 pt-0">Upload New File: </legend>
                        <div class="col-sm-8">
                            <?php echo $this->Form->control('file', ["type"=>"file", 'label' => false]); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
