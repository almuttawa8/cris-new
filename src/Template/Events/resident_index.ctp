<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event[]|\Cake\Collection\CollectionInterface $events
 */
?>



<style>
    .searchBar{
        height: calc(2.25rem + 8px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }
</style>

<!-- Page Content -->
<div class="container">

    <div style="color: #961982;"><b><?= $this->Flash->render('success_register') ?>
            <?= $this->Flash->render('re_register') ?>
            <?= $this->Flash->render('full_register') ?></b>
    </div>

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Events
    </h1>
    <div class="row">
        <div class="col">
            <div>
                <?= $this->Html->link(__('My Registrations'), ['controller' => 'Registrations','action' => 'resident_index'], array('class' => 'btn btn-primary','style'=>'width: 308px')) ?>
                </center> <p>
                    <?php $this->Paginator->setTemplates([
                        'sort' => '<a class="btn btn-primary mt-2 mr-1" style="" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?php $this->Paginator->setTemplates([
                        'sortAsc' => '<a class="btn btn-primary mt-2" style="" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?php $this->Paginator->setTemplates([
                        'sortDesc' => '<a class="btn btn-primary mt-2" style="" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?= $this->Paginator->sort('name', 'Sort by name') ?>
                    <?= $this->Paginator->sort('event_date', 'Sort by date')?>
                </p></center>
            </div>
        </div>
        <div class="col" align="right">
            <form method="post" name="event" action="<?php echo $this->Url->build([
                "controller" => "Events",
                "action" => "residentIndex"
            ]);?>">
                <input type="text" class="searchBar mt-2" style="width: 50%" name="eventName" placeholder="Search for events...">
                <input type="submit" class="btn btn-primary" style="height:calc(2.25rem + 8px); margin-top: -5px " value="Search">
            </form>
        </div>
    </div>


    <div class="row">
        <?php foreach ($events as $event): ?>
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-180 mt-2">
                    <?php if($event->image_name != '')
                    {

                        echo $this->Html->image("/files/Events/image_name/".$event->image_name,['height'=>'250px', 'class' =>'card-img-top', 'url'=> ['action'=> 'view', $event->id]]);


                    }
                    else{
                        echo $this->Html->image('cris-logo.png', ['height'=>'250px', 'class' =>'card-img-top', 'url' => ['action'=>'view', $event->id]]);

                    }?>
                    <div class="card-body">
                        <h9 class="card-title">
                           <b> <?= h($event->name) ?></b>
                        </h9>
                        <p class="card-text"><i class="fas fa-calendar-day fa-fw"></i><?= "  ",h($event->event_date->format("d M, Y")) ?></p>
                        <p class="card-text"><i class="far fa-clock fa-fw"></i><?= "  ",h($event->start_time->format("h:i A")) ?> - <?= "  ",h($event->end_time->format("h:i A")) ?></p>
                        <p class="card-text"> <i class="fas fa-dollar-sign fa-fw"></i><?= "  ",$this->Number->format($event->cost) ?></p>
                        <p class="card-text"><i class="fas fa-users fa-fw"></i><?= "  ",$this->Number->format($event->availability) - $event->count_resident ?></p>
                    </div>
                    <div class="card-footer">
                        <div align="center">
                                <?= $this->Html->link(__('Event details'), ['action' => 'view', $event->id], array('class' => 'btn btn-primary mt-2 mb-2')) ?>
                                <?= $this->Html->link(__('Register'), ['controller' => 'Registrations','action' => 'add', $event->id], array('class' => 'btn btn-primary mt-2 mb-2')) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>



    <!-- Pagination -->


    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
            </div>
        </div>

                <ul class="pagination">
                       <?= $this->Paginator->prev('< ' . __('previous')) ?>
                       <?php echo $this->Paginator->numbers();?>
                     <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>


    </div>



</div>

