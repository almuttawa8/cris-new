<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
$this->layout = 'resident';
?>
<style>
    .eventImage{
        height:300px;
        width:600px
    }

    @media only screen and (max-width: 730px) {
        .eventImage{
            height:300px;
            width:100%;
        }
    }



</style>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3"><?= h($event->name) ?>
    </h1>


    <!-- Project One -->
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php if($event->image_name != '')
            {
                echo $this->Html->image("/files/Events/image_name/".$event->image_name, array('alt' => '', 'class' =>'card-img-top eventImage'));
            }
            else{
                echo $this->Html->image('cris-logo.png',array('class' =>'card-img-top eventImage'));

            }?>
        </div>
    </div>
    <div class="row mt-2">

        <div class="col-md-5">

            <table>
            <tr>
                <th scope="row"><i class="fas fa-calendar-day fa-fw" style="font-size: 1.2em;margin-right: 10px"></i></th>
                <td><?= "  ",h($event->event_date->format("d M, Y")) ?></td>
                <th scope="row"><i class="far fa-clock fa-fw"style="font-size: 1.2em;"></i></th>
                <td><?= "  ",h($event->start_time->format("h:i A")) ?> - <?= h($event->end_time->format("h:i A")) ?></td>
            </tr>
            <tr>
                <th scope="row"><i class="fas fa-map-marked-alt fa-fw" style="font-size: 1.2em;"></i></th>
                <td><?= "  ",$event->location->name ?></td>

                <th scope="row"><i class="fas fa-dollar-sign fa-fw" style="font-size: 1.2em;"></i></th>
                <td><?= "  ",$this->Number->format($event->cost) ?></td>
            </tr>
                <tr>
                    <th scope="row"><i class="fas fa-utensils fa-fw" style="font-size: 1.2em; margin-right: 10px"></i></th>
                    <td><?= "  ",h($event->catering_option) ?></td>

                    <th scope="row"<i class="fas fa-coffee fa-fw" style="font-size: 1.2em;"></i></th>
                    <td><?= "  ",h($event->beverage_option) ?></td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-wheelchair fa-fw" style="font-size: 1.2em;"></i></th>
                    <td><?= "  ",h($event->accessibility_option) ?></td>

                    <th scope="row"><i class="fas fa-users fa-fw" style="font-size: 1.2em;"></i></th>
                    <td><?= "  ",$this->Number->format($event->availability) - $event->count_resident ?></td>
                </tr>
                <?php if (!empty($event->Hyperlink)){?>
                    <tr>
                        <th scope="row"><i class="fas fa-link fa-fw" style="font-size: 1.2em;"></i></th>

                        <td><a href="<?php echo $hyperlink= ($event->Hyperlink)?>" target="_blank"><b>Click here for more details</b></a></td>

                    </tr>
                <?php }?>
            </table>
            <?php if (!empty($event->desc)){?>
                <table>
                    <tr>
                        <div class="row">

                            <td><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 1.2em; margin-right: 10px"></i> <?= h($event->desc) ?></td>

                        </div>
                    </tr>
                </table>
            <?php }?>

        </div>
    </div>
    <hr>

    <div align="left">
        <?= $this->Html->link(__('Back'), ['controller' => 'Events', 'action' => 'resident_index'], ['class' => 'btn btn-primary', 'style' => 'margin-right: 0.5em']) ?>
        <?= $this->Html->link("Register", ['controller' => 'Registrations','action' => 'add',$event->id], ['class' => 'btn btn-primary']) ?>
    </div><br>


</div>

