<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>


<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Event</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Events page</li>
                <li class="breadcrumb-item active">Edit Event</li>
            </ol>
            <?= $this->Form->create($event, ['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Event Name *</label>
                            <div class="ml-2"><?php echo $this->Form->control('name',['label' => false]); ?></div>


                            <label class="col-sm-4 col-form-label">Event Date *</label>
                            <div class="ml-2"><?php echo $this->Form->control('event_date',['id'=>'datepicker','type'=>'text', 'label'=>false , 'readOnly']); ?></div>
                            </table>

                            <label class="col-sm-4 col-form-label">Start Time *</label>
                            <div class="ml-2"><?php echo $this->Form->control('start_time', ['label'=>false, 'id'=>'example', 'type'=>'text', 'readOnly']); ?></div>

                            <label class="col-sm-4 col-form-label">End Time *</label>
                            <div class="ml-2"><?php echo $this->Form->control('end_time', ['label'=>false, 'id'=>'example1', 'type'=>'text', 'readOnly']); ?></div>

                            <label class="col-sm-4 col-form-label">Location *</label>
                            <div class="ml-2"><?php echo $this->Form->control('location_id', ['options' => $locations, 'label'=> false, 'class' => 'form-control form-control-lg']); ?></div>

                            <label class="col-sm-4 col-form-label">Cost *</label>
                            <div class="ml-2"><?php echo $this->Form->control('cost', ['label' => false,'min'=>0]); ?></div>

                            <label class="col-sm-4 col-form-label">Availability *</label>
                            <div class="ml-2"><?php echo $this->Form->control('availability', ['label' => false,'min'=>0]); ?></div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Accessibility *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <?php echo $this->Form->control('accessibility', ['type'=>'radio', 'options' => [
                                            ['value' => 'Y', 'text' => __('Yes')],
                                            ['value' => 'N', 'text' => __('No')],
                                        ],
                                            'label'=>false,
                                        ]); ?>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Catering *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <?php echo $this->Form->control('catering', ['type'=>'radio','options' => [
                                            ['value' => 'Y', 'text' => __('Yes')],
                                            ['value' => 'N', 'text' => __('No')],
                                        ],
                                            'label'=>false,
                                        ]); ?>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Coffee/Tea *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <?php echo $this->Form->control('beverage', ['type'=>'radio','options' => [
                                            ['value' => 'Y', 'text' => __('Yes')],
                                            ['value' => 'N', 'text' => __('No')],
                                        ],
                                            'label'=>false,
                                        ]); ?>
                                    </div>
                                </div>
                            </fieldset>

                            <label class="col-sm-4 col-form-label">Website</label>
                            <div class="ml-2"> <?php echo $this->Form->control('Hyperlink', ['label' => false, 'type'=>'url', 'placeholder'=>'https://www.website.com/']); ?></div>

                            <div class="ml-2"><?php echo $this->Form->control('desc', ['label'=>"Description", 'rows'=>10]); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-xl-9">
                    <legend class="col-form-label col-sm-4 pt-0">Upload New Image: </legend>
                    <div class="col-sm-8">
                        <?php echo $this->Form->text('image_name', ['type' => 'file']); ?>
                    </div>
                </div>
            </div>



        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Events', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>
