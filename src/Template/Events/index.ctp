<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event[]|\Cake\Collection\CollectionInterface $events
 */
?>
<?= $this->Flash->render('notify_resident') ?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Events page</h1>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Add Event", ['action' => 'add'], array('class' => 'btn btn-primary','style'=>'width: 150px')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Events table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Event Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Cost</th>
                                <th>Attendees</th>
                                <th>Locations</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($events as $event): ?>
                                <tr>
                                    <td><?= h($event->name) ?></td>
                                    <td>
                                        <?= h($event->event_date->format("d M, Y")) ?>
                                    </td>
                                    <td><?= h($event->start_time->format("h:i A")) ?></td>
                                    <td><?= h($event->end_time->format("h:i A")) ?></td>
                                    <td><?= "$",$this->Number->format($event->cost) ?></td>
                                    <td><?= $event->count_resident?></td>
                                    <td><?= $event->location->name?></td>
                                    <td class="actions" align="center">
                                        <a class="btn btn-primary mb-1" href="<?php echo $this->Url->build([
                                            "controller" => "Events",
                                            "action" => "edit",
                                            $event->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal<?=$event->id ?>"">Archive</button>

                                        <div class="modal fade" id="confirmModal<?=$event->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this event <?php echo $event->name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'archive', $event->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


