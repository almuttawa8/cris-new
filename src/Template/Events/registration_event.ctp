<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<div id="dataTable_filter" class="dataTables_filter" align="left" style="margin-bottom: 0.5em">
    <button class="btn btn-primary"><?= $this->Html->link(__('Back'), ['controller' => 'Registrations', 'action' => 'index'], array('style' => 'color:white')) ?></button>
</div>


        <div class="col-md-12">
            <div class="card">
                <div class="related">
                    <div class="card-header"> Registrations for <b><?= $event->name?></b></div>
                    <div class="card-body">
                        <p>Number of Registrations: <?= $total ?></p>
                    <?php if (!empty($event->residents)): ?>
                        <table class="table table-bordered table-hover" id="dataTable">
                            <tr>
                                <th scope="col"><?= __('First Name') ?></th>
                                <th scope="col"><?= __('Last Name') ?></th>
                                <th scope="col"><?= __('Email') ?></th>
                                <th scope="col"><?= __('Phone') ?></th>
                            </tr>
                            <?php foreach ($registrations as $registration): ?>
                                <tr>
                                    <td><?= h($registration->Resident->fname) ?></td>
                                    <td><?= h($registration->Resident->lname) ?></td>
                                    <td><?= h($registration->Resident->email) ?></td>
                                    <td><?= h($registration->Resident->phone) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>



<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({dateFormat:'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate: 0});
    } );
</script>


