<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>

<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Events</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Events page</li>
                <li class="breadcrumb-item active">Add Events</li>
            </ol>
            <?= $this->Form->create($event, ['type' => 'file'])  ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Event Name *</label>
                            <input type="text" class="form-control ml-2" name="name" placeholder="Event Name" required>

                            <label class="col-sm-4 col-form-label">Event Date *</label>
                            <table class="table table-bordered ml-2" id="dynamic_field">
                                <tr>
                                    <td><input name="event_date[]"  autocomplete="off"  value="<?php echo date('d-m-yy')?>" class="eventdatep form-control name_list"  readonly>
                                    <td><button name="add" id="add" class="btn btn-primary" style="width: 200px">Add Another Date</button></td>
                                </tr>
                            </table>

                            <label class="col-sm-4 col-form-label">Start Time *</label>
                            <div class="ml-2"> <?php echo $this->Form->control('start_time', ['label'=>false, 'id'=>'example', 'type'=>'text', 'autocomplete'=>'off', 'readOnly','value'=>'00:00']); ?></div>

                            <label class="col-sm-4 col-form-label">End Time *</label>
                            <div class="ml-2"> <?php echo $this->Form->control('end_time', ['label'=>false, 'id'=>'example1', 'type'=>'text', 'readOnly','value'=>'00:00']); ?></div>

                            <label class="col-sm-4 col-form-label">Location *</label>
                                <div class="ml-2"> <?php echo $this->Form->control('location_id', ['options' => $locations, 'label' => false, 'class' => 'form-control form-control-lg']); ?></div>

                            <label class="col-sm-4 col-form-label">Cost *</label>
                            <input type="number" min="0" step="0.01" class="form-control ml-2" name="cost" placeholder="$" required>

                            <label class="col-sm-4 col-form-label">Availability *</label>

                            <input type="number" min="0" class="form-control ml-2" name="availability" placeholder="#" required>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Accessibility *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <label class="container1">Yes
                                            <input type="radio" name="accessibility" value="Y" id="Y">
                                            <span class="checkmark1 mt-1" for="Y"></span>
                                        </label>
                                        <label class="container1">No
                                            <input type="radio" name="accessibility" value="N" id="N" checked="checked">
                                            <span class="checkmark1 mt-1" for="N"></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Catering *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <label class="container1">Yes
                                            <input type="radio" name="catering" value="Y" id="Y">
                                            <span class="checkmark1 mt-1" for="Y"></span>
                                        </label>
                                        <label class="container1">No
                                            <input type="radio" name="catering" value="N" id="N" checked="checked">
                                            <span class="checkmark1 mt-1" for="N"></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Coffee/Tea *</legend>
                                    <div class="col-sm-8 ml-2">
                                        <label class="container1">Yes
                                            <input type="radio" name="beverage" value="Y" id="Y">
                                            <span class="checkmark1 mt-1" for="Y"></span>
                                        </label>
                                        <label class="container1">No
                                            <input type="radio" name="beverage" value="N" id="N" checked="checked">
                                            <span class="checkmark1 mt-1" for="N"></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                            <label class="col-sm-4 col-form-label">Website</label>
                            <input type="url" class="form-control ml-2" name="Hyperlink" placeholder="https://www.website.com/">

                            <div class="ml-2"> <?php echo $this->Form->control('desc', ['label'=>"Description", 'rows'=>10, 'class' => 'form-control']); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-xl-9">
                    <legend class="col-form-label col-sm-4 pt-0">Upload New Image: </legend>
                    <div class="col-sm-8">
                        <?php echo $this->Form->text('image_name', ['type' => 'file']); ?>
                    </div>
                </div>
            </div>



        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Events', 'action' => 'index'], array('style' => 'color:white' , 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>
<script>
    n =  new Date();
    y = n.getFullYear();
    m = n.getMonth() + 1;
    d = n.getDate();
    document.getElementById("date").innerHTML = m + "/" + d + "/" + y;
</script>


