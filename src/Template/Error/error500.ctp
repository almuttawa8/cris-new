<?php
/**
 * Created by PhpStorm.
 * User: bcho27
 * Date: 19-May-19
 * Time: 5:19 PM
 */
$this->layout = 'login';
?>
<br><br>
<div class="container">
    <div class="jumbotron">
        <div align="center">
            <h2>Sorry, Something went wrong :(</h2>
        </div>
    </div>
    <center><div>
            <?= $this->Html->link(__('Back'), ['controller' => 'Home', 'action' => 'resident_index'], ['class' => 'btn btn-primary', 'style' => 'width: 200px;']) ?>
        </div><br></center>
</div>
<footer class="footer py-3 fixed-bottom" style="background-color: #961982">
    <div class="container">
        <p class="m-0 text-center text-white" style="color: white">Copyright © Classic Residents Brighton 2020</p>
    </div>
</footer>
