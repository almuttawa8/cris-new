<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MinCategory[]|\Cake\Collection\CollectionInterface $minCategories
 */
?>





    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Minutes Categories page</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Minutes</li>
                    <li class="breadcrumb-item active">Minutes Categories page</li>
                </ol>

                <div class="mt-4 mb-4" align="right">
                    <?= $this->Html->link("Back", ['controller'=>'Minutes','action' => 'index'], array('style' => 'color:white', 'class'=>'btn btn-primary')) ?>

                    <?= $this->Html->link("Add Category", ['action' => 'add'], array('style' => 'color:white', 'class'=>'btn btn-primary', 'style'=> 'width: 180px')) ?>
                </div>
                <div class="card mb-4 w-125">
                    <div class="card-header"><i class="fas fa-table mr-1"></i>Categories table</div>
                    <div class="card-body w-100">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($minCategories as $minCategory): ?>
                                    <tr>
                                       <td><?= h($minCategory->name) ?></td>
                                       <td class="actions" width="50%" align="center">
                                            <a class="btn btn-primary mb-1" href="
                                            <?php echo $this->Url->build([
                                                "controller" => "MinCategories",
                                                "action" => "edit",
                                                $minCategory->id
                                            ]);?>">Edit</a>
                                            <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?=$minCategory->id ?>" data-id="<?php echo $minCategory->id?>" id="<?php echo $minCategory->id ?>">Archive</button>

                                            <div class="modal fade" id="confirmModal-<?=$minCategory->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Are you sure you want to archive this minute category - <?= $minCategory->name?>?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $minCategory->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                            <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>


