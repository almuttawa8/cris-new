<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MinCategory $minCategory
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Newsletter $newsletter
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
$this->Form->setTemplates([
    'radio' => '<input{{attrs}}class="form-check-input" type="radio" name="{{name}}" value="{{value}}">',
    'nestingLabel' => '<div{{attrs}}class="form-check form-check-inline">{{input}}<label class="form-check-label" for="inlineRadio1">{{text}}</label></div>'
]);
?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Minutes Categories</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Minutes</li>
                <li class="breadcrumb-item active">Minutes Categories page</li>
                <li class="breadcrumb-item active">Edit Minutes Categories</li>
            </ol>
            <?= $this->Form->create($minCategory) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <label class="col-sm-4 col-form-label">Name: </label>
                            <div class="ml-2"> <?php echo $this->Form->control('name', ['label' => false]); ?></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['action' => 'index'], array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>


