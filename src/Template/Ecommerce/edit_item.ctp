<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce $ecommerce
 */
$this->Flash->render('success_edit_registration');
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
$this->Form->setTemplates([
    'radio' => '<input{{attrs}}class="form-check-input" type="radio" name="{{name}}" value="{{value}}">',
    'nestingLabel' => '<div{{attrs}}class="form-check form-check-inline">{{input}}<label class="form-check-label" for="inlineRadio1">{{text}}</label></div>'

]);?>

<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3">Edit <?= $ecommerce->name?> form
    </h1>

    <?= $this->Form->create($ecommerce, ['type' => 'file'])  ?>
    <fieldset>
        <div class="row ml-2">
            <div class="col-lg-8 mb-4">
                <div class="container">
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Product name*</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('name', ['label' => false, 'class' => 'form-control']);?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Product cost</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('cost', ['label' => false, 'class' => 'form-control']);?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Email*</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control']);?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Phone number</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('phone', ['label' => false, 'class' => 'form-control']);?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Website</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('website', ['label' => false, 'class' => 'form-control', 'placeholder' => 'https://www.website.com']);?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Description</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->input('desc', ['rows'=>10, 'label' => false,'class' => 'form-control'])?>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-xl-9">
                            <legend class="col-form-label col-sm-4 pt-0">Upload Item Image: </legend>
                            <div class="col-sm-8">
                                <?php echo $this->Form->text('image_name', ['type' => 'file']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </fieldset>
    <div class="form-row">
        <div class="form-group col-md-6">
        </div>
    </div>
    <div align="left">
        <?= $this->Html->link(__('Back'), ['controller' => 'Ecommerce', 'action' => 'my_items'], array('class' => 'btn btn-primary')) ?>

        <?= $this->Form->button(__('Submit')) ?>
    </div>

</div>
</div>
</div>

<?= $this->Form->end() ?>



</div>
