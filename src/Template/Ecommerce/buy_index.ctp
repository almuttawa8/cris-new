<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce[]|\Cake\Collection\CollectionInterface $ecommerce
 */
?>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Want to buy?
    </h1>
    <div class="row">
        <div class="col">
            <div>
                <?= $this->Html->link(__('Want to sale'), ['controller' => 'Ecommerce','action' => 'resident_index'], array('class' => 'btn btn-primary')) ?>
                <br>   <?= $this->Html->link(__('Apply for listing'), ['controller' => 'Ecommerce','action' => 'apply'], array('class' => 'btn btn-primary mt-2')) ?>
                <?= $this->Html->link(__('My Items'), ['controller' => 'Ecommerce','action' => 'my_items'], array('class' => 'btn btn-primary mt-2')) ?>



            </div>
        </div>
    </div><br>

    <div class="row">
        <?php foreach ($ecommerce as $item): ?>
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-180">
                    <?php if($item->image_name != '')
                    {

                        echo $this->Html->image("/files/Ecommerce/image_name/".$item->image_name,['height'=>'250px', 'class' =>'card-img-top']);


                    }
                    else{
                        echo $this->Html->image('cris-logo.png', ['height'=>'250px', 'class' =>'card-img-top']);

                    }?>
                    <div class="card-body">
                        <h3 class="card-title">
                            <b> <?= h($item->name) ?></b>
                        </h3>
                        <p class="card-text" style="padding-top: 10px"> <b>Description: </b> <?= $item->desc ?></p>

                    </div>
                    <div class="card-footer">
                        <div align="center">
                            <?= $this->Html->link(__('More details'), ['action' => 'view', $item->id], array('class' => 'btn btn-primary mt-2 mb-2')) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- Pagination -->


    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
            </div>
        </div>

        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?php echo $this->Paginator->numbers();?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
    </div>

</div>
