<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce $ecommerce
 */
$this->layout = 'resident';
?>
<style>
    .itemImage{
        height:300px;
        width:600px
    }

    @media only screen and (max-width: 730px) {
        .itemImage{
            height:300px;
            width:100%;
        }
    }



</style>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3"><?= h($ecommerce->name) ?>
    </h1>


    <!-- Project One -->
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php if($ecommerce->image_name != '')
            {
                echo $this->Html->image("/files/Ecommerce/image_name/".$ecommerce->image_name, array('alt' => '','class' =>'card-img-top itemImage'));
            }
            else{
                echo $this->Html->image('cris-logo.png',array('class' =>'card-img-top itemImage'));

            }?>
        </div>
    </div>
    <div class="row mt-2">

        <div class="ml-3">

            <table>
                <tr>
                    <?php if (!empty($ecommerce->phone)){?>
                        <th scope="row"><i class="fas fa-phone-square fa-fw" style="font-size: 1.2em; margin-right: 10px"></i></th>
                        <td><?= "  ",h($ecommerce->phone) ?></td>
                    <?php }?>

                    <?php if (!empty($ecommerce->email)){?>
                        <th scope="row"<i class="fas fa-envelope fa-fw" style="font-size: 1.2em; margin-right: 10px"></i></th>
                        <td><?= "  ",h($ecommerce->email) ?></td>
                    <?php }?>

                </tr>
                <tr>
                    <?php if (!empty($ecommerce->cost)){?>
                        <th scope="row"<i class="fas fa-dollar-sign" style="font-size: 1.2em; margin-right: 10px"></i></th>
                        <td><?= "  ",h($ecommerce->cost) ?></td>
                    <?php }?>

                <?php if (!empty($ecommerce->website)){?>

                        <th scope="row"><i class="fas fa-link fa-fw" style="font-size: 1.2em;"></i></th>

                        <td><a href="<?php echo $website= ($ecommerce->website)?>" target="_blank"><b>Click here for more details</b></a></td>


                <?php }?></tr>

            </table>
            <?php if (!empty($ecommerce->desc)){?>
                <table>
                    <tr>
                        <div class="row">

                            <td><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 1.2em; margin-right: 10px"></i> <?= h($ecommerce->desc) ?></td>

                        </div>
                    </tr>
                </table>
            <?php }?>
        </div>
    </div>
    <hr>

    <div align="left">

        <?= $this->Html->link(__('Back'), ['controller' => 'Ecommerce', 'action' => 'resident_index'], ['class' => 'btn btn-primary', 'style' => 'margin-right: 0.5em']) ?>

    </div><br>


</div>
