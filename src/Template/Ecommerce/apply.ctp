<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce $ecommerce
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>






<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<!-- Page Content -->
<div class="container">
    <div style="color: #961982;"><b><?= $this->Flash->render('success_enquiry') ?>
            <?= $this->Flash->render('fail_enquiry') ?></b></div>

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">
        Marketplace form
    </h1>
<br>


    <?= $this->Form->create($ecommerce, ['type' => 'file'])  ?>
    <div class="row ml-2">
        <div class="col-lg-8 mb-4">
            <div class="alert alert-warning" role="alert">
                * Indicates required field
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Product name*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('name', ['label' => false, 'class' => 'form-control','required']);?>

                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Product cost</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('cost', ['label' => false, 'class' => 'form-control','min'=>'0','type'=>'number','step'=>'0.01']);?>
                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Email</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control','type'=>'email']);?>
                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Phone number*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('phone', ['label' => false, 'class' => 'form-control','type'=>'tel','placeholder'=>'04XXXXXXXX', 'required','pattern'=>'[0-9]{8,10}','maxlength'=>10]);?>
                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Product Website</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('website', ['label' => false, 'class' => 'form-control', 'placeholder' => 'https://www.website.com','type'=>'url']);?>
                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Description*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->input('desc', ['rows'=>10, 'label' => false,'class' => 'form-control','required','maxlength'=> '300'])?>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <legend class="col-form-label col-sm-4 pt-0">Upload Item Image: </legend>
                    <div class="col-sm-8">
                        <?php echo $this->Form->text('image_name', ['type' => 'file']); ?>
                    </div>
                </div>
            </div><br>


            <div class="alert alert-warning" role="alert">
                When submitting the form, admin or marketplace admin will need to accept your application in order to be shown in marketplace page.
            </div>
        </div>

    </div>
    <div align="left" style="margin-bottom: 0.5em">
        <?= $this->Html->link(__('Back'), ['controller' => 'Ecommerce', 'action' => 'resident_index'], array('style' => 'color:white', 'class' => 'btn btn-primary ml-4 mt-2')) ?>
        <input class="btn btn-primary ml-4 mt-2" type="submit" value="Submit">
    </div>

    <?= $this->Form->end() ?>

</div>
<script>

</script>
