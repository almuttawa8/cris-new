<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce[]|\Cake\Collection\CollectionInterface $ecommerce
 */
?>





<style>
    .searchBar{
        height: calc(2.25rem + 8px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }
</style>

<!-- Page Content -->
<div class="container">
    <div style="color: #961982;"><b><?= $this->Flash->render('success_enquiry') ?>
            <?= $this->Flash->render('fail_enquiry') ?></b></div>

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Marketplace
    </h1>
    <div class="row">
        <div class="col">

                   <?= $this->Html->link(__('Apply for listing'), ['controller' => 'Ecommerce','action' => 'apply'], array('class' => 'btn btn-primary mt-2','style'=>'height:calc(2.25rem + 8px);text-align: center;line-height: 32px;')) ?>
                   <?= $this->Html->link(__('My Items'), ['controller' => 'Ecommerce','action' => 'my_items'], array('class' => 'btn btn-primary mt-2','style'=>'height:calc(2.25rem + 8px);text-align: center;line-height: 32px;')) ?>
        </div>

        <div class="col" align="right">

            <form method="post" name="search" action="<?php echo $this->Url->build([
                "controller" => "Ecommerce",
                "action" => "residentIndex"
            ]);?>">

                <input type="text" class="searchBar mt-2" style="width: 45%" name="itemName" placeholder="Search for items..">
                <input type="submit" class="btn btn-primary" style="height:calc(2.25rem + 8px); margin-top: -5px " value="Search">

            </form>
        </div>
    </div><br>

    <div class="row">
        <?php foreach ($ecommerce as $item): ?>
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-180" style="height: 31em">
                    <?php if($item->image_name != '')
                    {

                        echo $this->Html->image("/files/Ecommerce/image_name/".$item->image_name,['height'=>'250px', 'class' =>'card-img-top','url'=> ['action'=> 'view', $item->id]]);
                    }
                    else{
                        echo $this->Html->image('cris-logo.png', ['height'=>'250px', 'class' =>'card-img-top','url'=> ['action'=> 'view', $item->id]]);

                    }?>
                    <div class="card-body">
                        <h3 class="card-title" style="height: 1em">
                            <b> <?= h($item->name) ?></b>
                        </h3>
                        <p class="card-text" style="padding-top: 50px"> <b>Cost:</b> $<?= $item->cost ?></p>

                    </div>
                    <div class="card-footer">
                        <div align="center">
                            <?= $this->Html->link(__('More details'), ['action' => 'view', $item->id], array('class' => 'btn btn-primary mt-2 mb-2')) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- Pagination -->


    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
            </div>
        </div>

        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?php echo $this->Paginator->numbers();?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
    </div>

</div>
