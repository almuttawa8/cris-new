<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce[]|\Cake\Collection\CollectionInterface $ecommerce
 * @var \App\Model\Entity\Resident[]|\Cake\Collection\CollectionInterface $residents
 */
?>
<?= $this->Flash->render('notify_resident') ?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Sold marketplace applications page</h1>


            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Marketplace page", ['action' => 'resident_index'], array('class' => 'btn btn-primary' , 'style'=> 'width: 180px')) ?>
            </div>

            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Application details table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Product name</th>
                                <th>Cost</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($ecommerce as $item): ?>
                                <tr>
                                    <td><?php foreach ($residents as $resident):
                                            if($item->resident_id == $resident->id){?>
                                                <?= h($resident->fname)?>
                                                <?= h($resident->lname)?>
                                            <?php }
                                        endforeach; ?>
                                    </td>
                                    <td><?= h($item->name) ?></td>
                                    <td><?= h($item->cost) ?></td>
                                    <td><?= h($item->desc) ?></td>
                                    <td class="actions" align="center">
                                        <a class="btn btn-primary" href="<?php echo $this->Url->build([
                                            "controller" => "Ecommerce",
                                            "action" => "sold_details",
                                            $item->id
                                        ]);?>">View</a>
                                        <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal<?=$item->id ?>">Delete</button>

                                        <div class="modal fade" id="confirmModal<?=$item->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete this application <?php echo $item->name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'deleteSold', $item->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
