<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce[]|\Cake\Collection\CollectionInterface $ecommerce
 */
?>

<?= $this->Flash->render('success_unregister') ?>
<?= $this->Flash->render('unregister') ?>
<?= $this->Flash->render('success_edit_registration') ?>



<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3">My Items
    </h1>


    <div class="mb-2" align="left">
        <?= $this->Html->link(__('Back'), ['controller' => 'Ecommerce', 'action' => 'resident_index'], array('class' => 'btn btn-primary')) ?>
    </div>
    <table class="table table-hover mt-4" id="datatable">
        <thead>
        <tr>
            <th scope="col" style="color:#c507ac">Product name</th>
            <th scope="col" style="color:#c507ac">Cost</th>
            <th scope="col" style="color:#c507ac">Email</th>
            <th scope="col" style="color:#c507ac">Phone</th>
            <th scope="col" style="color:#c507ac">Website</th>
            <th scope="col" style="color:#c507ac">Description</th>
            <th scope="col" style="color:#c507ac">Status</th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($ecommerce as $item): ?>
            <tr>

                <td><?= h($item->name)?></td>
                <td><?= h($item->cost)?></td>
                <td><?= h($item->email)?></td>
                <td><?= h($item->phone)?></td>
                <td><a href="<?php echo $hyperlink= ($item->website)?>" target="_blank"><?php echo ($item->website)?></a></td>
                <td><?= h($item->desc)?></td>
                <?php if ($item->wanted_for == 'pending'){?>
                    <td>Pending</td>
                <?php }?>
                <?php if ($item->wanted_for == 'available'){?>
                <td>Accepted</td>
                <?php }?>
                <?php if ($item->wanted_for == 'sold'){?>
                    <td>Sold</td>
                <?php }?>

                <td class="actions" align="center">
                    <center><a class="btn btn-primary mb-2" style="margin-right:30px; color: white;" href="<?php echo $this->Url->build([
                            "controller" => "Ecommerce",
                            "action" => "edit_item",$item->id
                        ]);?>">Edit</a>

                        <button type ="button" class="btn btn-primary clickable mb-2" style="margin-right:30px;" data-toggle="modal" data-target="#confirmModal-<?=$item->id ?>">Delete</button>
                        <?php if ($item->wanted_for == 'available'){?>
                            <a class="btn btn-primary mb-2" style="margin-right:30px; color: white;" href="<?php echo $this->Url->build([
                                "controller" => "Ecommerce",
                                "action" => "sold_item",$item->id
                            ]);?>">Sold</a>
                        <?php }?>
                    </center>
                    <div class="modal fade" id="confirmModal-<?=$item->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete this product - <?=$item->name ?>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"><?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $item->id], array('style' => 'color: #961982')) ?></button>
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
