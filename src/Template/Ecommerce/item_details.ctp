<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ecommerce[]|\Cake\Collection\CollectionInterface $ecommerce
 * @var \App\Model\Entity\Resident[]|\Cake\Collection\CollectionInterface $residents
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>


<style>
    .itemImage{
        height:300px;
        width:600px
    }

    @media only screen and (max-width: 730px) {
        .itemImage{
            height:300px;
            width:100%;
        }
    }



</style>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">New Application details</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">New marketplace application page</li>
                <li class="breadcrumb-item active">New Application details</li>
            </ol>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"><br>
                                    <?php if($ecommerce->image_name != '')
                                    {
                                        echo $this->Html->image("/files/Ecommerce/image_name/".$ecommerce->image_name, array('alt' => '','class' =>'card-img-top itemImage'));
                                    }
                                    else{
                                        echo $this->Html->image('cris-logo.png',array('class' =>'card-img-top itemImage'));

                                    }?>
                                </div>
                            </div><br>
                            <?php foreach ($residents as $resident):
                                if($ecommerce->resident_id == $resident->id){?>
                                    <th scope="row"><b>User name:</b></th>
                                    <?= h($resident->fname)?>
                                    <?= h($resident->lname)?>
                                <?php }
                            endforeach; ?>
                            <?php if (!empty($ecommerce->name)){?><br>
                                <th scope="row"><b>Product name:</b></th>
                                <td><?= "  ",h($ecommerce->name) ?></td>
                            <?php }?>
                            <?php if (!empty($ecommerce->phone)){?><br>
                                <th scope="row"><b>Phone Number:</b></th>
                                <td><?= "  ",h($ecommerce->phone) ?></td>
                            <?php }?>

                            <?php if (!empty($ecommerce->email)){?><br>
                                <th scope="row"><b>Email:</b></th>
                                <td><?= "  ",h($ecommerce->email) ?></td>
                            <?php }?>

                            <?php if (!empty($ecommerce->website)){?><br>
                                <th scope="row"><b>Website:</b></th>
                                <td><a href="<?php echo $hyperlink= ($ecommerce->website)?>" target="_blank"><?php echo ($ecommerce->website)?></a></td>

                            <?php }?>

                            <?php if (!empty($ecommerce->desc)){?><br>
                                <th scope="row"><b>Description:</b></th>
                                <td><?= "  ",h($ecommerce->desc) ?></td>
                            <?php }?>
                            <br>
                                <th scope="row"><b>Date of listing:</b></th>
                                <td><?= h($ecommerce->date->format("d M, Y")) ?></td>

                            <br>  <br><?= $this->Form->postLink(__('Approve'), ['action' => 'approveItem', $ecommerce->id], array('class'=>'btn btn-primary')) ?>

                            <button type ="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal<?=$ecommerce->id ?>">Delete</button>

                            <div class="modal fade" id="confirmModal<?=$ecommerce->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this application <?php echo $ecommerce->name?>?
                                        </div>
                                        <div class="modal-footer">
                                            <?= $this->Form->postLink(__('Yes'), ['action' => 'deleteItem', $ecommerce->id], array('style' => 'color: black;','class'=>'btn btn-light')) ?>
                                            <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Ecommerce', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>

        </div><br>
    </main>
</div>
