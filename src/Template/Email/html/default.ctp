<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>

<h1>MAINTENANCE REQUEST</h1>
<table>
    <tr>
        <th align="left">Reference number: </th>
        <td><?= $id ?></td>
    </tr>
    <tr>
        <th align="left">Date submitted Request: </th>
        <td><?= $date ?></td>
    </tr>
    <tr>
        <th align="left">Apartment/Villa number: </th>
        <td><?= $apartment_no ?></td>
    </tr>
    <tr>
        <th align="left">Resident name: </th>
        <td><?= $resident_name?></td>
    </tr>
    <tr>
        <th align="left">Email address: </th>
        <td><?= $email?></td>
    </tr>
    <tr>
        <th align="left">Preferred phone no: </th>
        <td><?= $phone?></td>
    </tr>
    <tr>
        <th align="left">Maintenance requested: </th>
        <td><?= $desc?></td>
    </tr>
    <tr>
        <th align="left">Internal Access in resident absence: </th>
        <td><?= $access?></td>
    </tr>
    <tr>
        <th align="left">Name of person completing the form: </th>
        <td><?= $name?></td>
    </tr>
</table>
<p>This email is sent from CRIS. Do not reply to this email. Please respond to the resident with acknowledgement of receipt</p>

