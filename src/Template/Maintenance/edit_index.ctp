<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaintenanceDetails $MaintenanceDetails
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Maintenance page details</h1>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Maintenance form details page</li>
                <li class="breadcrumb-item active">Edit Maintenance page details</li>
            </ol>

            <?= $this->Form->create($MaintenanceDetails, ['type' => 'file']) ?>
            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('form_instruction', ['label'=>"Form instructions:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('form_conditions', ['label'=>"Form conditions:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('form_note', ['label'=>"Form note:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  mt-4">
                <div class="col-xl-9">
                <?= $this->Html->link(__('Back'), ['controller' => 'Maintenance', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
                <?= $this->Form->button(__('Submit')) ?>
             </div>
            </div>


            <?= $this->Form->end() ?>



            </div>

    </main>
</div>
