<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaintenanceDetails $MaintenanceDetails
 * @var \App\Model\Entity\Maintenance $Maintenance
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>






<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<!-- Page Content -->
<div class="container">
    <div style="color: #961982;"><b><?= $this->Flash->render('success_enquiry') ?>
            <?= $this->Flash->render('fail_enquiry') ?></b></div>
    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">
        Classic Residences Brighton - Maintenance form
    </h1>


    <?= $this->Form->create($Maintenance, ['type' => 'file'])  ?>
    <div class="row ml-2">
        <div class="col-lg-8 mb-4">

            <div class="alert alert-warning" role="alert"><?php foreach ($MaintenanceDetails as $detail): ?>
                    <?= h($detail->form_instruction) ?>
                <?php endforeach; ?>
            </div>
            <div class="alert alert-warning" role="alert">
                * Indicates required field
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Apartment/Villa number*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('apartment_no', ['label' => false, 'class' => 'form-control']);?>

                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Resident name*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('resident_name', ['label' => false, 'class' => 'form-control']);?>

                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Email address</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('email', ['label' => false, 'class' => 'form-control','type'=>'email']);?>

                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Phone number*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->control('phone', ['label' => false,'placeholder'=>'04XXXXXXXX', 'class' => 'form-control','type'=>'tel','maxlength'=>10,'pattern'=>'[0-9]{8,10}']);?>

                    </div>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group form-group">
                <div class="controls">
                    <label>Maintenance requested*</label>
                    <div class="col-sm-50">
                        <?php echo $this->Form->input('maintenance_desc', ['rows'=>10, 'label' => false,'class' => 'form-control'])?>
                    </div>
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-8 pt-0">Is internal access OK in resident’s absence?</legend>

                    <div class="col-sm-7">
                        <label class="container1">Yes
                            <input type="radio" name="internal_access" checked="checked" value="yes">
                            <span class="checkmark1 mt-1" for="yes"></span>
                        </label>
                        <label class="container1">No
                            <input type="radio" name="internal_access" value="no">
                            <span class="checkmark1 mt-1" for="no"></span>
                        </label>
                    </div>

                </div>
            </fieldset>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <legend class="col-form-label col-sm-4 pt-0">Image: </legend>
                    <div class="col-sm-8">
                        <?php echo $this->Form->text('image_name', ['type' => 'file']); ?>
                    </div>
                </div>
            </div><br>

            <div class="alert alert-warning" role="alert"><?php foreach ($MaintenanceDetails as $detail): ?>
                    <?= h($detail->form_conditions) ?><br>
                    <b>PLEASE NOTE: </b><?= h($detail->form_note) ?>
                <?php endforeach; ?>
            </div>


        </div>
    </div>
    <div align="left" style="margin-bottom: 0.5em">
        <input class="btn btn-primary ml-4 mt-2" type="submit" value="Submit">
    </div>

    <?= $this->Form->end() ?>

</div>

