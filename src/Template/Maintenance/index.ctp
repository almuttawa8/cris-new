<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaintenanceDetails[]|\Cake\Collection\CollectionInterface $MaintenanceDetails
 */
?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">

            <h1 class="mt-4">Maintenance form details page</h1>
            <div class="card mb-4">
                <div class="card-header">Form instructions</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($MaintenanceDetails as $detail): ?>
                            <div class="col-xl-9">

                                <div class="form-group">
                                    <?= h($detail->form_instruction) ?>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Form conditions</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($MaintenanceDetails as $detail): ?>
                        <div class="col-xl-9">

                            <div class="form-group">
                                <?= h($detail->form_conditions) ?>
                            </div>

                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Form note</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($MaintenanceDetails as $detail): ?>
                        <div class="col-xl-9">

                            <div class="form-group">
                                <?= h($detail->form_note) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row ml-2 mt-4">
                <a class="btn btn-primary" href="
                        <?php echo $this->Url->build([
                    "controller" => "Maintenance",
                    "action" => "editIndex",
                    $detail->id
                ]);?>">Edit</a>
                <?php endforeach; ?>
            </div>

        </div>
    </main>
</div>
