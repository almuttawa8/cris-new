<?php
/**
 * @var \App\View\AppView $this
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<!-- Page Content -->
<div class="container"><br>
    <div class="alert alert-warning" role="alert">
        <center><h3>Request Sent</h3><br>
        Your request number <?= $data['id'] ?> was sent to the front desk. You will receive a reply from front desk. <br><br>

        <div style="margin-bottom: 0.5em">
            <?= $this->Html->link(__('New request'), ['controller' => 'Maintenance', 'action' => 'resident_index'], array('style' => 'color:white;', 'class' => 'btn btn-primary ml-4 mt-2')) ?>

        </div></center>
    </div>


</div>
