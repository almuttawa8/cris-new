<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Role[]|\Cake\Collection\CollectionInterface $roles
*/
?>




<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Roles page</h1>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Add Role", ['action' => 'add'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Roles table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($roles as $role): ?>
                                <tr>
                                    <td><?= h($role->name) ?></td>
                                    <td class="actions" align="center">
                                        <a class="btn btn-primary mb-1" href="<?php echo $this->Url->build([
                                            "controller" => "Roles",
                                            "action" => "edit",
                                            $role->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?= $role->id?>">Delete</button>

                                        <div class="modal fade" id="confirmModal-<?= $role->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete this role - <?= $role->name?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $role->id], ['style'=> 'color: black;','class'=>'btn btn-light']) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>





