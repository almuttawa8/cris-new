<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registration[]|\Cake\Collection\CollectionInterface $registrations
 */
?>

<?= $this->Flash->render('success_unregister') ?>
<?= $this->Flash->render('unregister') ?>
<?= $this->Flash->render('success_edit_registration') ?>



<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3">My Registrations
    </h1>
    <div class="alert alert-warning" role="alert">
        <b>Note</b> : The cancellation period for events is 7 days. If you wish to unregister within the 7 days, talk to reception or a committee member.
    </div>

    <div class="mb-2" align="left">
        <?= $this->Html->link(__('Back'), ['controller' => 'Events', 'action' => 'resident_index'], array('class' => 'btn btn-primary')) ?>
    </div>
    <table class="table table-hover mt-4" id="datatable">
        <thead>
        <tr>
            <th scope="col" style="color:#c507ac">Event Date</th>
            <th scope="col" style="color:#c507ac">Event Name</th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($myRegistrations as $registration): ?>
            <tr>
                <td data-order="<?= h($registration->event->event_date->format("Y/m/d")) ?>" data-sort="<?= h($registration->event->event_date->format("Y/m/d")) ?>">
                    <?= h($registration->event->event_date->format("d M, Y")) ?>
                </td>
                <td><?= h($registration->event->name)?></td>
                <td class="actions" align="center">
                    <center><a class="btn btn-primary mb-2" style="margin-right:30px; color: white;" href="<?php echo $this->Url->build([
                        "controller" => "Registrations",
                        "action" => "resident_edit",$registration->id
                    ]);?>">Edit</a>

                    <button type ="button" class="btn btn-primary clickable mb-2" style="margin-right:30px;" data-toggle="modal" data-target="#confirmModal-<?=$registration->id ?>">Unregister</button>
                    </center>
                    <div class="modal fade" id="confirmModal-<?=$registration->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to unregister from this event - <?=$registration->event->name ?>?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"><?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $registration->id], array('style' => 'color: #961982')) ?></button>
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>

