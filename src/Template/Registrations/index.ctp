<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Registration[]|\Cake\Collection\CollectionInterface $registrations
*/
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">All Registrations page</h1>

            <div align="right">
                <p>
                    <h9>Sort:</h9>
                    <?php $this->Paginator->setTemplates([
                        'sort' => '<a class="btn btn-primary" style="margin-left: 0.5em" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?php $this->Paginator->setTemplates([
                        'sortAsc' => '<a class="btn btn-primary" style="margin-left: 0.5em" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?php $this->Paginator->setTemplates([
                        'sortDesc' => '<a class="btn btn-primary" style="margin-left: 0.5em" href="{{url}}">{{text}}</a>'
                    ]);?>
                    <?= $this->Paginator->sort('Events.name', 'Name') ?>
                    <?= $this->Paginator->sort('Events.event_date', 'Date')?>
                </p>
            </div>
            <?php foreach($events as $event): ?>
            <div class="card mb-4">


                    <div class="card-header"><div class="row">
                            <h5><?= $event->name?></h5>
                        <h5> &nbsp;(<?= $event->event_date?>)</h5>
                        <h5 class="col" align="right">Residents attending: <?= $event->count_resident?></h5>
                        </div></div>
                        <div class="row card-body">
                            <p class="card-text col-11"><?= $event->desc?></p>
                            <a class="btn btn-primary" style="height: 25%" href="<?php echo $this->Url->build([
                                "controller" => "Registrations",
                                "action" => "registration_event",
                                $event->id
                            ]);?>">View</a>
                        </div>


            </div>
            <?php endforeach ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                            <ul class="pagination">

                                <a><?= $this->Paginator->prev('< ' . __('previous')) ?></a>

                                <a><?= $this->Paginator->numbers() ?></a>

                                <a><?= $this->Paginator->next(__('next') . ' >') ?></a>

                            </ul>
                        </div>
                    </div>
                </div>



        </div>
    </main>
</div>



