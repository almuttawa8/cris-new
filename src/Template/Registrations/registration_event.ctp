<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>




<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4"><?= $events->name?> registration</h1>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">All Registrations</li>
                <li class="breadcrumb-item active">Event Registration</li>
            </ol>


            <div class="card-body">
                <p><i>Number of Registrations: </i><?= $total ?></p>
                <p><i>Event date :</i> <?= $events->event_date->format("d M, Y")?> </p>
                <p><i>Event time :</i> <?= $events->start_time->format("h:i A")?> -
                    <?= $events->end_time->format("h:i A")?> </p>
                <p><i>Spots available:</i> <?= ($events->availability - $total)?></p>

                <table class="table table-bordered table-hover" id="dataTable">
                    <tr>
                        <th scope="col"><?= __('First Name') ?></th>
                        <th scope="col"><?= __('Last Name') ?></th>
                        <th scope="col"><?= __('Email') ?></th>
                        <th scope="col"><?= __('Phone') ?></th>
                        <th scope="col"><?= __('Paid') ?></th>
                        <th scope="col"><?= __('Need Transport') ?></th>
                        <th scope="col"><?= __('Action')?></th>
                    </tr>
                    <?php foreach ($registrations as $registration): ?>
                        <tr>
                            <td><?= h($registration->resident->fname) ?></td>
                            <td><?= h($registration->resident->lname) ?></td>
                            <td><?= h($registration->resident->email) ?></td>
                            <td><?= h($registration->resident->phone) ?></td>
                            <td><?= h($registration->payment_option)?></td>
                            <td><?= h($registration->transport_option)?></td>
                            <td class="actions" align="center">
                                <?= $this->Html->link(__('Update'), ['action' => 'edit', $registration->id], array('style' => 'color:white','class'=> 'btn btn-primary')) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>


            <div class="row  mt-4">
                <div class="col-xl-9">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Registrations', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>

                </div>
            </div>





        </div>

</main>
</div>

