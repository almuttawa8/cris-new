<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registration $registration
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
$this->Form->setTemplates([
    'radio' => '<input{{attrs}}class="form-check-input" type="radio" name="{{name}}" value="{{value}}">',
    'nestingLabel' => '<div{{attrs}}class="form-check form-check-inline">{{input}}<label class="form-check-label" for="inlineRadio1">{{text}}</label></div>'
]);
?>

<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: -10px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>


<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3"><?= $event_name ?>
    </h1>


        <?= $this->Form->create($registration) ?>
        <div class="row ml-2">
            <div class="col-lg-8 mb-4">
                <div class="container">


                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-8 pt-0">Do you need any transportation?</legend>

                            <div class="col-sm-7">
                                <label class="container1">Yes
                                    <input type="radio" name="transport" value="Y">
                                    <span class="checkmark1 mt-1" for="Y"></span>
                                </label><br>
                                <label class="container1">No
                                    <input type="radio" name="transport" value="N" checked="checked">
                                    <span class="checkmark1 mt-1" for="N"></span>
                                </label>
                            </div>

                        </div>
                    </fieldset>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Please specify any dietary requirements?</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('dietary', ['rows'=>2, 'label'=>false]); ?>
                            </div>
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Comment</label>
                            <div class="col-sm-50">
                                <?php echo $this->Form->control('comment', ['rows'=>10, 'label' => false]); ?>
                            </div>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                        </div>
                    </div>
                    <div align="left">
                        <?= $this->Html->link(__('Back'), ['controller' => 'Events', 'action' => 'resident_index'], array('class' => 'btn btn-primary ml-4 mt-2')) ?>

                        <input class="btn btn-primary ml-4 mt-2" type="submit" value="Submit">
                    </div>

                </div>
            </div>
        </div>

        <?= $this->Form->end() ?>

</div>



