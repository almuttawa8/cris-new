<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registration[]|\Cake\Collection\CollectionInterface $registrations
 */
?>

<div id="dataTable_filter" class="dataTables_filter" align="center">
    <?= $this->Html->link("Events", ['controller' => 'Events', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
    <b><?= $this->Html->link("All Registrations", ['controller' => 'Registrations', 'action' => 'index'], array('class' => 'btn btn-primary1')) ?></b>
    <?= $this->Html->link("Locations", ['controller' => 'Locations', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
</div>

<div class="registrations index large-9 medium-8 columns content">

    <h3><?= __('Registrations List') ?></h3>
    <div class="dropdown">
        <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filter by event name
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <?php foreach($events as $event): ?>
                <a class= "dropdown-item"><?= $this->Form->postLink($event->name, ['action' => 'home', $event->id],['escape' => false]) ?></a>
            <?php endforeach ?>
        </div>
    </div>
    <?php foreach($registrations as $registration): ?>
    <div class="card">
        <h5 class="card-header"><?= $registration->event->name?></h5>
        <div class="card-body">
            <p class="card-text"><?= $registration->event->desc?></p>
            <button class = "btn btn-primary1"><?= $this->Form->postLink('Print View',['controller' => 'Events', 'action' => 'registration_event', $registration->event->id, 'style' => 'color:white'],['escape' => false]) ?></button>
            <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link(__('Edit'), ['action' => 'edit', $registration->id], array('style' => 'color:white')) ?></button>
        </div>
    </div>
    <?php endforeach ?>


</div>
