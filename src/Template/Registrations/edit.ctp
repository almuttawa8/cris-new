<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Registration $registration
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Registration</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">All Registrations</li>
                <li class="breadcrumb-item active">Event Registration</li>
                <li class="breadcrumb-item active">Edit Registration</li>
            </ol>
            <?= $this->Form->create($registration) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <fieldset>



                                            <br>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Event Name:</label>
                                                <div class="col-sm-8">
                                                    <?php echo $this->Form->control('event.name',['disabled' => 'disabled', 'label' => false]);?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">First Name:</label>
                                                <div class="col-sm-8">
                                                    <?php echo $this->Form->control('resident.fname',['disabled', 'label' => false]); ?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Last Name:</label>
                                                <div class="col-sm-8">
                                                    <?php echo $this->Form->control('resident.lname',['disabled', 'label' => false]); ?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Transportation</label>
                                                <div class="col-sm-8">
                                                    <?php echo $this->Form->control('transport',
                                                        ['type'=>'radio','options' => [
                                                            ['value' => 'Y', 'text' => __('Yes')],
                                                            ['value' => 'N', 'text' => __('No')],
                                                        ],
                                                            'default' => 'N',
                                                            'label' => false]);?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Payment</label>
                                                <div class="col-sm-8">
                                                    <?php     echo $this->Form->control('payment',
                                                        ['type'=>'radio','options' => [
                                                            ['value' => 'Y', 'text' => __('Yes')],
                                                            ['value' => 'N', 'text' => __('No')],
                                                        ],
                                                            'default' => 'N',
                                                            'label' => false]);?>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Comments:</label>
                                                <div class="col-sm-8">
                                                    <?php echo $this->Form->control('comment',['disabled', 'rows' =>10,'label'=>false ]); ?>
                                                </div>
                                            </div>


                            </fieldset></div>
                    </div>
                </div>
            </div>




        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Registrations', 'action' => 'registration_event/'.$registration->event_id], array('class' => 'btn btn-primary')) ?>
            <?= $this->Form->button(__('Submit')) ?>
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

