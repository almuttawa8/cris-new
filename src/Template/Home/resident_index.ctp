<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\News[]|\Cake\Collection\CollectionInterface $newsData
 * @var \App\Model\Entity\Notify[]|\Cake\Collection\CollectionInterface $Notify
 * @var $Flag
 * @var \App\Model\Entity\Home[]|\Cake\Collection\CollectionInterface $Home
 * @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $Menus
 * @var \App\Model\Entity\Restaurant[]|\Cake\Collection\CollectionInterface $Restaurant
 */
?>


<style>
    .no-padding {
        padding-left: 0;
        padding-right: 0;
    }
    .container1 {
        width: 100%;
        margin-right: 20px;
        margin-left: 20px;
        position: relative;

    }
    .container .content {
        position: absolute;
        right: 0;
        bottom: 0;
        background: rgb(0, 0, 0); /* Fallback color */
        background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
        color: #f1f1f1;
        width: 100%;
        padding: 10px;
        border-radius: 1%;
    }
    .container img {vertical-align: middle;border-radius: 1%;}
    .menu_button{
        background-color: #961982;
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        display: inline-block;
        font-size: 16px;
        cursor: pointer;
        float: right;
    }

</style>
<header>
    <div id="carouselExampleIndicators" class="carousel slide" style="margin-top: -4px" data-ride="">
        <div class="carousel-inner" role="listbox">
<!--            <div class="carousel-item active" style="background-image: url('/Iteration4/Production/team016-app/webroot/img/Capture.png');">-->
<!--            </div>-->
            <div class="carousel-item active" style="background-image: url('/webroot/img/Capture.png');">
            </div>
        </div>
    </div>
</header>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Notification part -->
<?php $i=0;
foreach ($Notify as $i=>$notify):
    $i++;
endforeach;


if($i!=0){?>
<!--    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--        <div class="modal-dialog" role="document">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <h5 class="modal-title" id="exampleModalLabel">Notification</h5>-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                        <span aria-hidden="true">&times;</span>-->
<!--                    </button>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
    <?php foreach ($Notify as $notify): ?>

    <?php endforeach; ?>
<!--                </div>-->
<!--                <div class="modal-footer">-->
<!--                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

<?php }?>
<!-- Page Content -->
<div class="container">

     <?php foreach ($Home as $home): ?>
         <h1 class="my-4 title"><b><?= h($home->welcome) ?></b></h1>
        <p class="card-text ml-3" style="font-size: 17px;"> <?= h($home->Intro) ?></p>
     <?php endforeach; ?>



            <!-- News -->
    <h4 class="my-4 title"><b>Important News</b></h4>
    <div class="row">
        <?php $i=0;
        foreach ($newsData as $i=>$news):
            $i++;
        endforeach;


        if($i!=0){?>
            <div class="table-responsive">
                <div style="overflow: hidden;">
                    <table class="ml-4" style="width: 90%; font-size: 17px;" >
                        <tbody>
                        <?php foreach ($newsData as $news): ?>
                            <tr>
                                <td><li><?= h($news->newsDesc) ?></li></td>
                                <td><?= h($news->date->format("d M, Y")) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }
        else{?>
            <p class="card-text" style="margin-left: 30px"> No important news</p>
        <?php }?>
    </div>
    <!-- Noticeboard -->
    <h4 class="my-4 title"><b>Noticeboard</b></h4>
    <div class="row">
        <center>
            <a class="btn btn-primary ml-4 my-4" href="<?php echo $this->Url->build([
                "controller" => "Newsletters",
                "action" => "resident_index"
            ]);?>"><i class="far fa-newspaper fa-5x"></i><br>Newsletters</a>
            <a class="btn btn-primary ml-4 my-4" href="<?php echo $this->Url->build([
                "controller" => "Minutes",
            "action" => "resident_index"
            ]);?>"><i class="far fa-clipboard fa-5x"></i><br>Minutes</a>
            <a class="btn btn-primary ml-4 my-4" href="<?php echo $this->Url->build([
                "controller" => "Calendars",
                "action" => "resident_index"
            ]);?>"><i class="far fa-calendar fa-5x"></i><br>Calendars</a>
            <a class="btn btn-primary ml-4 my-4" href="<?php echo $this->Url->build([
                "controller" => "Menus",
                "action" => "resident_index"
            ]);?>"><i class="fas fa-utensils fa-5x"></i><br>Food Menus</a>
            <a class="btn btn-primary ml-4 my-4" href="<?php echo $this->Url->build([
                "controller" => "miscellaneous",
                "action" => "resident_index"
            ]);?>"><i class="fab fa-readme fa-5x"></i><br>Others</a>

        </center>
    </div>

    <!-- Food menu -->
    <div class="row">
        <div class="col">
            <h4 class="my-4 title"><b>Today's special menu</b></h4>
        </div>
    </div>
    <div class="row">
            <div class="container1">
<!--                <img src="/Iteration4/Production/team016-app/webroot/img/menu.jpg" style="width: 100%;">-->
                <img src="/webroot/img/menu.jpg" style="width: 100%;">
                <div class="content">
                    <?php foreach ($Restaurant as $rest): ?>
                        <h3><?= h($rest->header) ?></h3>
                        <p style="font-size: 17px;"><?= h($rest->desc) ?></p>
                        <?php $i=0;
                        foreach ($Menus as $menu):
                            $i++;
                        endforeach;

                        if($i==0){?>
                            <p class="card-text ml-3" style="float: right; font-size: 17px;"> No Special Menu Today!!</p>
                        <?php }?>
                        <?php foreach ($Menus as $menu): ?>
                            <a class="btn btn-primary my-4 mb-2" target="_blank" style="float: right;" href="/webroot/files/Menus/file_name/<?php echo $menu->file_name ?>">Today's Special</a>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            </div>

    </div><br>

    <!-- Services -->
    <div class="row">
        <div class="col">
            <h4 class="my-4 title"><b>Services</b></h4>
        </div>
        <div class="col" align="right">
            <?= $this->Html->link(__('More Services'), ['controller' => 'Services','action' => 'residentIndex'], array('class' => 'btn btn-primary my-4 mb-2')) ?>
        </div>
    </div>
    <div class="row">

        <?php
        $i=0;
        foreach ($services as $service):?>

                <?php if($i < 3){?>
                    <div class="col-lg-4 mb-4">
                        <div class="card" style="height: 26em">
                       <?php if($service->banner != '')
                        {
                            echo $this->Html->image("/files/Services/banner/".$service->banner, ['width'=>'50px', 'class' =>'card-img-top','url'=> ['controller'=> 'Services','action'=> 'resident_index']]);
                        }
                        else{
                            echo $this->Html->image('circle-cropped.png', ['width'=>'50px', 'class' =>'card-img-top','url'=> ['controller'=> 'Services','action'=> 'resident_index']]);
                        }?>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php echo $this->Url->build([
                                    "controller" => "Services",
                                    "action" => "resident_index"
                                ]);?>"><?= h($service->name) ?></a>
                            </h4>
                            <p class="card-text" style="font-size: 17px;" > <b>Phone: </b> <?= $service->phone ?></p>
                            <p class="card-text" style="font-size: 17px;"> <b>Email: </b> <?= $service->email ?></p>
                        </div>
                        </div>
                    </div>
                <?php    $i= $i + 1;
             }?>
        <?php endforeach; ?>
    </div>

</div>

<script>
    <?php if($Flag){?>

    $(document).ready(function(){
        $("#myModal").modal({backdrop: true});
    });
    <?php }?>
</script>

