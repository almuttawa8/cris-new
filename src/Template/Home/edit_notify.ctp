<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Notify $notify
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Notification</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Notification page</li>
                <li class="breadcrumb-item active">Edit Notification</li>
            </ol>
            <?= $this->Form->create($notify, ['type' => 'file']) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('news', ['label'=>"Notification description *", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div align="left" class="mt-4 ml-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Home', 'action' => 'notifyIndex'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

