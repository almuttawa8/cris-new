<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Home $home
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Home messages</h1>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Home page</li>
                <li class="breadcrumb-item active">Edit Home messages</li>
            </ol>

            <?= $this->Form->create($home, ['type' => 'file']) ?>
            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('welcome', ['label'=>"Welcome message:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('Intro', ['label'=>"Introduction message:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row  mt-4">
                <div class="col-xl-9">
                <?= $this->Html->link(__('Back'), ['controller' => 'Home', 'action' => 'HomeIndex'], array('class' => 'btn btn-primary')) ?>
                <?= $this->Form->button(__('Submit')) ?>
             </div>
            </div>


            <?= $this->Form->end() ?>



            </div>

    </main>
</div>
