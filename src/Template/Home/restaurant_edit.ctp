<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Restaurant $Restaurant
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));

?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Restaurant Details</h1>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Restaurant Details page</li>
                <li class="breadcrumb-item active">Edit Restaurant Details</li>
            </ol>

            <?= $this->Form->create($Restaurant, ['type' => 'file']) ?>
            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('header', ['label'=>"Title:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('desc', ['label'=>"Description:", 'rows'=>5]); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row  mt-4">
                <div class="col-xl-9">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Home', 'action' => 'RestaurantIndex'], array('class' => 'btn btn-primary')) ?>
                    <?= $this->Form->button(__('Submit')) ?>
                </div>
            </div>


            <?= $this->Form->end() ?>



        </div>

    </main>
</div>
