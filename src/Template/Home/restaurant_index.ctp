<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Restaurant[]|\Cake\Collection\CollectionInterface $Restaurant
 */
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">

            <h1 class="mt-4">Restaurant Details page</h1>
            <div class="card mb-4">
                <div class="card-header">Title</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($Restaurant as $rest): ?>
                            <div class="col-xl-9">

                                <div class="form-group">
                                    <?= h($rest->header) ?>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Description</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($Restaurant as $rest): ?>
                        <div class="col-xl-9">

                            <div class="form-group">
                                <?= h($rest->desc) ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="row ml-2 mt-4">
                <a class="btn btn-primary" href="
                        <?php echo $this->Url->build([
                    "controller" => "Home",
                    "action" => "restaurant_edit",
                    $rest->id
                ]);?>">Edit</a>
                <?php endforeach; ?>
            </div>
        </div>
    </main>
</div>
