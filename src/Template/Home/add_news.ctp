<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\News $news
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add News</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">News page</li>
                <li class="breadcrumb-item active">Add News</li>
            </ol>
            <?= $this->Form->create($news, ['type' => 'file'])  ?>

                <div class="row mt-2">
                    <div class="col-xl-9">
                        <div class="card container">
                            <div class="form-group">
                                <?php echo $this->Form->control('newsDesc', ['label'=>"News *", 'rows'=>5, 'class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>
                </div>


        </div>
                <div align="left" class="mt-4 ml-4">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Home', 'action' => 'index'], array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
                    <input class="btn btn-primary" type="submit" value="Submit">
                </div>
            <?= $this->Form->end() ?>

    </main>
</div>


