<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Home[]|\Cake\Collection\CollectionInterface $Home
 */
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">

            <h1 class="mt-4">Home page</h1>
            <div class="card mb-4">
                <div class="card-header">Welcome message</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($Home as $home): ?>
                                <div class="col-xl-9">

                                        <div class="form-group">
                                            <?= h($home->welcome) ?>
                                        </div>

                                </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Introduction message</div>
                <div class="card-body">

                    <div class="row ml-1 mt-2">
                        <?php foreach ($Home as $home): ?>
                            <div class="col-xl-9">

                                <div class="form-group">
                                    <?= h($home->Intro) ?>
                                </div>

                            </div>

                    </div>
                </div>
            </div>

            <div class="row ml-2 mt-4">
                    <a class="btn btn-primary" href="
                        <?php echo $this->Url->build([
                        "controller" => "Home",
                        "action" => "editHome",
                        $home->id
                        ]);?>">Edit</a>
                <?php endforeach; ?>
            </div>
        </div>
    </main>
</div>

