<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Notify[]|\Cake\Collection\CollectionInterface $Notify
 */
?>


    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Notification page</h1>

                <?php $i=0;
                foreach ($Notify as $i=>$notify):
                    $i++;
                endforeach;


                if($i==0){?>
                <div class="mt-4 mb-4" align="right">
                    <?= $this->Html->link("Add a Notification", ['action' => 'addNotify'], array('class' => 'btn btn-primary','style'=>'width: 180px')) ?>
                </div>
                <?php }?>

                <div class="card mb-4 w-125">
                    <div class="card-header"><i class="fas fa-table mr-1"></i>Notification table</div>
                    <div class="card-body w-100">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Notification</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($Notify as $notify): ?>
                                    <tr>
                                        <td><?= h($notify->news) ?></td>
                                        <td class="actions w-75" align="center">
                                            <a class="btn btn-primary mb-1" href="<?php echo $this->Url->build([
                                                "controller" => "Home",
                                                "action" => "editNotify",
                                                $notify->id
                                            ]);?>">Edit</a>
                                            <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?= $notify->id?>">Disable</button>

                                            <div class="modal fade" id="confirmModal-<?= $notify->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Are you sure you want to disable this notification?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <?= $this->Form->postLink(__('Yes'), ['class' => 'btn btn-light','action' => 'deleteNotify', $notify->id], array('style' => 'color: black;')) ?>
                                                            <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

