<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */

//$this->assign('title', 'Classic Residences');
$this->layout = 'login';
?>
<?= $this->Flash->render('fail_login') ?>
<div id="layoutAuthentication_content">
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header"><h3 class="text-center font-weight-light my-2">Login</h3></div>
                        <div class="card-img-top" align="center" style="background-color: #961982">
                            <img src="../img/circle-cropped.png" width="250px">
                        </div>
<!--                        <div class="alert alert-warning" role="alert" align="center">-->
<!--                            <b>Note</b> : Please do not open the website using Internet Explorer and ensure that the latest iOS version is updated.-->
<!--                        </div>-->
                        <?= $this->Form->create(); ?>
                        <div class="card-body">
                            <form>

                               <div class="form-group"> <?= $this->Form->control('identifier', ['placeholder' => 'Please Enter Your Resident ID...', 'required', 'class'=>'form-control py-4','label'=>'Resident ID']);?></div>
                                <div class="form-group">    <?= $this->Form->control('lname', ['placeholder' => 'Please Enter Your Last Name...', 'required', 'class'=>'form-control py-4','label'=>'Last Name']);?>
                                </div>

                                <div class="form-group d-flex align-items-center justify-content-between mt-2" style="float: right"><?= $this->Form->button(__('Login'), ['class'=>'btn btn-primary']) ?></div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
