<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Resident</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Residents page</li>
                <li class="breadcrumb-item active">Add Resident</li>
            </ol>
            <?= $this->Form->create($resident) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">

                            <label class="col-sm-4 col-form-label">Resident name *</label>
                            <div class="col-md-5"><?php echo $this->Form->control('fname', ['label' => false, 'placeholder'=>'First name']); ?></div>
                            <div class="col-md-5 mt-2"><?php echo $this->Form->control('lname', ['label' => false, 'placeholder'=>'Last name']); ?></div>


                            <label class="col-sm-4 col-form-label">Resident ID *</label>
                            <div class="ml-2"><?php echo $this->Form->control('identifier', ['label' => false, 'placeholder'=>'Resident ID','required']); ?></div>

                            <label class="col-sm-4 col-form-label">Mobile Phone</label>
                            <div class="ml-2"><?php echo $this->Form->control('phone',['label' => false, 'type'=>'tel','maxlength'=>10,'pattern'=>'[0-9]{8,10}','placeholder'=>'04XXXXXXXX']); ?></div>

                            <label class="col-sm-4 col-form-label">Email</label>
                            <div class="ml-2"><?php echo $this->Form->control('email',['label' => false, 'placeholder'=>'Email','type'=>'email']); ?></div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Property type</legend>
                                    <div class="col-sm-8 ml-2">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="property_type" id="Apartment" value="Apartment" checked>
                                            <label class="form-check-label" for="Apartment">Apartment</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="property_type" id="Serviced Apartment" value="Serviced Apartment">
                                            <label class="form-check-label" for="Serviced Apartment">Serviced Apartment</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="property_type" id="Villa" value="Villa">
                                            <label class="form-check-label" for="Villa">Villa</label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <label class="col-sm-4 col-form-label">Landline phone</label>
                            <div class="ml-2"><?php echo $this->Form->control('landline',['label' => false, 'placeholder'=>'Landline']); ?></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Residents', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

    </main>
</div>


