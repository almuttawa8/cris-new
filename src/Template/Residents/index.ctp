<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident[]|\Cake\Collection\CollectionInterface $residents
 */
?>





<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Residents page</h1>


            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link("Add Resident", ['action' => 'add'], array('class' => 'btn btn-primary mb-1', 'style' => 'width: 180px')) ?>
                <?= $this->Html->link("Import Residents", ['action' => 'import'], array('class' => 'btn btn-primary mb-1', 'style' => 'width: 180px')) ?>
            </div>
            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Residents table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Mobile phone</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($residents as $resident): ?>
                                <tr>
                                    <td><?= h($resident->fname) ?></td>
                                    <td><?= h($resident->lname) ?></td>
                                    <td><?= h($resident->phone) ?></td>
                                    <td><?= h($resident->email) ?></td>
                                    <td class="actions" align="center">
                                        <a class="btn btn-primary mb-1" href="<?php echo $this->Url->build([
                                            "controller" => "Residents",
                                            "action" => "edit",
                                            $resident->id
                                        ]);?>">Edit</a>
                                        <button type ="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#confirmModal-<?= $resident->id?>">Delete</button>

                                        <div class="modal fade" id="confirmModal-<?= $resident->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to archive this resident - <?= $resident->lname?>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?= $this->Form->postLink(__('Yes'), ['action' => 'archive', $resident->id], ['style'=> 'color: black;','class'=>'btn btn-light']) ?>
                                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>






