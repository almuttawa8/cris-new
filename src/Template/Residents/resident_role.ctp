<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Resident[]|\Cake\Collection\CollectionInterface $residents
*/
?>




<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Resident Roles</h1>


            <div class="card mb-4 w-125">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Resident Roles table</div>
                <div class="card-body w-100">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Roles</th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($residents as $resident): ?>
                                <tr>
                                    <td><?= h($resident->fname) ?></td>
                                    <td><?= h($resident->lname) ?></td>
                                    <td><?php if (!empty($resident->roles)): ?>
                                            <?php foreach ($resident->roles as $roles): ?>
                                                <?= h($roles->name) ?>
                                                <br>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="actions" align="center">
                                        <?= $this->Html->link(__('Change Role'), ['action' => 'edit_role', $resident->id], array('style' => 'color:white','class'=>'btn btn-primary','style'=>'width: 180px')) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>





