<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<?= $this->Flash->render('success_profile') ?>

<style>
    /* The container */
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container1 .checkmark1:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-4">My profile
    </h1>

    <?= $this->Form->create($resident) ?>
    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <!-- Blog Post -->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="col-sm-6">
                        <div class="indicatorDefault">
                            * Indicates required field
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Resident ID</label>
                        <div class="col-sm-3">
                            <?php echo $this->Form->control('identifier',['label'=>false, 'readonly']); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('fname', ['label' => 'First Name', 'readonly']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('lname', ['label' => 'Last Name', 'readonly']); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Mobile Phone *</label>
                        <div class="col-sm-5">
                            <?php echo $this->Form->control('phone',['label'=>false,'maxlength'=>10,'pattern'=>'[0-9]{8,10}','placeholder'=>'04XXXXXXXX']); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email *</label>
                        <div class="col-sm-5">
                            <?php echo $this->Form->control('email',['label'=>false]); ?>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Role</label>
                        <div class="col-sm-5">
                            <?php if ($role == ''){
                                echo 'Resident';
                            }
                            else{
                                echo $role;
                            } ?>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">



            <!-- Categories Widget -->
            <div class="card md-4">
                <h5 class="card-header">Select clubs</h5>
                <div class="card-body">
                    <div class="row">
                            <div class="form-check">
                                <?php echo $this->Form->select('clubs._ids', $clubs, ['multiple' => 'checkbox', 'class' => 'form-check-label']); ?>
                            </div>

                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Select interests</h5>
                <div class="card-body">
                    <div class="form-check">
                        <?php echo $this->Form->select('interests._ids', $interests, ['multiple' => 'checkbox', 'class' => 'form-check-label']); ?>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class="col" align="left">
        <input class="btn btn-primary" type="submit" value="Submit">
    </div>
    <!-- /.row -->

</div>


