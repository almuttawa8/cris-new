<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>



<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Resident</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Residents page</li>
                <li class="breadcrumb-item active">Edit Resident</li>
            </ol>
            <?= $this->Form->create($resident) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">

                            <label class="col-sm-4 col-form-label">Resident name *</label>
                            <div class="col-md-5"><?php echo $this->Form->control('fname', ['label' => false]); ?></div>
                            <div class="col-md-5 mt-2"> <?php echo $this->Form->control('lname', ['label' => false]); ?></div>


                            <label class="col-sm-4 col-form-label">Resident ID *</label>
                            <div class="ml-2"><?php echo $this->Form->control('identifier',['label' => false, 'readonly']); ?></div>

                            <label class="col-sm-4 col-form-label">Mobile Phone</label>
                            <div class="ml-2"><?php echo $this->Form->control('phone',['label' => false,'type'=>'tel','maxlength'=>10,'pattern'=>'[0-9]{8,10}','placeholder'=>'04XXXXXXXX']); ?></div>

                            <label class="col-sm-4 col-form-label">Email</label>
                            <div class="ml-2"><?php echo $this->Form->control('email',['label' => false]); ?></div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-4 pt-0 ml-2">Property type</legend>
                                    <div class="col-sm-8 ml-2">
                                        <?php echo $this->Form->control('property_type', ['type'=>'radio','options' => [
                                            ['value' => 'Apartment', 'text' => __('Apartment')],
                                            ['value' => 'Serviced Apartment', 'text' => __('Serviced Apartment')],
                                            ['value' => 'Villa', 'text' => __('Villa')],
                                        ], 'label'=>false]); ?>
                                    </div>
                                </div>
                            </fieldset>


                            <label class="col-sm-4 col-form-label">Landline phone</label>
                            <div class="ml-2"><?php echo $this->Form->control('landline',['label' => false]); ?></div>

                           </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <h6><?= __('Participates in the following clubs:') ?></h6>
                            <?php if (!empty($resident->clubs)): ?>
                                <table class="table table-bordered" >
                                    <?php foreach ($resident->clubs as $clubs): ?>
                                        <tr>
                                            <td><?= h($clubs->name) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php endif; ?>

                            <h6><?= __('Attends these events:') ?></h6>
                            <?php if (!empty($resident->events)): ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <th scope="col"><?= __('Event Name') ?></th>
                                        <th scope="col"><?= __('Event Date') ?></th>
                                    </tr>
                                    <?php foreach ($resident->events as $events): ?>
                                        <tr>
                                            <td><?= h($events->name) ?></td>
                                            <td><?= h($events->event_date->format("d M, Y")) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php endif; ?>
                            <h6><?= __('Is interested in') ?></h6>
                            <?php if (!empty($resident->interests)): ?>
                                <table class="table table-bordered" id="dataTable">
                                    <tr>
                                        <th scope="col"><?= __('Interest') ?></th>
                                    </tr>
                                    <?php foreach ($resident->interests as $interests): ?>
                                        <tr>
                                            <td><?= h($interests->name) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Residents', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

    </main>
</div>


