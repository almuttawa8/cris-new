<div class="residents index large-9 medium-8 columns content">
    <h3><?= __('Archived Items') ?></h3>
    <div class="col-md-6">
        <div class="card">
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Resident Archive", ['controller' => 'Residents', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
               <?= $this->Html->link("View Event Archive", ['controller' => 'Events', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Newsletter Archive", ['controller' => 'Newsletters', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Minute Archive", ['controller' => 'Minutes', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Calendar Archive", ['controller' => 'Calendars', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Food Menu Archive", ['controller' => 'Menus', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
            <div class="btnDiv"  align="center">
                <?= $this->Html->link("View Others Archive", ['controller' => 'Miscellaneous', 'action' =>'archive_index'], array('class' => 'btn btn-primary')) ?>
            </div>
        </div>
    </div>
</div>
