<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<style>
    .container1 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: -10px;
        cursor: pointer;
        font-size: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .container1 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark1 {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container1:hover input ~ .checkmark1 {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container1 input:checked ~ .checkmark1 {
        background-color: #961982;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark1:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container1 input:checked ~ .checkmark1:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container1 .checkmark1:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>



<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->

    <h1 class="mt-4 mb-3">Profile Registration
    </h1>


    <?= $this->Form->create($resident) ?>
    <div class="row ml-2">
        <div class="col-lg-8 mb-4">
            <div class="container">

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Please enter an email or a phone number</label>
                        <div class="col-sm-50">
                            <?php echo $this->Form->control('phone',['label'=>"Mobile Phone (Length: 8-10)",'type'=>'tel','maxlength'=>10,'pattern'=>'[0-9]{8,10}','placeholder'=>'04XXXXXXXX']); ?>
                        </div>
                        <div class="col-sm-50">
                            <?php echo $this->Form->control('email',['label'=>"Email",'type'=>'email']); ?>
                        </div>

                        <p class="help-block"></p>
                    </div>
                </div>


                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-9 pt-0">Please select your property type and enter your landline phone number</legend>
                        <label class="col-sm-4 col-form-label">Property Type</label><br>
                        <div class="col-sm-8 ml-2">
                            <?php echo $this->Form->control('property_type', ['type'=>'radio', 'options' => [
                                ['value' => 'Apartment', 'text' => __('Apartment  ')],
                                ['value' => 'Serviced Apartment', 'text' => __('Serviced Apartment  ')],
                                ['value' => 'Villa', 'text' => __('Villa  ')],
                            ],
                                'label'=>false,
                            ]); ?>
                        </div>
                    </div>
                </fieldset>


                <div class="control-group form-group mt-2">
                    <div class="controls">
                        <div class="col-sm-50">
                            <?php echo $this->Form->control('landline',['label'=>"Landline"]); ?> </div>
                        <p class="help-block"></p>
                    </div>
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-6">
                </div>
            </div>
            <div align="left">
                <input class="btn btn-primary ml-4 mt-2" type="submit" value="Submit">
            </div>

        </div>
    </div>

    <?= $this->Form->end() ?>
</div>



