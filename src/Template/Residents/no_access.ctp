<div class="container mt-5">
    <div class="jumbotron">
        <div align="center">
            <h2>Sorry, you cannot access this page.</h2>
        </div>
        <div align="center">
            <?= $this->Html->link(__('Back'), $this->request->referer(), array('style' => 'color:white', 'class' => 'btn btn-primary')) ?>
        </div>
    </div>
</div>

