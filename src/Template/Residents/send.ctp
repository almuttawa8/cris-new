<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div class="residents form large-9 medium-8 columns content">
    <?= $this->Form->create('Resident', array('action'=>'send')) ?>
    <legend><?= __("Messages") ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="col-md-5">
                    <?php echo $this->Form->input('mail', array('required' => true, 'label' => 'To', 'placeholder' => 'Email')) ?>
                </div>
                <div class="col-md-10">
                    <?php echo $this->Form->input('subject')?>
                </div>
                <div class="col-md-10">
                    <?php echo $this->Form->input('message', array('rows'=>10))?>
                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>



