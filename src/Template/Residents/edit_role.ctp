<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Change roles</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Residents Roles</li>
                <li class="breadcrumb-item active">Change roles</li>
            </ol>
            <?= $this->Form->create($resident) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">
                            <?php echo $this->Form->control('fname', ['label' => 'First Name', 'readonly']); ?>
                            <?php echo $this->Form->control('lname', ['label' => 'Last Name', 'readonly']); ?>
                            <label class="mt-2">Select roles: </label>
                            <?php echo $this->Form->select('roles._ids', $roles, ['multiple' => 'checkbox']); ?>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Residents', 'action' => 'resident_role'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

</main>
</div>

