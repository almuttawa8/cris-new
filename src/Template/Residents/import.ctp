<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Import Resident</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Residents page</li>
                <li class="breadcrumb-item active">Import Resident</li>
            </ol>

            <div class="mt-4 mb-4" align="right">
                <?= $this->Html->link(__('Download Sample File'),'/files/ResidentCSV/file_name/Residents Sample.csv' , array('download' => 'Residents Sample.csv','class' => 'btn btn-primary mb-1', 'style' => 'width: 220px')) ?>

            </div>
            <?= $this->Form->create(null,["type"=>"file"]) ?>
            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">

                            <label class="col-sm-4 col-form-label">Import CSV</label>
                            <div class="col-md-5"><?php echo $this->Form->control('file',["type"=>"file"]); ?></div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Residents', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

    </main>
</div>


