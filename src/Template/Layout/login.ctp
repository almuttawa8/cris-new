<?php
/**
 * Created by PhpStorm.
 * User: bcho27
 * Date: 19-May-19
 * Time: 6:47 PM
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>CRIS</title>
    <?= $this->Html->css('/Admin/css/styles.css') ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body style="background-color: #F0DEF0;">
<div id="layoutAuthentication">

    <?= $this->fetch('content') ?>

<!--    <footer class="footer py-3 fixed-bottom" style="background-color: #961982">-->
<!--        <div class="container">-->
<!--            <p class="m-0 text-center text-white" style="color: white">Copyright © Classic Residents Brighton 2020</p>-->
<!--        </div>-->
<!--    </footer>-->
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<?= $this->Html->script('/Admin/js/scripts.js') ?>
<!--<script src="/Admin/js/scripts.js"></script>-->
</body>
</html>

