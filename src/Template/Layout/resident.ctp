<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CRIS</title>

    <?= $this->Html->css('/vendor/fontawesome-free/css/all.min.css') ?>

    <!-- Custom fonts for this template-->
    <?= $this->Html->css('/user/css/modern-business.css') ?>

    <!-- Page level plugin CSS-->
    <?= $this->Html->css('/user/vendor/bootstrap/css/bootstrap.css') ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
</head>
<style>
      @media (min-width: 1025px) {
            .test1 {

                width: 70%;
            }
        }
        @media (min-width: 1026px) {
            .test1 {
                display: none;
            }
        }

      .test{
           width: 90%;
      }
</style>
<body>

<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top py-3" style="display: flex;">


    <div class="container d-none d-xl-block" style="font-family: Poppins; width: 10%;">

        <a class="navbar-brand" style="color: white;font-size: 17px;">CRIS</a>
    </div>



     <div class=" container test" style="font-family: Poppins; float: left;">
         <div class="nav-item d-xl-none" style="font-size: 17px;">
             <a class="navbar-brand" style="color: white;font-size: 17px;">CRIS</a>
         </div>
         <div class="test1">
         </div>
        <div class="nav-item d-xl-none ml-auto" style="font-size: 17px;">
            <span><?= $this->Html->Link("Logout", ['controller' => 'Residents', 'action' => 'logout'], array('class' => 'nav-link text-white')) ?></span>
        </div>

            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Home", ['controller' => 'Home', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Events", ['controller' => 'Events', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Noticeboard", ['controller' => 'Newsletters', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Services", ['controller' => 'Services', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Marketplace", ['controller' => 'ecommerce', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Maintenance Form", ['controller' => 'Maintenance', 'action' => 'resident_index'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("FAQs", ['controller' => 'Enquiries', 'action' => 'mainView'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item" style="font-size: 17px;">
                    <span><?= $this->Html->link("Admin", ['controller' => 'Home', 'action' => 'HomeIndex'], array('class' => 'nav-link')) ?></span>
                </li>
                <li class="nav-item d-none d-xl-block" style="font-size: 17px;">
                    <span><?= $this->Html->Link("Logout", ['controller' => 'Residents', 'action' => 'logout'], array('class' => 'nav-link')) ?></span>
                </li><li class="nav-item" style="font-size: 17px;">
                    <span><a class="nav-link" href="<?php echo $this->Url->build([
                            "controller" => "Residents",
                            "action" => "profile"
                        ]);?>"> <i class="fas fa-user"></i> <?= $this->request->getSession()->read('Auth.User.fname'); ?></a></span>
                </li>
            </ul>
        </div>

    </div>

</nav>

<div id="wrapper">
    <div id="content-wrapper">
         <div class="container-fluid no-padding" style="font-family: Poppins;">
            <?= $this->fetch('content') ?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->


<footer class="footer py-3 bg-dark" style="font-family: Poppins;">
    <div class="container">
        <p class="m-0 text-center text-white" style="color: white">Copyright © Classic Residents Brighton 2020</p>
    </div>
</footer>


<!-- Bootstrap core JavaScript -->


<?=  $this->Html->script('/user/vendor/jquery/jquery.min.js');?>
<?=  $this->Html->script('/vendor/bootstrap/js/bootstrap.bundle.min.js');?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>

</body>
<script>
    $(document).ready(function () {
        $('#confirmModal').modal('show');
        $("#confirmModal").appendTo("body");
    });

    //myRegistration

    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#datatable').DataTable();

    });
</script>
<script>
    function fixedTitle() {

    }
</script>
<style>

    .dataTables_filter {
        float: right;
    }

</style>
</html>
