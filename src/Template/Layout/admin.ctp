<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - CRIS</title>
    <?= $this->Html->css('/Admin/css/styles.css') ?>
    <?= $this->Html->css('https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js') ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
</head>
<body class="sb-nav-fixed" style="font-family: Poppins;">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="<?php echo $this->Url->build([
        "controller" => "Home",
        "action" => "residentIndex"
    ]);?>">CRIS - Dashboard</a>
    <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    <!-- Navbar-->

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
        <a class="nav-link"  href="<?php echo $this->Url->build([
            "controller" => "Residents",
            "action" => "logout"
        ]);?>">Logout</a></li>
        </ul>

</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">

                <!--Managers -->
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-suitcase"></i></div>
                        Managers
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Home",
                                "action" => "HomeIndex"
                            ]);?>">Home</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Home",
                                "action" => "index"
                            ]);?>">News</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Home",
                                "action" => "restaurant_index"
                            ]);?>">Restaurant details</a>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth"
                            >Events
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                            <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Events",
                                        "action" => "index"
                                    ]);?>">All events</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "registrations",
                                        "action" => "index"
                                    ]);?>">All Registrations</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Locations",
                                        "action" => "index"
                                    ]);?>">Locations</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseErrors" aria-expanded="false" aria-controls="pagesCollapseErrors"
                            >Noticeboard Categories
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                            <div class="collapse" id="pagesCollapseErrors" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Newsletters",
                                        "action" => "index"
                                    ]);?>">Newsletters</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Minutes",
                                        "action" => "index"
                                    ]);?>">Minutes</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Calendars",
                                        "action" => "index"
                                    ]);?>">Calendars</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Menus",
                                        "action" => "index"
                                    ]);?>">Food Menus</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Miscellaneous",
                                        "action" => "index"
                                    ]);?>">Others</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuths" aria-expanded="false" aria-controls="pagesCollapseAuths"
                            >Marketplace
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                            <div class="collapse" id="pagesCollapseAuths" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Ecommerce",
                                        "action" => "index"
                                    ]);?>">New applications</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Ecommerce",
                                        "action" => "accepted_index"
                                    ]);?>">Accepted applications</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Ecommerce",
                                        "action" => "sold_index"
                                    ]);?>">Sold applications</a>
                                </nav>
                            </div>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Services",
                                "action" => "index"
                            ]);?>">Services</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Maintenance",
                                "action" => "index"
                            ]);?>">Maintenance</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Enquiries",
                                "action" => "index"
                            ]);?>">FAQs</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Enquiries",
                                "action" => "EnquiriesIndex"
                            ]);?>">Enquiries</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Residents",
                                "action" => "index"
                            ]);?>">Resident</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Interests",
                                "action" => "index"
                            ]);?>">Interests</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Roles",
                                "action" => "index"
                            ]);?>">Roles</a>

                        </nav>
                    </div>


                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                        <div class="sb-nav-link-icon"><i class="fas fa-archive"></i></div>
                        Archive
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Home",
                                "action" => "archive_index"
                            ]);?>">News</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Events",
                                "action" => "archive_index"
                            ]);?>">Event</a>
                            <a class="nav-link" href="<?php echo $this->Url->build([
                                "controller" => "Residents",
                                "action" => "archive_index"
                            ]);?>">Residents</a>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth"
                            >Noticeboard Categories
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                ></a>
                            <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Newsletters",
                                        "action" => "archive_index"
                                    ]);?>">Newsletters</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Minutes",
                                        "action" => "archive_index"
                                    ]);?>">Minutes</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Calendars",
                                        "action" => "archive_index"
                                    ]);?>">Calendars</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Menus",
                                        "action" => "archive_index"
                                    ]);?>">Food Menus</a>
                                    <a class="nav-link" href="<?php echo $this->Url->build([
                                        "controller" => "Miscellaneous",
                                        "action" => "archive_index"
                                    ]);?>">Others</a>
                                </nav>
                            </div>

                        </nav>
                    </div>

                    <a class="nav-link" href="<?php echo $this->Url->build([
                        "controller" => "Residents",
                        "action" => "current_committee"
                    ]);?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                        Current Committee</a>
                    <a class="nav-link" href="<?php echo $this->Url->build([
                        "controller" => "Residents",
                        "action" => "resident_role"
                    ]);?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-exchange-alt"></i></div>
                        Change Users' Roles</a>
                    <a class="nav-link" href="<?php echo $this->Url->build([
                        "controller" => "Home",
                        "action" => "residentIndex"
                    ]);?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                        Home page</a>
                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Welcome
                <?= $this->request->getSession()->read('Auth.User.fname'); ?></div>
            </div>
        </nav>


    </div>
        <?= $this->fetch('content') ?>

</div>



<!--<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>-->
<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>-->
<?=$this->Html->script('https://code.jquery.com/jquery-3.4.1.min.js');?>
<?=$this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js');?>
<?=  $this->Html->script('/Admin/js/scripts.js'); ?>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>-->

<!--<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>-->
<!--<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>-->
<?=$this->Html->script('https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js');?>
<?=$this->Html->script('https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js');?>
<?=$this->Html->script('/Admin/assets/demo/datatables-demo.js');?>
<!---->
<!-- Bootstrap core JavaScript-->
<?= $this->Html->script('/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>


<!-- Custom scripts for all pages-->
<?= $this->Html->script('/js/sb-admin.min.js') ?>
<?= $this->Html->script('/js/jquery-confirm.js') ?>


<?=  $this->Html->script('/js/lightbox.js');?>

<!-- For datepicker -->
<?=  $this->Html->css('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');?>
<?=  $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.js');?>

<?=  $this->Html->script('/js/jquery.timepicker.min.js');?>
<?=  $this->Html->script('/js/jquery.timepicker.js');?>


<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<body>
<script>
    $('#example').timepicker({
        uiLibrary: 'bootstrap4'
    });
</script>
<script>
    $('#example1').timepicker({
        uiLibrary: 'bootstrap4'
    });
</script>

<script>
    $(document).ready(function () {
        $('#confirmModal').modal('show');
        $("#confirmModal").appendTo("body");
    });
</script>
<script>
    $(document).on('focusin', '.eventdatep', function(){
        $(this).datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true
        });

    });


</script>

<!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"-->
<!--        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"-->
<!--        crossorigin="anonymous">-->
<!--</script>-->

<!--<script>-->
<!--    $(function(){-->
<!--        $('#example').timepicker({-->
<!--            timeFormat: 'HH:mm',-->
<!--            interval:5-->
<!--        });-->
<!--    });-->
<!--</script>-->


<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true
        });
    } );
</script>

<script>
    $(function(){
        $('#example1').timepicker({
            timeFormat: 'HH:mm',
            interval:5
        });
    });

    $(document).ready(function () {
        var i =1;
        $('#add').click(function () {
            i++;
            $('#dynamic_field').append(' <tr id="row'+i+'"><td><input type="type"  name="event_date[]" autocomplete="off"  class="eventdatep form-control name_list " required></td><td><button name="remove" id="'+i+'" class="btn btn-primary btn_primary btn_remove">Remove</button></td></tr>');

        })
        $(document).on('click','.btn_remove', function () {
            var button_id=$(this).attr("id");
            $('#row'+button_id+'').remove();
        })

    })
</script>



</body>
</html>
