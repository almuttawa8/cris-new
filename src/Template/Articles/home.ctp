<?php
$this->assign('title', 'CRIS');
$this->layout = 'login';
//$this->render(true);
?>

<div class="container">
    <div class="jumbotron">
        <div class="card-img-top" align="center">
            <img src="img/crb-logo.png" width="250px">
        </div>
        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <div align="center">
                        <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link("Login", ['controller' => 'Residents', 'action' => 'login'], array('style' => 'color:white')) ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
