<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Topic $topic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Topic'), ['action' => 'edit', $topic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Topic'), ['action' => 'delete', $topic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $topic->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Topics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topic'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Enquiries'), ['controller' => 'Enquiries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Enquiry'), ['controller' => 'Enquiries', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="topics view large-9 medium-8 columns content">
    <h3><?= h($topic->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($topic->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($topic->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Enquiries') ?></h4>
        <?php if (!empty($topic->enquiries)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Resident Id') ?></th>
                <th scope="col"><?= __('Subject') ?></th>
                <th scope="col"><?= __('Body') ?></th>
                <th scope="col"><?= __('Solved') ?></th>
                <th scope="col"><?= __('Solution') ?></th>
                <th scope="col"><?= __('Response Type') ?></th>
                <th scope="col"><?= __('Topic Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($topic->enquiries as $enquiries): ?>
            <tr>
                <td><?= h($enquiries->id) ?></td>
                <td><?= h($enquiries->resident_id) ?></td>
                <td><?= h($enquiries->subject) ?></td>
                <td><?= h($enquiries->body) ?></td>
                <td><?= h($enquiries->solved) ?></td>
                <td><?= h($enquiries->solution) ?></td>
                <td><?= h($enquiries->response_type) ?></td>
                <td><?= h($enquiries->topic_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Enquiries', 'action' => 'view', $enquiries->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Enquiries', 'action' => 'edit', $enquiries->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Enquiries', 'action' => 'delete', $enquiries->id], ['confirm' => __('Are you sure you want to delete # {0}?', $enquiries->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
