<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Topic[]|\Cake\Collection\CollectionInterface $topics
 */
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <?= $this->Html->link("Back", ['controller'=>'Enquiries', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
</div>
<div id="dataTable_filter" class="dataTables_filter" align="right">
    <?= $this->Html->link("Add Topic", ['controller'=>'Topics', 'action' => 'add'], array('class' => 'btn btn-primary')) ?>
</div>

<div class="table-responsive">
    <div class="topics index large-9 medium-8 columns content">
        <h3><?= __('Topics') ?></h3>
        <table class="table table-bordered" id="dataTable">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($topics as $topic): ?>
                <tr>
                    <td><?= h($topic->name) ?></td>
                    <td class="actions" width="30%" align="center">
                        <button class="btn btn-primary" style="margin-right:30px"><?= $this->Html->link(__('Edit'), ['action' => 'edit', $topic->id], array('style' => 'color:white')) ?></button>
                        <button type ="button" class="btn btn-primary3" data-toggle="modal" data-target="#confirmModal-<?= $topic->id?>">Delete</button>

                        <div class="modal fade" id="confirmModal-<?= $topic->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to delete this enquiry topic - <?= $topic->name?>?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"><?= $this->Form->postLink(__('Yes'), ['action' => 'delete', $topic->id]) ?></button>
                                        <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                <?php endforeach; ?>
                <script>
                    $(document).ready(function () {
                        $('#confirmModal').modal('show');
                        $("#confirmModal").appendTo("body");
                    });
                </script>
            </tbody>
        </table>

    </div>
</div>
