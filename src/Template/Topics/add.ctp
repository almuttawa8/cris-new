<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Topic $topic
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>
<div id="dataTable_filter" class="dataTables_filter" align="left">
    <button class="btn btn-primary"><?= $this->Html->link(__('Back'), ['controller' => 'Topics', 'action' => 'index'], array('style' => 'color:white')) ?></button>
</div>
<div class="topics form large-9 medium-8 columns content">
    <?= $this->Form->create($topic) ?>
    <legend><?= __('Add Topic') ?></legend>
    <div class="row">
        <div class="col-md-6">
            <div class="card container">

                <br>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Topic *</label>
                    <div class="col-sm-5">
                        <?php echo $this->Form->control('name'); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

