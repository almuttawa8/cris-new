<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Club $club
 */
$this->Form->setTemplates(\Cake\Core\Configure::read('FormTemplates.Admin'));
?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Club</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Clubs page</li>
                <li class="breadcrumb-item active">Edit Club</li>
            </ol>
            <?= $this->Form->create($club) ?>

            <div class="row mt-2">
                <div class="col-xl-9">
                    <div class="card container">
                        <div class="form-group">

                            <label class="col-sm-4 col-form-label">Club Name *</label>
                            <div class="ml-2"> <?php echo $this->Form->control('name', ['label' => false]); ?></div>

                            <div class="ml-2"> <?php echo $this->Form->control('description',['label'=>"Description",'rows'=>'2']); ?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div align="left" class="mt-4 ml-4 mb-4">
            <?= $this->Html->link(__('Back'), ['controller' => 'Clubs', 'action' => 'index'], array('class' => 'btn btn-primary')) ?>
            <input class="btn btn-primary" type="submit" value="Submit">
        </div>
        <?= $this->Form->end() ?>

    </main>
</div>
