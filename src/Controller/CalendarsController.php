<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Calendars Controller
 *
 * @property \App\Model\Table\CalendarsTable $Calendars
 *
 * @method \App\Model\Entity\Calendar[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CalendarsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }


        $calendars = $this->Calendars->find('all')->where(['Calendars.archived' => false])->contain([]);

        $this->set(compact('calendars'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $calendar = $this->Calendars->newEntity();
        if ($this->request->is('post')) {

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->getData());
            $calendar->date_published = $date_published;
            //$calendar->display_field = "Calendar_" . $calendar->date_published->format("Y/m/d");

            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('The calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The calendar could not be saved. Please, try again.'));
        }
        $this->set(compact('calendar'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $calendar = $this->Calendars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->getData());
            $calendar->date_published = $date_published;

            //$calendar->display_field = "Calendar_" . $calendar->date_published->format("Y/m/d");


            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('The calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The calendar could not be saved. Please, try again.'));
        }
        $this->set(compact('calendar'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $calendar = $this->Calendars->get($id);
        if ($this->Calendars->delete($calendar)) {
            $this->Flash->success(__('The calendar has been deleted.'));
        } else {
            $this->Flash->error(__('The calendar could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        $calendar = $this->Calendars->get($id);
        $calendar->archived = true;

        if ($this->Calendars->save($calendar)){
            $this->Flash->success(__('This calendar has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this calendar.'));

    }

    public function restore($id = null)
    {
        $calendar = $this->Calendars->get($id);
        $calendar->archived = false;

        if ($this->Calendars->save($calendar)) {
            $this->Flash->success(__('This calendar has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this calendar.'));
    }


    public function archiveIndex()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedCalendars = TableRegistry::get('Calendars')->find('all')->where(['Calendars.archived' => true])->contain([]);
        $this->set('archivedCalendars', $archivedCalendars);

    }

    public function residentIndex()
    {
        $this->layout = 'resident';
        $calendars = $this->Calendars->find('all')->where(['Calendars.archived' => false])->contain([]);

        $this->set(compact('calendars'));
    }
}
