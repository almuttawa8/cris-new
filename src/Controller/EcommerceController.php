<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;


/**
 * @property \App\Model\Table\EcommerceTable $Ecommerce
 * @property \App\Model\Table\ResidentsTable $Residents
 */

class EcommerceController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
        $this->loadModel('Ecommerce');
        $this->loadComponent('Paginator');
    }

    public function residentIndex()
    {
        $this->layout = 'resident';
        $itemName = $this->request->getData();
        if ($itemName=='') {
            $this->paginate = [
                'order' => [
                    'name' => 'asc'
                ],
                'limit' => 9
            ];
            $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'available']));
        }
        else{
            if ($this->request->is('post')) {
                if(!isset($itemName['itemName'])){
                    $this->paginate = [
                        'order' => [
                            'name' => 'asc'
                        ],
                        'limit' => 9
                    ];
                    $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'available']));
                }
                else{
                    $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['name LIKE' => "%".$itemName['itemName']."%",'Ecommerce.wanted_for' => 'available']));
                }
            }
            else{
                $this->paginate = [
                    'order' => [
                        'name' => 'asc'
                    ],
                    'limit' => 9
                ];
                $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'available']));
            }
        }

        $lastmonth = date('Y-m-d', strtotime('-30 days'));

        $record = $this->Ecommerce->find('all')->where([ 'Ecommerce.date <' => $lastmonth]);
        foreach ($record as $res){
            $this->Ecommerce->delete($res);
        }


        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $this->set(compact('ecommerce', 'residents'));

    }
    public function buyIndex()
    {
        $this->layout = 'resident';
        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $this->loadModel('Ecommerce');
        $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => true]));
        $this->set('ecommerce', $ecommerce );
        $this->set(compact('ecommerce', 'residents'));

    }

    public function apply()
    {
        $this->layout = 'resident';
        //get the user ID
        $userID = $this->Auth->user('id');



        $ecommerce = $this->Ecommerce->newEntity();
        if ($this->request->is('post')) {
            $ecommerce = $this->Ecommerce->patchEntity($ecommerce, $this->request->getData());
            //save the user ID
            $ecommerce->resident_id = $userID;
            if ($this->Ecommerce->save($ecommerce)) {
                $this->Flash->success('Your item has been sent. It will be added to the page when accepted by admin', [
                    'key' => 'success_enquiry'
                ]);

                return $this->redirect(['action' => 'residentIndex']);
            }
            $this->Flash->error('Your item has not been sent. Please try again.', [
                'key' => 'fail_enquiry'
            ]);
        }
        $this->set('ecommerce');
    }

    public function view($id = null)
    {
        $this->layout = 'resident';
        $ecommerce = $this->Ecommerce->get($id);
        $this->set('ecommerce', $ecommerce);

    }

    public function myItems($id = null)
    {
        $userID = $this->Auth->user('id');
        $this->layout = 'resident';
        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $this->loadModel('Ecommerce');
        $ecommerce = $this->paginate($this->Ecommerce->find('all')->where(['Ecommerce.resident_id' => $userID]));
        $this->set('ecommerce', $ecommerce );
        $this->set(compact('ecommerce', 'residents'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ecommerce = $this->Ecommerce->get($id);
        if ($this->Ecommerce->delete($ecommerce)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'resident_index']);
    }

    public function editItem($id = null)
    {
        $this->layout = 'resident';
        $ecommerce = $this->Ecommerce->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ecommerce->wanted_for = 'pending';
            $ecommerce = $this->Ecommerce->patchEntity($ecommerce, $this->request->getData());
            if ($this->Ecommerce->save($ecommerce)) {
                $this->Flash->success('You have successfully edited your application.', [
                    'key' => 'success_edit_registration'
                ]);

                return $this->redirect(['controller' => 'Ecommerce', 'action' => 'my_items']);
            }
            $this->Flash->error(__('The application details could not be saved. Please, try again.'));
        }
        $this->set(compact('ecommerce'));
    }

    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }




        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'pending']);
        $this->set(compact('ecommerce', 'residents'));
    }

    public function itemDetails($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }

        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->get($id);
        $this->set(compact('ecommerce', 'residents'));
    }

    public function acceptedDetails($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }

        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->get($id);
        $this->set(compact('ecommerce', 'residents'));
    }

    public function approveItem($id = null)
    {
        $this->loadModel('Ecommerce');
        $ecommerce = $this->Ecommerce->get($id);

            $ecommerce->wanted_for='available';
        if ($this->Ecommerce->save($ecommerce)){
            $this->Flash->success(__('This application has been approved.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to approve this application.'));
    }

    public function soldItem($id = null)
    {
        $ecommerce = $this->Ecommerce->get($id);
        $ecommerce->wanted_for='sold';
        if ($this->Ecommerce->save($ecommerce)){
            $this->Flash->success(__('Changed to sold.'));

            return $this->redirect(['action' => 'my_items']);

        }
        $this->Flash->error(__('Unable to approve this change.'));
    }

    public function deleteItem($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ecommerce = $this->Ecommerce->get($id);
        if ($this->Ecommerce->delete($ecommerce)) {
            $this->Flash->success(__('The application has been deleted.'));
        } else {
            $this->Flash->error(__('The application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function deleteItems($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ecommerce = $this->Ecommerce->get($id);
        if ($this->Ecommerce->delete($ecommerce)) {
            $this->Flash->success(__('The application has been deleted.'));
        } else {
            $this->Flash->error(__('The application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'accepted_index']);
    }
    public function deleteSold($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ecommerce = $this->Ecommerce->get($id);
        if ($this->Ecommerce->delete($ecommerce)) {
            $this->Flash->success(__('The application has been deleted.'));
        } else {
            $this->Flash->error(__('The application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'sold_index']);
    }

    public function acceptedIndex()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }

        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'available']);
        $this->set(compact('ecommerce', 'residents'));
    }
    public function soldIndex()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }


        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->find('all')->where(['Ecommerce.wanted_for' => 'sold']);
        $this->set(compact('ecommerce', 'residents'));
    }


    public function soldDetails($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }

        $this->loadModel('Residents');
        $residents = $this->Residents->find()->all();
        $this->set('residents', $residents );
        $ecommerce = $this->Ecommerce->get($id);
        $this->set(compact('ecommerce', 'residents'));
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
}
