<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;



/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EventsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
        $this->loadModel('Registrations');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        // First get all events that is not archived which date is yesterday, then make it archived.
        $archivedEvents = $this->Events->find('all')->where(['Events.archived' => false , 'Events.event_date <' => date('Y-m-d')]);
        foreach ($archivedEvents as $event){

          $this->archive($event->id);

        }
        $events = $this->Events->find('all')->where(['Events.archived' => false])->contain(['Locations']);
        $this->set(compact('events'));
    }

    /**
     * View method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $event = $this->Events->get($id, [
            'contain' => ['Locations', 'Residents']
        ]);

        $this->set('event', $event);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $event = $this->Events->newEntity();
        if ($this->request->is('post')) {

            //Convert date from the form to TimeObject
            $event_date_array[] = $this->request->getData('event_date');

            $flag = 0;
            foreach ( $event_date_array[0] as $event_date_raw) {

               $event = $this->Events->newEntity();
               $event_date = Time::createFromFormat("d-m-Y", $event_date_raw);

               //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
               $event = $this->Events->patchEntity($event, $this->request->getData());
               $event->event_date = $event_date;

                if ($this->Events->save($event)) {
                    $flag = 1;
                }

            }
            if($flag == 1){
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            else{
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        }
        $locations = $this->Events->Locations->find('list', ['limit' => 200]);
        $residents = $this->Events->Residents->find('list', ['limit' => 200]);
        $this->set(compact('event', 'locations', 'residents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        //get the user who registers this event
        $query = $this->Registrations->find();
        $query = $query->select([
            'resident' => 'Residents.id',
        ])
            ->where(['Registrations.event_id' => $id]);

        $total = $query->count();

        $event = $this->Events->get($id, [
            'contain' => ['Residents']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Convert date from the form to TimeObject
            $event_date_raw = $this->request->getData('event_date');
            $event_date = Time::createFromFormat("d-m-Y", $event_date_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $event = $this->Events->patchEntity($event, $this->request->getData());
            $event->event_date = $event_date;

            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                if ($total > 0){
                    $this->Flash->success('Please notify the residents of the changes', [
                        'key' => 'notify_resident'
                    ]);
                    return $this->redirect(['action' => 'index']);
                }

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $locations = $this->Events->Locations->find('list', ['limit' => 200]);
        $residents = $this->Events->Residents->find('list', ['limit' => 200]);
        $this->set(compact('event', 'locations', 'residents', 'total'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $event = $this->Events->get($id);
        if ($this->Events->delete($event)) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $event = $this->Events->get($id);
        $event->archived = true;

        if ($this->Events->save($event)){
            $this->Flash->success(__('This event has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this event.'));
    }

    public function restore($id = null)
    {
        $event = $this->Events->get($id);
        $event->archived = false;

        if ($this->Events->save($event)) {
            $this->Flash->success(__('This event has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this event.'));
    }


    public function archiveIndex(){

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedEvents = TableRegistry::get('Events')->find('all')->where(['Events.archived' => true])->contain(['Locations']);
        $this->set('archivedEvents', $archivedEvents);
    }


    public function residentIndex()
    {
        $this->layout = 'resident';
        $eventName = $this->request->getData();

        if ($eventName=='') {
            $this->paginate = [
                'contain' => ['Locations'],
                'order' => [
                    'event_date' => 'asc'
                ],
                'limit' => 9
            ];
            $events = $this->paginate($this->Events->find('all')->where(['Events.archived' => false, 'Events.event_date >= NOW()'])->contain([]));

        }
        else{
            if ($this->request->is('post')) {
                if(!isset($eventName['eventName'])){
                    $this->paginate = [
                        'contain' => ['Locations'],
                        'order' => [
                            'event_date' => 'asc'
                        ],
                        'limit' => 9
                    ];
                    $events = $this->paginate($this->Events->find('all')->where(['Events.archived' => false, 'Events.event_date >= NOW()'])->contain([]));
                }
                else{
                    $events = $this->paginate($this->Events->find('all')->where(['name LIKE' => "%".$eventName['eventName']."%",'Events.archived' => false, 'Events.event_date >= NOW()']));

                }
            }
            else{
                $this->paginate = [
                    'contain' => ['Locations'],
                    'order' => [
                        'event_date' => 'asc'
                    ],
                    'limit' => 9
                ];
                $events = $this->paginate($this->Events->find('all')->where(['Events.archived' => false, 'Events.event_date >= NOW()'])->contain([]));
            }
        }

        $this->set(compact('events'));
    }



    public function registrationEvent($id=null){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }


        //get the user who registers this event
        $query = $this->Registrations->find();

        $query = $query->select([
            'resident' => 'Residents.id',
        ])
            ->where(['Registrations.event_id' => $id]);

        $regQuery = $this->Registrations->find();
        $regQuery = $regQuery->select([
            'resident' => 'Residents.id',
        ])
            ->where(['Registrations.event_id' => $id]);


        $total = $query->count();

        $event = $this->Events->get($id, [
            'contain' => ['Residents']
        ]);

        $locations = $this->Events->Locations->find('list', ['limit' => 200]);
        $residents = $this->Events->Residents->find('list', ['limit' => 200]);
        $this->set(compact('event', 'locations', 'residents', 'total','registrations'));
}

}
