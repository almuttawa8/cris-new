<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Properties Controller
 *
 * @property \App\Model\Table\PropertiesTable $Properties
 *
 * @method \App\Model\Entity\Property[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PropertiesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $properties = $this->Properties->find();

        $this->set(compact('properties'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $property = $this->Properties->newEntity();
        if ($this->request->is('post')) {
            $property = $this->Properties->patchEntity($property, $this->request->getData());
            if ($this->Properties->save($property)) {
                $this->Flash->success(__('The property has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The property could not be saved. Please, try again.'));
        }
        $this->set(compact('property'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Property id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $property = $this->Properties->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $property = $this->Properties->patchEntity($property, $this->request->getData());
            if ($this->Properties->save($property)) {
                $this->Flash->success(__('The property has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The property could not be saved. Please, try again.'));
        }
        $this->set(compact('property'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Property id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $property = $this->Properties->get($id);
        if ($this->Properties->delete($property)) {
            $this->Flash->success(__('The property has been deleted.'));
        } else {
            $this->Flash->error(__('The property could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function import()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        if ($this->request->is('post')) {

            $csvArray=array_map('str_getcsv',file($this->request->getData('file')['tmp_name']));
            foreach($csvArray as $index => $singleProperty) {
                if($index == 0){ continue; }

                $address=$singleProperty[0];
                $propertyid=$this->Properties->findByAddress($address)->first();
//
                //Save one property
                $property = $this->Properties->newEntity();
                $property = $this->Properties->patchEntity($property, ['address' => $singleProperty[0],
                    'type' => $singleProperty[1],
                    'landline' => $singleProperty[2]
                ]);

                if(!is_null($propertyid)){
                    $property->id=$propertyid->id;
                }
//
                $result=$this->Properties->save($property);
//
                if (!$result) {
                    $this->Flash->error(__('The property could not be saved. Please, try again.'));
                }
            }
//            exit;
            $this->Flash->success(__('The property has been saved.'));
            return $this->redirect(['action' => 'index']);

        }



    }
}

