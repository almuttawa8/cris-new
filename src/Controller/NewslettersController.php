<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Newsletters Controller
 *
 * @property \App\Model\Table\NewslettersTable $Newsletters
 *
 * @method \App\Model\Entity\Newsletter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NewslettersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $newsletters = $this->Newsletters->find('all')->where(['Newsletters.archived' => false])->contain([]);

        $this->set(compact('newsletters'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $newsletter = $this->Newsletters->newEntity();
        if ($this->request->is('post')) {
//            debug($this->request->getData());
//            debug($this->request->getUploadedFiles());

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->getData());
            $newsletter->date_published = $date_published;


            if ($this->Newsletters->save($newsletter)) {
                $this->Flash->success(__('The newsletter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The newsletter could not be saved. Please, try again.'));
        }
        $this->set(compact('newsletter'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $newsletter = $this->Newsletters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->getData());
            $newsletter->date_published = $date_published;

            if ($this->Newsletters->save($newsletter)) {
                $this->Flash->success(__('The newsletter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The newsletter could not be saved. Please, try again.'));
        }
        $this->set(compact('newsletter'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newsletter = $this->Newsletters->get($id);
        if ($this->Newsletters->delete($newsletter)) {
            $this->Flash->success(__('The newsletter has been deleted.'));
        } else {
            $this->Flash->error(__('The newsletter could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        $newsletter = $this->Newsletters->get($id);
        $newsletter->archived = true;

        if ($this->Newsletters->save($newsletter)){
            $this->Flash->success(__('This newsletter has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this newsletter.'));
    }

    public function restore($id = null)
    {
        $newsletter = $this->Newsletters->get($id);
        $newsletter->archived = false;

        if ($this->Newsletters->save($newsletter)) {
            $this->Flash->success(__('This newsletter has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this newsletter.'));
    }


    public function archiveIndex(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedNewsletters = TableRegistry::get('Newsletters')->find('all')->where(['Newsletters.archived' => true])->contain([]);
        $this->set('archivedNewsletters', $archivedNewsletters);
    }

    public function residentIndex(){
        $this->layout = 'resident';
        $newsletters = $this->Newsletters->find('all')->where(['Newsletters.archived' => false])->contain([]);
        $this->set(compact('newsletters'));
    }


}
