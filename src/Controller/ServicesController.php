<?php
namespace App\Controller;

use App\Controller\AppController;
use PhpParser\Node\Expr\Isset_;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 *
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ServicesController extends AppController
{
    public $paginate = ['order' => ['Services.name'=> 'asc']];
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $services = $this->paginate($this->Services);

        $this->set(compact('services'));
    }

    /**
     * View method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => ['Events', 'Categories']
        ]);

        $this->set('service', $service);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $service = $this->Services->newEntity();
        if ($this->request->is('post')) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $events = $this->Services->Events->find('list', ['limit' => 200]);
        $categories = $this->Services->Categories->find('list', ['limit' => 200, 'order' => ['Categories.name' => 'ASC']]);
        $this->set(compact('service', 'events', 'categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $service = $this->Services->get($id, [
            'contain' => ['Events', 'Categories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $events = $this->Services->Events->find('list', ['limit' => 200]);
        $categories = $this->Services->Categories->find('list', ['limit' => 200]);
        $this->set(compact('service', 'events', 'categories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get($id);
        if ($this->Services->delete($service)) {
            $this->Flash->success(__('The service has been deleted.'));
        } else {
            $this->Flash->error(__('The service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function residentIndex(){
        $filterid = $this->request->getParam('pass');
        $this->layout = 'resident';
        if ($filterid != null) {
            $this->paginate = [
                'order' => [
                    'name' => 'asc'
                ],
                'limit' => 9
            ];
            $filterid = $filterid[0];
            $serviceName = $this->request->getData();
            if ($serviceName=='') {
                $services = $this->paginate($this->Services->find('all')->innerJoinWith('Categories')->where(['Categories.id' => $filterid]));
            }
            else{
                $services = $this->paginate($this->Services->find('all')->innerJoinWith('Categories')->where(['Categories.id' => $filterid]));
            }
        }
        else {
            if ($this->request->is('post')) {
                $serviceName = $this->request->getData();
                if(!isset($serviceName['serviceName'])){
                    $this->paginate = [
                        'order' => [
                            'name' => 'asc'
                        ],
                        'limit' => 9
                    ];
                    $services = $this->paginate($this->Services->find());
                }
                else{
                    $services = $this->paginate($this->Services->find('all')->where(['name LIKE' => "%".$serviceName['serviceName']."%"]));
                }
            }
            else{
                $this->paginate = [
                    'order' => [
                        'name' => 'asc'
                    ],
                    'limit' => 9
                ];
                $services = $this->paginate($this->Services->find());
            }

        }



        $categories = $this->Services->Categories->find()->order(['name' => 'asc']);
        $this->set(compact('services','categories'));
    }


}
