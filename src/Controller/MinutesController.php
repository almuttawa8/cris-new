<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Minutes Controller
 *
 * @property \App\Model\Table\MinutesTable $Minutes
 *
 * @method \App\Model\Entity\Minute[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MinutesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $minutes = $this->Minutes->find('all')->where(['Minutes.archived' => false])->contain(['MinCategories']);

        $this->set(compact('minutes'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $minute = $this->Minutes->newEntity();

        if ($this->request->is('post')) {

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $minute = $this->Minutes->patchEntity($minute, $this->request->getData());
            $minute->date_published = $date_published;

            //$minute->display_field = "Minutes_" . $minute->date_published->format("Y/m/d");


            if ($this->Minutes->save($minute)) {
                $this->Flash->success(__('The minute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The minute could not be saved. Please, try again.'));
        }
        $minCategories = $this->Minutes->MinCategories->find('list', ['limit' => 200, 'order' => ['MinCategories.name' => 'ASC']]);
        $this->set(compact('minute', 'minCategories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Minute id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $minute = $this->Minutes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Convert date from the form to TimeObject
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $minute = $this->Minutes->patchEntity($minute, $this->request->getData());
            $minute->date_published = $date_published;

            //$minute->display_field = "Minutes_" . $minute->date_published->format("Y/m/d");
            if ($this->Minutes->save($minute)) {
                $this->Flash->success(__('The minute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The minute could not be saved. Please, try again.'));
        }
        $minCategories = $this->Minutes->MinCategories->find('list', ['limit' => 200, 'order' => ['MinCategories.name' => 'ASC']]);
        $this->set(compact('minute', 'minCategories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Minute id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $minute = $this->Minutes->get($id);
        if ($this->Minutes->delete($minute)) {
            $this->Flash->success(__('The minute has been deleted.'));
        } else {
            $this->Flash->error(__('The minute could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        $minute = $this->Minutes->get($id);
        $minute->archived = true;

        if ($this->Minutes->save($minute)){
            $this->Flash->success(__('This minutes has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this minutes.'));

    }

    public function restore($id = null)
    {
        $minute = $this->Minutes->get($id);
        $minute->archived = false;

        if ($this->Minutes->save($minute)) {
            $this->Flash->success(__('This minutes has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this minutes.'));
    }


    public function archiveIndex()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedMinutes = TableRegistry::get('Minutes')->find('all')->where(['Minutes.archived' => true])->contain(['MinCategories']);
        $this->set('archivedMinutes', $archivedMinutes);

    }

    public function residentIndex()
    {
        $this->layout = 'resident';
        $minutes = $this->Minutes->find('all')->where(['Minutes.archived' => false])->contain(['MinCategories']);

        $this->set(compact('minutes'));
    }

}
