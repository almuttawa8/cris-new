<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * @property \App\Model\Table\MaintenanceDetailsTable $MaintenanceDetails
 * @property \App\Model\Table\MaintenanceTable $Maintenance
 * @property \App\Model\Table\ResidentsTable $Residents
 */

class MaintenanceController extends AppController
{

    public function initialize()
    {

        parent::initialize();
        $this->loadModel('MaintenanceDetails');
        $this->loadModel('Residents');
        $this->loadModel('Maintenance');
    }

    public function residentIndex()
    {
        $this->loadModel('Residents');
        $this->layout = 'resident';
        $userID = $this->Auth->user('id');
        $Maintenance = $this->Maintenance->newEntity();
        if ($this->request->is('post')) {
            $Maintenance = $this->Maintenance->patchEntity($Maintenance, $this->request->getData());
            //save the user ID
            $Maintenance->resident_id = $userID;
            $Residents = $this->Residents->find('all');
            $name = '';
            $date='';
            if ($receiptno=$this->Maintenance->save($Maintenance)) {
                 foreach ($Residents as $resident):
                        if($receiptno->resident_id == $resident->id){
                           $name= ($resident->fname)." ".($resident->lname);
                         }
                 endforeach;
                $forms = $this->Maintenance->find('all');
                foreach ($forms as $form):
                    if($receiptno->id == $form->id){
                        $date= $form->date->format("d M, Y");
                    }
                endforeach;
                $email = new Email();
                $email->setViewVars(['name'=>$name,'apartment_no'=>$receiptno->apartment_no,'phone'=>$receiptno->phone,'id' => $receiptno->id,'resident_name'=>$receiptno->resident_name,'email'=>$receiptno->email,'desc'=>$receiptno->maintenance_desc,'access'=>$receiptno->internal_access,'date'=>$date]);
                if($receiptno->image_name==''){
                    $email->from(['cris.intranet.user@gmail.com' => 'CRIS'])
                        ->template('default', 'default')
                        ->emailFormat('html')
                        ->to('griffin.vanHeeswyk@lendlease.com')
                        ->subject('Maintenance Form')
                        ->send();

                }else{
                    $email->from(['cris.intranet.user@gmail.com' => 'CRIS'])
                        ->template('default', 'default')
                        ->emailFormat('html')
                        ->to('griffin.vanHeeswyk@lendlease.com')
                        ->subject('Maintenance Form')
                        ->attachments('/home/classi10/public_html/webroot/files/Maintenance/image_name/'.$receiptno->image_name)
                        ->send();

                }

                return $this->redirect(['action' => 'formConfirm/'.$receiptno->id]);
            }
            $this->Flash->error('Your request has not been sent. Please try again.', [
                'key' => 'fail_enquiry'

            ]);

        }
        $MaintenanceDetails = $this->MaintenanceDetails->find('all');
        $this->set(compact('MaintenanceDetails','Maintenance'));
    }
    public function index()
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }


        $MaintenanceDetails = $this->MaintenanceDetails->find('all');
        $this->set(compact('MaintenanceDetails'));
    }

    public function formConfirm($receiptno)
    {
        $this->layout = 'resident';
        $MaintenanceDetails = $this->MaintenanceDetails->find('all');
        $data['id']=$receiptno;
        $this->set(compact('MaintenanceDetails','data'));

    }



    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function editIndex($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $MaintenanceDetails = $this->MaintenanceDetails->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $MaintenanceDetails = $this->MaintenanceDetails->patchEntity($MaintenanceDetails, $this->request->getData());

            if ($this->MaintenanceDetails->save($MaintenanceDetails)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $this->set(compact('MaintenanceDetails'));
    }
}
