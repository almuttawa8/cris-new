<?php
namespace App\Controller;
use Cake\Core\Configure;
use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
* @property \App\Model\Table\ServicesTable $Services
 * @property \App\Model\Table\NewsTable $News
 * @property \App\Model\Table\NotifyTable $Notify
 * @property \App\Model\Table\HomeTable $Home
 * @property \App\Model\Table\MenusTable $Menus Restaurant
 * @property \App\Model\Table\RestaurantTable $Restaurant
*/

class HomeController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        Configure::write('debug', 2);
        $this->loadModel('Residents');
        $this->loadModel('News');
        $this->loadModel('Notify');
        $this->loadModel('Home');
        $this->loadModel('Menus');
    }

    public function residentIndex($Flag=false)
    {


        $this->layout = 'resident';
        $this->loadModel('Menu');
        $userID = $this->Auth->user('id');
        //get user's email
        $emailQuery = $this->Residents->find();
        $emailQuery = $emailQuery->select([
            'email' => 'Residents.email',
        ])
            ->where(['Residents.id' => $userID]);
        $email = $emailQuery->first();

        //get user's phone
        $phoneQuery = $this->Residents->find();
        $phoneQuery = $phoneQuery->select([
            'phone' => 'Residents.phone',
        ])
            ->where(['Residents.id' => $userID]);
        $phone = $phoneQuery->first();

        //get user's property_type
        $propertyTypeQuery = $this->Residents->find();
        $propertyTypeQuery = $propertyTypeQuery->select([
            'property_type' => 'Residents.property_type',
        ])
            ->where(['Residents.id' => $userID]);
        $propertyType = $propertyTypeQuery->first();

        //get user's landline
        $landlineQuery = $this->Residents->find();
        $landlineQuery = $landlineQuery->select([
            'landline' => 'Residents.landline',
        ])
            ->where(['Residents.id' => $userID]);
        $landline = $landlineQuery->first();

        //redirect the user to register page if the user does have an email or phone number
        if (( ($email['email'] == "") AND ($phone['phone'] == "") ) OR ( ($propertyType['property_type'] == "") OR ($landline['landline'] == "") )){
            $this->redirect(['controller' => 'Residents', 'action' => 'register']);
        }

        $this->loadModel('Services');
        $services = $this->Services->find()->all();
        $this->set('services', $services );
        $categories = $this->Services->Categories->find()->all();
        $this->set('categories', $categories );
        $this->viewBuilder()->setLayout('resident');
        $newsData = $this->News->find('all', array(
            'order' => 'News.id DESC'
        ))->where(['News.archived' => false]);
        $Notify = $this->Notify->find('all');
        $Home = $this->Home->find('all');
        //$menu = $this->Menus->find('first')->where(['Menu.menu_date =' => date('Y-m-d')]);
        $Menus = $this->Menus->find('all', array(
            'limit' => 1
        ))->where(['Menus.menu_date =' => date('Y-m-d')]);
        $this->loadModel('Restaurant');
        $Restaurant = $this->Restaurant->find('all');
        $this->set(compact('newsData', 'Notify', 'Flag', 'Home','Menus','Restaurant'));
    }

    public function HomeIndex($Flag=false)
    {
        $this->loadModel('News');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

          $Home = $this->Home->find('all');
          $this->set(compact('Home'));

    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $news = $this->News->get($id);
        if ($this->News->delete($news)) {
            $this->Flash->success(__('The news has been deleted.'));
        } else {
            $this->Flash->error(__('The news could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }


    public function filtercategory($id)
    {

        $this->loadModel('Services');
        $services = $this->Services->find('all')->where(['services_categories.service_id' => 'services.service_id', 'id' => $id ])->contain(['services_categories']);
        $this->set('services', $services );
        $categories = $this->Services->Categories->find()->all();
        $this->set('categories', $categories );
        //  $this->layout = 'resident';
        $this->viewBuilder()->setLayout('resident');

    }

    public function display()
    {
        $try="test";

        $this->loadModel(“Services”);
        $services = $this->Services->find()->all();
        $this->set(‘services‘, $services );
        $this->set(’_serialize’, ['services']);
        $this->set(‘name‘, $try);
    }

    public function index()
    {
        $this->loadModel('News');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $News = $this->News->find('all')->where(['News.archived' => false]);
        $this->set(compact('News'));
    }


    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }



    public function addNews()
    {
        $this->loadModel('News');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $news = $this->News->newEntity();
        if ($this->request->is('post')) {
            $flag = 0;
            $news = $this->News->patchEntity($news, $this->request->getData());
            if ($this->News->save($news)) {
                $flag = 1;
            }
            if($flag == 1){
                $this->Flash->success(__('The news has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            else{
                $this->Flash->error(__('The news could not be saved. Please, try again.'));
            }

        }
        $this->set(compact('news'));
    }

    public function deleteNews($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $news = $this->News->get($id);
        if ($this->News->delete($news)) {
            $this->Flash->success(__('The news has been deleted.'));
        } else {
            $this->Flash->error(__('The news could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function archive($id = null){


        $news = $this->News->get($id);
        $news->archived = true;

        if ($this->News->save($news)){
            $this->Flash->success(__('This news has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this news.'));
    }


    public function editNews($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $news = $this->News->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {



            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $news = $this->News->patchEntity($news, $this->request->getData());

            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The news could not be saved. Please, try again.'));
        }
        $this->set(compact('news'));
    }

    public function editHome($id = null)
    {
        $this->loadModel('Home');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $home = $this->Home->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $home = $this->Home->patchEntity($home, $this->request->getData());

            if ($this->Home->save($home)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'HomeIndex']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $this->set(compact('home'));
    }

    public function notifyIndex(){

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $Notify = $this->Notify->find('all');
        $this->set(compact('Notify'));

    }

    public function addNotify()
    {
        $this->loadModel('Notify');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $notify = $this->Notify->newEntity();
        if ($this->request->is('post')) {
            $flag = 0;
            $notify = $this->Notify->patchEntity($notify, $this->request->getData());
            if ($this->Notify->save($notify)) {
                $flag = 1;
            }
            if($flag == 1){
                $this->Flash->success(__('The notification has been saved.'));

                return $this->redirect(['action' => 'notifyIndex']);
            }
            else{
                $this->Flash->error(__('The notification could not be saved. Please, try again.'));
            }

        }
        $this->set(compact('notify'));
    }

    public function deleteNotify($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $notify = $this->Notify->get($id);
        if ($this->Notify->delete($notify)) {
            $this->Flash->success(__('The Notification has been disabled.'));
        } else {
            $this->Flash->error(__('The Notification could not be disabled. Please, try again.'));
        }

        return $this->redirect(['action' => 'notifyIndex']);
    }

    public function editNotify($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $notify = $this->Notify->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {



            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $notify = $this->Notify->patchEntity($notify, $this->request->getData());

            if ($this->Notify->save($notify)) {
                $this->Flash->success(__('The notification has been saved.'));

                return $this->redirect(['action' => 'notifyIndex']);
            }
            $this->Flash->error(__('The notification could not be saved. Please, try again.'));
        }
        $this->set(compact('notify'));
    }

    public function archiveIndex(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $archivedNews = TableRegistry::get('News')->find('all')->where(['News.archived' => true]);
        $this->set('archivedNews', $archivedNews);
    }

    public function restore($id = null)
    {
        $news = $this->News->get($id);
        $news->archived = false;

        if ($this->News->save($news)) {
            $this->Flash->success(__('This news has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this news.'));
    }

    public function restaurantIndex($Flag=false)
    {
        $this->loadModel('Restaurant');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $Restaurant = $this->Restaurant->find('all');
        $this->set(compact('Restaurant'));

    }

    public function restaurantEdit($id = null)
    {
        $this->loadModel('Restaurant');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $Restaurant = $this->Restaurant->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $Restaurant = $this->Restaurant->patchEntity($Restaurant, $this->request->getData());

            if ($this->Restaurant->save($Restaurant)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'RestaurantIndex']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $this->set(compact('Restaurant'));
    }

}
