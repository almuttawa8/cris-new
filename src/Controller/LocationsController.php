<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 *
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $locations = $this->Locations->find();

        $this->set(compact('locations'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $location = $this->Locations->newEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $this->set(compact('location'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $location = $this->Locations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $this->set(compact('location'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $location = $this->Locations->get($id);
        if ($this->Locations->delete($location)) {
            $this->Flash->success(__('The location has been deleted.'));
        } else {
            $this->Flash->error(__('The location could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
