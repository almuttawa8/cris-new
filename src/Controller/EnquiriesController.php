<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Enquiries Controller
 *
 * @property \App\Model\Table\EnquiriesTable $Enquiries
 * @property \App\Model\Table\ResidentsTable $Residents
 * @property \App\Model\Table\FaqsTable $Faqs
 * @method \App\Model\Entity\Enquiry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EnquiriesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
        $this->loadModel('Faqs');
        $this->loadModel('Enquiries');
    }

    public function isAdmin(){

        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Faqs');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $Faqs = $this->Faqs->find('all');
        $this->set(compact('Faqs'));
    }

    public function EnquiriesIndex(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $Enquiries = $this->Enquiries->find('all');
        $Residents = $this->Residents->find('all');
        $this->set(compact('Enquiries', 'Residents'));
    }

    public function mainView()
    {
        $this->layout = 'resident';

        //get the user ID
        $userID = $this->Auth->user('id');

        $Faqs = $this->Faqs->find('all');
        $this->set(compact('Faqs'));
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->layout = 'resident';

        //get the user ID
        $userID = $this->Auth->user('id');
        $userfName = $this->Auth->user('fname');




        $enquiry = $this->Enquiries->newEntity();
        if ($this->request->is('post')) {
            $enquiry = $this->Enquiries->patchEntity($enquiry, $this->request->getData());
            //save the user ID
            $enquiry->resident_id = $userID;
            $enquiry->modified_name= $userfName;
            if ($this->Enquiries->save($enquiry)) {
                $this->Flash->success('Your enquiry has been sent. Your receipt number is '.$enquiry->id.'. Someone will contact you back within 5 business days.', [
                    'key' => 'success_enquiry'
                ]);

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error('Your enquiry has not been sent. Please try again.', [
                'key' => 'fail_enquiry'
            ]);
        }
        $topics = $this->Enquiries->Topics->find('list', ['limit' => 200]);
        $residents = $this->Enquiries->Residents->find('list', ['limit' => 200]);
        $this->set(compact('enquiry', 'topics', 'residents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $enquiry = $this->Enquiries->get($id, [
            'contain' => ['Residents', 'Topics']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $enquiry = $this->Enquiries->patchEntity($enquiry, $this->request->getData());
            $enquiry->modified_date = date('Y-m-d');
            //get the user's ID
            $userID = $this->Auth->user('id');

            //get the user lastname
            $last_name_query = $this->Residents->find()->select(['lname' => 'Residents.lname',])
                    ->where(['Residents.id' => $userID]);
            $last_name = $last_name_query->first();
            $enquiry->modified_name = $this->request->getSession()->read('Auth.User.fname') . ' ' . $last_name['lname'];

            $enquiry->modified_phone = $this->request->getSession()->read('Auth.User.phone');
            $enquiry->modified_email = $this->request->getSession()->read('Auth.User.email');

            if ($this->Enquiries->save($enquiry)) {
                $this->Flash->success(__('The enquiry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The enquiry could not be saved. Please, try again.'));
        }
        $topics = $this->Enquiries->Topics->find('list', ['limit' => 200]);
        $residents = $this->Enquiries->Residents->find('list', ['limit' => 200]);
        $this->set(compact('enquiry', 'topics','residents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Enquiry id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $enquiry = $this->Enquiries->get($id);
        if ($this->Enquiries->delete($enquiry)) {
            $this->Flash->success(__('The enquiry has been deleted.'));
        } else {
            $this->Flash->error(__('The enquiry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function residentIndex()
    {
        $this->layout = 'resident';

        $userID = $this->Auth->user('id');

        $enquiries = $this->Enquiries->find('all')->where(['Enquiries.resident_id'=>$userID])->contain(['Topics']);
        $this->set(compact('enquiries'));
    }

    public function view($id = null)
    {
        $this->layout = 'resident';
        $enquiry = $this->Enquiries->get($id, [
            'contain' => ['Residents', 'Topics']
        ]);
        $this->set(compact('enquiry'));
    }

    public function addNew(){
        $this->loadModel('Faqs');
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $this->log('test0');
        $faqs = $this->Faqs->newEntity();
        if ($this->request->is('post')) {
            $flag = 0;

            $faqs = $this->Faqs->patchEntity($faqs, $this->request->getData());
            if ($this->Faqs->save($faqs)) {
                $flag = 1;

            }
            if($flag == 1){
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            else{
                $this->Flash->error(__('The question could not be saved. Please, try again.'));
            }

        }
        $this->set(compact('faqs'));
    }

    public function editAnswer($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $faqs = $this->Faqs->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {



            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)
            $faqs = $this->Faqs->patchEntity($faqs, $this->request->getData());

            if ($this->Faqs->save($faqs)) {
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question could not be saved. Please, try again.'));
        }
        $this->set(compact('faqs'));
    }

    public function deleteQuestion($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faqs = $this->Faqs->get($id);
        if ($this->Faqs->delete($faqs)) {
            $this->Flash->success(__('The question has been deleted.'));
        } else {
            $this->Flash->error(__('The question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function deleteEnquiry($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $Enquiry = $this->Enquiries->get($id);
        if ($this->Enquiries->delete($Enquiry)) {
            $this->Flash->success(__('The enquiry has been deleted.'));
        } else {
            $this->Flash->error(__('The enquiry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'EnquiriesIndex']);
    }




}
