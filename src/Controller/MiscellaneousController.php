<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Miscellaneous Controller
 *
 * @property \App\Model\Table\MiscellaneousTable $Miscellaneous
 *
 * @method \App\Model\Entity\Miscellaneous[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MiscellaneousController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $miscellaneous = $this->Miscellaneous->find('all')->where(['Miscellaneous.archived' => false])->contain([]);

        $this->set(compact('miscellaneous'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $miscellaneous = $this->Miscellaneous->newEntity();
        if ($this->request->is('post')) {
            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);

            $miscellaneous = $this->Miscellaneous->patchEntity($miscellaneous, $this->request->getData());
            $miscellaneous->date_published = $date_published;
            if ($this->Miscellaneous->save($miscellaneous)) {
                $this->Flash->success(__('The miscellaneous has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The miscellaneous could not be saved. Please, try again.'));
        }
        $this->set(compact('miscellaneous'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Miscellaneous id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $miscellaneous = $this->Miscellaneous->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $date_published_raw = $this->request->getData('date_published');
            $date_published = Time::createFromFormat("d-m-Y", $date_published_raw);
            $miscellaneous = $this->Miscellaneous->patchEntity($miscellaneous, $this->request->getData());
            $miscellaneous->date_published = $date_published;
            if ($this->Miscellaneous->save($miscellaneous)) {
                $this->Flash->success(__('The miscellaneous has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The miscellaneous could not be saved. Please, try again.'));
        }
        $this->set(compact('miscellaneous'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Miscellaneous id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $miscellaneous = $this->Miscellaneous->get($id);
        if ($this->Miscellaneous->delete($miscellaneous)) {
            $this->Flash->success(__('The miscellaneous has been deleted.'));
        } else {
            $this->Flash->error(__('The miscellaneous could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        $misc = $this->Miscellaneous->get($id);
        $misc->archived = true;

        if ($this->Miscellaneous->save($misc)){
            $this->Flash->success(__('This miscellaneous has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this miscellaneous.'));
    }

    public function restore($id = null)
    {
        $misc = $this->Miscellaneous->get($id);
        $misc->archived = false;

        if ($this->Miscellaneous->save($misc)) {
            $this->Flash->success(__('This miscellaneous has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this miscellaneous.'));
    }

    public function archiveIndex(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedMiscs = TableRegistry::get('Miscellaneous')->find('all')->where(['Miscellaneous.archived' => true])->contain([]);
        $this->set('archivedMiscs', $archivedMiscs);
    }

    public function residentIndex(){
        $this->layout = 'resident';
        $miscs = $this->Miscellaneous->find('all')->where(['Miscellaneous.archived' => false])->contain([]);
        $this->set(compact('miscs'));
    }
}
