<?php

namespace App\Controller;

use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Registrations Controller
 *
 * @property \App\Model\Table\RegistrationsTable $Registrations
 *
 * @method \App\Model\Entity\Registration[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RegistrationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
        $this->loadModel('Events');
    }

    public function isAdmin()
    {
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if ($role['role'] == 'ADMIN') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isResident()
    {
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if ($query->count() == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $this->paginate = [
            'contain' => ['Residents'],
            'order' => [
                'Events.event_date' => 'asc'
            ],
            'limit' => 10
        ];

        $e = TableRegistry::getTableLocator()->get('Events');
        $query = $this->paginate($e->find());
        $events = $query->toArray();
        $this->set(compact( 'events'));
    }

    /**
     * View method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $registration = $this->Registrations->get( [
            'contain' => ['Residents', 'Events']
        ]);
        $residents = $this->Registrations->Residents->find('list', ['limit' => 200]);
        $events = $this->Registrations->Events->find('list', ['limit' => 200]);
        $this->set(compact('registration', 'residents', 'events'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->layout = 'resident';

        //check if resident already register an event
        $passedArgs = $this->request->getParam('pass');
        $userID = $this->Auth->user('id');
        $query = $this->Registrations->find()->select([
            'Registrations.event_id',
            'Registrations.resident_id'
        ])
            ->where(['Registrations.event_id' => $passedArgs[0]])
            ->where(['Registrations.resident_id' => $userID]);
        $exist = $query->count();

        //check for available seats
        //get event's availability
        $query = $this->Events->find()->select([
            'Events.availability',
        ])
            ->where(['Events.id' => $passedArgs[0]]);
        $availability = $query->first();
        $availability = $availability['availability'];

        //get count events
        $query = $this->Registrations->find()->where(['event_id' => $passedArgs[0]]);

        $total = $query->count();

        //if there is already a record of this resident registered for this event, show error msg and redirect to resident_index of Events
        if ($exist != 0) {
            $this->Flash->error('You already registered for this event.', [
                'key' => 're_register'
            ]);
            return $this->redirect(['controller' => 'Events', 'action' => 'resident_index']);
        } elseif ($availability - $total == 0) {
            $this->Flash->error('This event is full.', [
                'key' => 'full_register'
            ]);
            return $this->redirect(['controller' => 'Events', 'action' => 'resident_index']);
        }

        //Show event name
        $query = $this->Events->find()->select([
            'Events.name',
        ])
            ->where(['Events.id' => $passedArgs[0]]);
        $event_name = $query->first();
        $event_name = $event_name['name'];


        $registration = $this->Registrations->newEntity();
        if ($this->request->is('post')) {
            $registration = $this->Registrations->patchEntity($registration, $this->request->getData());
            $passedArgs = $this->request->getParam('pass');
            $registration->event_id = $passedArgs[0];
            $userID = $this->Auth->user('id');
            $registration->resident_id = $userID;


            if ($this->Registrations->save($registration)) {
                $this->Flash->success('Your registration is successful.', [
                    'key' => 'success_register'
                ]);

                return $this->redirect(['controller' => 'Events', 'action' => 'resident_index']);
            }
            $this->Flash->error(__('The registration could not be saved. Please, try again.'));
        }
        $residents = $this->Registrations->Residents->find('list', ['limit' => 200]);
        $events = $this->Registrations->Events->find('list', ['limit' => 200]);
        $this->set(compact('registration', 'residents', 'events','event_name'));
        $this->render('add');
    }


    /**
     * Edit method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $registration = $this->Registrations->get($id, [
            'contain' => ['Residents','Events']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $registration = $this->Registrations->patchEntity($registration, $this->request->getData());
            if ($this->Registrations->save($registration)) {
                $this->Flash->success(__('The registration has been saved.'));

                return $this->redirect(['action' => 'registration_event/'.$registration->event_id]);
            }
            $this->Flash->error(__('The registration could not be saved. Please, try again.'));
        }
        $residents = $this->Registrations->Residents->find('list', ['limit' => 200]);
        $events = $this->Registrations->Events->find('list', ['limit' => 200]);
        $this->set(compact('registration', 'residents', 'events'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $registration = $this->Registrations->get($id);
        $eventid = $registration->event_id;

        $events = TableRegistry::getTableLocator()->get('Events');
        $query = $events->find()->select(['event_date'])->where(['id' => $eventid])->toList();
        $event_date = $query[0]->event_date;
        //$event_date = $event_date->i18nFormat('yyyy-MM-dd HH:mm:ss');
        $max_date = $event_date->modify('-7 days');

//        $today = date("Y-m-d H:i:s");
        $today = Time::now();

//        debug($max_date);
//        debug($today);
//        debug($today > $max_date);
//
//        debug($event_date->isThisWeek());


        if ($today >= $max_date ) {
            $this->Flash->error(__('You can no longer unregister for this event'), ['key'=>'unregister']);
            return $this->redirect(['action' => 'resident_index']);

        } else {
            if ($this->Registrations->delete($registration)) {
                $this->Flash->success('You have unregistered successfully.', [
                    'key' => 'success_unregister'
                ]);
                return $this->redirect(['action' => 'resident_index']);
            } else {
                $this->Flash->error(__('Un-registration failed. Please, try again.'));
            }

        }
    }

    public function residentIndex()
    {
        $this->layout = 'resident';

        $userID = $this->Auth->user('id');

        $myRegistrations = TableRegistry::get('Registrations')->find('all')->where(['Registrations.resident_id' => $userID])->contain(['Residents', 'Events']);
        $this->set('myRegistrations', $myRegistrations);
    }

    public function home(){
        $filterid = $this->request->getParam('pass');


        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        if ($filterid != null) {
            $filterid = $filterid[0];
            $registrations = $this->Registrations->find('all')->where(['event_id' => $filterid])->contain(['Residents', 'Events']);
        } else {
            $registrations = $this->Registrations->find('all')->contain(['Residents', 'Events'] );
        }

        $e = TableRegistry::getTableLocator()->get('Events');
        $query = $e->find();
        $events = $query->toArray();
        $this->set(compact('registrations', 'events'));

    }

    public function residentEdit($id = null)
    {
        $this->layout = 'resident';
        $registration = $this->Registrations->get($id, [
            'contain' => ['Events']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $registration = $this->Registrations->patchEntity($registration, $this->request->getData());
            if ($this->Registrations->save($registration)) {
                $this->Flash->success('You have successfully edited your registration.', [
                    'key' => 'success_edit_registration'
                ]);

                return $this->redirect(['controller' => 'Registrations', 'action' => 'resident_index']);
            }
            $this->Flash->error(__('The registration could not be saved. Please, try again.'));
        }
        $residents = $this->Registrations->Residents->find('list', ['limit' => 200]);
        $events = $this->Registrations->Events->find('list', ['limit' => 200]);
        $this->set(compact('registration', 'residents', 'events'));
    }

    public function registrationEvent($id = null){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
        $registrations = $this->Registrations->find('all')->where(['event_id'=> $id])->contain(['Residents','Events']);

        $residents = $this->Registrations->Residents->find('list', ['limit' => 200]);
        $total= $registrations->count();
        $events = $this->Registrations->Events->get($id);
        $this->set(compact('registrations', 'residents', 'events', 'total'));

    }


}
