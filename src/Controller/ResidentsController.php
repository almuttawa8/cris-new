<?php
namespace App\Controller;
use App\Auth\LegacyPasswordHasher;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Residents Controller
 *
 * @property \App\Model\Table\ResidentsTable $Residents
 *
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResidentsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if ($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $residents = $this->Residents->find('all')->where(['Residents.archived' => false,'Residents.identifier !='=>'A000'])->contain([]);

        $this->set(compact('residents'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $resident = $this->Residents->newEntity();
        if ($this->request->is('post')) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());
            $resident->lname = ucfirst(strtolower($resident->lname));
            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('The resident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The resident could not be saved. Please, try again.'));
        }
        $properties = $this->Residents->Properties->find('list', ['limit' => 200]);
        $clubs = $this->Residents->Clubs->find('list', ['limit' => 200]);
        $events = $this->Residents->Events->find('list', ['limit' => 200]);
        $interests = $this->Residents->Interests->find('list', ['limit' => 200]);
        $roles = $this->Residents->Roles->find('list', ['limit' => 200]);
        $this->set(compact('resident', 'properties', 'clubs', 'events', 'interests', 'roles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Resident id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $resident = $this->Residents->get($id, [
            'contain' => ['Clubs', 'Events', 'Interests', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());
            $resident->lname = ucfirst($resident->lname);
            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('The resident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The resident could not be saved. Please, try again.'));
        }
        $properties = $this->Residents->Properties->find('list', ['limit' => 200]);
        $clubs = $this->Residents->Clubs->find('list', ['limit' => 200]);
        $events = $this->Residents->Events->find('list', ['limit' => 200]);
        $interests = $this->Residents->Interests->find('list', ['limit' => 200]);
        $roles = $this->Residents->Roles->find('list', ['limit' => 200]);
        $this->set(compact('resident', 'properties', 'clubs', 'events', 'interests', 'roles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Resident id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resident = $this->Residents->get($id);
        if ($this->Residents->delete($resident)) {
            $this->Flash->success(__('The resident has been deleted.'));
        } else {
            $this->Flash->error(__('The resident could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function send()
    {
        if ($this->request->is('post')){
            $mail = $this->request->getData('mail');
            $subject = $this->request->getData('subject');
            $message = $this->request->getData('message');
            $email = new Email('default');
            $email->from(['classic.residences.user@gmail.com' => 'Classic Residences'])
                ->to($mail)
                ->subject($subject)
                ->send($message);
            $this->Flash->success(__('The email has been sent successfully.'));
            return $this->redirect(['action' => 'index']);
        }
        $residents = $this->Residents->find('list', ['limit' => 200]);
        $this->set(compact('residents'));
    }

    public function archive($id = null){

        $resident = $this->Residents->get($id);
        $resident->archived = true;

        if ($this->Residents->save($resident)){
            $this->Flash->success(__('This resident has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this resident.'));

    }

    public function restore($id = null)
    {
        $resident = $this->Residents->get($id);
        $resident->archived = false;

        if ($this->Residents->save($resident)) {
            $this->Flash->success(__('This resident has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this resident.'));
    }


    public function archiveIndex()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedResdients = TableRegistry::get('Residents')->find('all')->where(['Residents.archived' => true]);
        $this->set('archivedResidents', $archivedResdients);

    }

    public function archivePage(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }
    }
    public function import()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        if ($this->request->is('post')) {
//
            $csvArray = array_map('str_getcsv', file($this->request->getData('file')['tmp_name']));
//

            foreach($csvArray as $index => $singleResident) {
                if($index == 0){ continue; }


                $identifier=$singleResident[2];
                $residentid=$this->Residents->findByIdentifier($identifier)->first();



//
                //Save one resident
                $resident = $this->Residents->newEntity();
                $resident = $this->Residents->patchEntity($resident, [
                    'fname' => $singleResident[0],
                    'lname' => ucfirst(strtolower($singleResident[1])),
                    'identifier'=>$singleResident[2]
                ]);


                if(!is_null($residentid)){
                    $resident->id=$residentid->id;
                }


                $result=$this->Residents->save($resident);


//
                if (!$result) {
                    $this->Flash->error(__('The resident could not be saved. Please, try again.'));
                }
            }
//
            $this->Flash->success(__('The resident has been saved.'));
            return $this->redirect(['action' => 'index']);

        }
    }

    public function profile(){
        $this->layout = 'resident';

        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        $role = $role['role'];

        $resident = $this->Residents->get($userID, [
            'contain' => ['Clubs', 'Events', 'Interests', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());
            if ($this->Residents->save($resident)) {
                $this->Flash->success('Your profile has been changed successfully.', [
                    'key' => 'success_profile'
                ]);

                return $this->redirect(['controller'=>'Residents','action' => 'profile']);
            }
            $this->Flash->error(__('The resident could not be saved. Please, try again.'));
        }
        $properties = $this->Residents->Properties->find('list', ['limit' => 200]);
        $clubs = $this->Residents->Clubs->find('list', ['limit' => 200]);
        $events = $this->Residents->Events->find('list', ['limit' => 200]);
        $interests = $this->Residents->Interests->find('list', ['limit' => 200]);
        $roles = $this->Residents->Roles->find('list', ['limit' => 200]);
        $this->set(compact('resident', 'properties', 'clubs', 'events', 'interests', 'roles','role'));
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Home', 'action' => 'resident_index/true']);
            } else {
                $this->Flash->error('Your Resident ID or your last name is incorrect', [
                    'key' => 'fail_login'
                ]);

            }
        }
    }

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new LegacyPasswordHasher)->hash($password);
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function noAccess(){
        $this->layout = 'resident';
    }

    public function register(){
        $this->layout = 'resident';

        //get the user's ID
        $userID = $this->Auth->user('id');


        $resident = $this->Residents->get($userID, [
            'contain' => ['Clubs', 'Events', 'Interests', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());
            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('Contact information is saved'));

                return $this->redirect(['controller'=>'Home', 'action' => 'resident_index']);
            }
            $this->Flash->error(__('Contact information could not be saved. Please, try again.'));
        }
        $properties = $this->Residents->Properties->find('list', ['limit' => 200]);
        $clubs = $this->Residents->Clubs->find('list', ['limit' => 200]);
        $events = $this->Residents->Events->find('list', ['limit' => 200]);
        $interests = $this->Residents->Interests->find('list', ['limit' => 200]);
        $roles = $this->Residents->Roles->find('list', ['limit' => 200]);
        $this->set(compact('resident', 'properties', 'clubs', 'events', 'interests', 'roles'));
    }

    public function residentRole(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }


        $residents = $this->Residents->find('all')
            ->where(['Residents.archived' => false, 'Residents.identifier !=' => 'A000'])->contain(['Roles']);

        $this->set(compact('residents'));
    }

    public function editRole($id=null){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $resident = $this->Residents->get($id, [
            'contain' => ['Clubs', 'Events', 'Interests', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());
            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('The role has been changed.'));

                return $this->redirect(['controller'=>'Residents','action' => 'resident_role']);
            }
            $this->Flash->error(__('The role could not be changed. Please, try again.'));
        }
        $properties = $this->Residents->Properties->find('list', ['limit' => 200]);
        $clubs = $this->Residents->Clubs->find('list', ['limit' => 200]);
        $events = $this->Residents->Events->find('list', ['limit' => 200]);
        $interests = $this->Residents->Interests->find('list', ['limit' => 200]);
        $roles = $this->Residents->Roles->find('list', ['limit' => 200]);
        $this->set(compact('resident', 'properties', 'clubs', 'events', 'interests', 'roles'));
    }

    public function currentCommittee(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }



        $residents = $this->Residents->find('all')->innerJoinWith('Roles')
            ->where(['Residents.archived' => false, 'Residents.identifier !=' => 'A000'])->contain(['Roles'])->distinct(['Residents.id']);
        $this->set(compact('residents'));
    }

}
/*$email = new Email();
$email -> setFrom('classic.residences.user@gmail.com')
    ->setTo($to)
    ->setSubject($subject);
if ($email->send($message)){
    return $this->redirect(['action' => 'index']);
}*/
