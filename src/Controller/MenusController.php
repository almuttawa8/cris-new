<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Menus Controller
 *
 * @property \App\Model\Table\MenusTable $Menus
 *
 * @method \App\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MenusController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Residents');
    }

    public function isAdmin(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        //get the role name
        $role = $query->first();

        //apply admin layout if the user is an admin
        if($role['role'] == 'ADMIN'){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isResident(){
        //get the user's ID
        $userID = $this->Auth->user('id');

        //get the role name of this user
        $roleQuery = $this->Residents->find();
        $query = $roleQuery->select([
            'role' => 'Roles.name',
        ])
            ->innerJoinWith('Roles')
            ->where(['Residents.id' => $userID]);

        if($query->count() == 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $menus = $this->Menus->find('all')->where(['Menus.archived' => false])->contain([]);

        $this->set(compact('menus'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {

            //Convert date from the form to TimeObject

            $menu_date_raw = $this->request->getData('menu_date');
            if ($menu_date_raw!=''){
                $menu_date = Time::createFromFormat("d-m-Y", $menu_date_raw);
                $menu = $this->Menus->patchEntity($menu, $this->request->getData());
                $menu->menu_date = $menu_date;
            }
            else{
                $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            }


            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $this->set(compact('menu'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            //Convert date from the form to TimeObject
            $menu_date_raw = $this->request->getData('menu_date');
            if ($menu_date_raw!=''){

            $menu_date = Time::createFromFormat("d-m-Y", $menu_date_raw);
                $menu = $this->Menus->patchEntity($menu, $this->request->getData());
             $menu->menu_date= $menu_date;
            }
            else{
                $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            }
            //Patch the date correctly (since patchEntity will not accept d-m-Y format date)



            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $this->set(compact('menu'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->Flash->success(__('The menu has been deleted.'));
        } else {
            $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'archive-index']);
    }

    public function archive($id = null){

        $menu = $this->Menus->get($id);
        $menu->archived = true;

        if ($this->Menus->save($menu)){
            $this->Flash->success(__('This food menu has been archived.'));

            return $this->redirect(['action' => 'index']);

        }
        $this->Flash->error(__('Unable to archive this food menu.'));
    }

    public function restore($id = null)
    {
        $menu = $this->Menus->get($id);
        $menu->archived = false;

        if ($this->Menus->save($menu)) {
            $this->Flash->success(__('This food menu has been restored.'));

            return $this->redirect(['action' => 'archiveIndex']);
        }
        $this->Flash->error(__('Unable to restore this food menu.'));
    }

    public function archiveIndex(){
        if($this->isResident()) {
            $this->redirect(['controller' => 'Residents', 'action' => 'no_access']);
        }
        elseif ($this->isAdmin()){
            $this->layout = 'admin';
        }
        else{
            $this->layout = 'default';
            $this->redirect(['controller' => 'Residents', 'action' => 'current_committee']);
        }

        $archivedMenus = TableRegistry::get('Menus')->find('all')->where(['Menus.archived' => true])->contain([]);
        $this->set('archivedMenus', $archivedMenus);
    }

    public function residentIndex(){
        $this->layout = 'resident';
        $menus = $this->Menus->find('all')->where(['Menus.archived' => false])->contain([]);
        $this->set(compact('menus'));
    }
}
