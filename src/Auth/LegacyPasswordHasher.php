<?php

namespace App\Auth;

use Cake\Auth\AbstractPasswordHasher;

class LegacyPasswordHasher extends AbstractPasswordHasher
{

    public function hash($password)
    {
        return ucfirst(strtolower($password));
    }

    public function check($password, $hashedPassword)
    {
        return ucfirst(strtolower($password)) === $hashedPassword;
    }
}
