<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MinCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MinCategoriesTable Test Case
 */
class MinCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MinCategoriesTable
     */
    public $MinCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MinCategories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MinCategories') ? [] : ['className' => MinCategoriesTable::class];
        $this->MinCategories = TableRegistry::get('MinCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MinCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
