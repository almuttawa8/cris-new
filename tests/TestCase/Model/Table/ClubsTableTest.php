<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClubsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClubsTable Test Case
 */
class ClubsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClubsTable
     */
    public $Clubs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Clubs',
        'app.Residents',
        'app.Properties',
        'app.Types',
        'app.ResidentsClubs',
        'app.Events',
        'app.Locations',
        'app.ResidentsEvents',
        'app.Interests',
        'app.ResidentsInterests',
        'app.Roles',
        'app.ResidentsRoles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clubs') ? [] : ['className' => ClubsTable::class];
        $this->Clubs = TableRegistry::get('Clubs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clubs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
